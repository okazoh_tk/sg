/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_LIGHT_H_INCLUDED_
#define SG_LIGHT_H_INCLUDED_

#include "sg/SharedObject.h"
#include "sg/MathUtil.h"

namespace sg {

class Program;

class SG_PUBLIC_API Light : public SharedObject {
public:
    enum Type {
        INVALID = 0,
        DIRECTIONAL = 1,
        SPOT = 2,
        POINT = 3,
    };

    virtual ~Light() = 0;

    /**
     * @attention Ignored alpha component. Alpha is always 1.
     */
    void setDiffuseColor(const Colorf& color);
    Colorf getDiffuseColor() const;

    /**
    * @attention Ignored alpha component. Alpha is always 1.
    */
    void setAmbientColor(const Colorf& color);
    Colorf getAmbientColor() const;

    /**
    * @attention Ignored alpha component. Alpha is always 1.
    */
    void setSpecularColor(const Colorf& color);
    Colorf getSpecularColor() const;

    struct Attenuation {
        float constant, linear, quadratic;
    };

    struct Cone {
        float inner_angle, outer_angle;
    };

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(Light* light = nullptr);
        WeakRef(const WeakRef& rhv);
        virtual ~WeakRef();

        Light* operator->();
        void operator=(const WeakRef& rhv);
        void operator=(Light* light);

        operator Light*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        Light* mLight;
    };

    void copyUniforms(Program& program, uint32_t index);

protected:
    Light(Type type);

    struct ProgramData {
        uint32_t name;
        const char* prefix;
    };

    virtual void onCopyValues(const ProgramData& program) = 0;
    static void copy(const ProgramData& program, const char* name, int32_t value);
    static void copy(const ProgramData& program, const char* name, float value);
    static void copy(const ProgramData& program, const char* name, const Colorf& color);
    static void copy(const ProgramData& program, const char* name, const Vector3& vec);

private:
    Type mType;
    Colorf mDiffuse;
    Colorf mAmbient;
    Colorf mSpecular;
};

class SG_PUBLIC_API SpotLight : public Light {
public:
    SpotLight();
    virtual ~SpotLight();

    void setDirection(const Vector3& direction);
    Vector3 getDirection() const;
    void setPosition(const Position3& position);
    Position3 getPosition() const;
    void setAttenuation(const Attenuation& attenuation);
    Attenuation getAttenuation() const;

    /** Set cone angles in degrees.
     */
    void setCone(const Cone& cone);
    Cone getCone() const;

    void setFallOffExponent(float exponent);
    float getFallOffExponent() const;

protected:
    virtual void onCopyValues(const ProgramData& program);

private:
    Vector3 mDirection;
    Position3 mPosition;
    Cone mCone;
    Attenuation mAttenuation;
    float mFallOffExponent;
};

class SG_PUBLIC_API PointLight : public Light {
public:
    PointLight();
    virtual ~PointLight();

    void setPosition(const Position3& position);
    Position3 getPosition() const;
    void setAttenuation(const Attenuation& attenuation);
    Attenuation getAttenuation() const;

protected:
    virtual void onCopyValues(const ProgramData& program);

private:
    Attenuation mAttenuation;
    Position3 mPosition;
};

class SG_PUBLIC_API DirectionalLight : public Light {
public:
    DirectionalLight();
    virtual ~DirectionalLight();

    void setDirection(const Vector3& direction);
    Vector3 getDirection() const;

protected:
    virtual void onCopyValues(const ProgramData& program);

private:
    Vector3 mDirection;
};

}

#endif
