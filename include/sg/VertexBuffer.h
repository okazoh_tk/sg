/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_VERTEX_BUFFER_H_INCLUDED_
#define SG_VERTEX_BUFFER_H_INCLUDED_

#include "sg/Config.h"
#include "sg/MathUtil.h"
#include "sg/Buffer.h"
#include <vector>

namespace sg {


struct VertexType {
    enum Type {
        BYTE = 0,
        UNSIGNED_BYTE,
        SHORT,
        UNSIGNED_SHORT,
        INT,
        UNSIGNED_INT,
        FLOAT,
        FIXED,
        HALF_FLOAT,
        INT_2_10_10_10_REV,
        UNSIGNED_INT_2_10_10_10_REV,
    };

    uint16_t num;
    Type type;
    bool normalized;
};

struct VertexAttrib {
    const char* name;
    const VertexType* type;
};


struct PosVertex {
    Vector3 position;

    static const VertexAttrib* LAYOUT;
};

struct PosColVertex {
    Vector3 position;
    Colorf color;

    static const VertexAttrib* LAYOUT;
};

struct PosTexVertex {
    Vector3 position;
    Vector2 uv;

    static const VertexAttrib* LAYOUT;
};

struct PosNormVertex {
    Vector3 position;
    Vector3 normal;

    static const VertexAttrib* LAYOUT;
};

struct PosColTexVertex {
    Vector3 position;
    Colorf color;
    Vector2 uv;

    static const VertexAttrib* LAYOUT;
};

struct PosColNormVertex {
    Vector3 position;
    Colorf color;
    Vector3 normal;

    static const VertexAttrib* LAYOUT;
};

struct PosTexNormVertex {
    Vector3 position;
    Vector2 uv;
    Vector3 normal;

    static const VertexAttrib* LAYOUT;
};

struct PosColTexNormVertex {
    Vector3 position;
    Colorf color;
    Vector2 uv;
    Vector3 normal;

    static const VertexAttrib* LAYOUT;
};

#define SG_POSITION_NAME   "inPosition"
#define SG_TEXCOORD_NAME   "inTexcoord"
#define SG_TEXCOORD0_NAME  "inTexcoord0"
#define SG_TEXCOORD1_NAME  "inTexcoord1"
#define SG_TEXCOORD2_NAME  "inTexcoord2"
#define SG_TEXCOORD3_NAME  "inTexcoord3"
#define SG_TEXCOORD4_NAME  "inTexcoord4"
#define SG_TEXCOORD5_NAME  "inTexcoord5"
#define SG_TEXCOORD6_NAME  "inTexcoord6"
#define SG_TEXCOORD7_NAME  "inTexcoord7"
#define SG_COLOR_NAME      "inColor"
#define SG_COLOR0_NAME     "inColor0"
#define SG_COLOR1_NAME     "inColor1"
#define SG_COLOR2_NAME     "inColor2"
#define SG_COLOR3_NAME     "inColor3"
#define SG_COLOR4_NAME     "inColor4"
#define SG_COLOR5_NAME     "inColor5"
#define SG_COLOR6_NAME     "inColor6"
#define SG_COLOR7_NAME     "inColor7"
#define SG_NORMAL_NAME     "inNormal"

class MemoryAllocator;

class SG_PUBLIC_API VertexBuffer : public Buffer {
public:

    static uint32_t computeAttribSize(const VertexAttrib* attrib);
    static uint32_t computeVertexSize(const VertexAttrib* layout);

    static const VertexType POSITION_TYPE;
    static const VertexType TEXCOORD_TYPE;
    static const VertexType TEXCOORD_CUBE_TYPE;
    static const VertexType NORMAL_TYPE;
    static const VertexType COLORF_TYPE;

    VertexBuffer(Usage usage = DYNAMIC_DRAW);
    virtual ~VertexBuffer();

    bool reserve(const VertexAttrib* layout, uint32_t vertex_num);
    void clear();

    void set(const Vector3* position, uint32_t num);
    void set(const Vector3* position, const Colorf* colors, uint32_t num);
    void set(const Vector3* position, const Vector2* texcoords, uint32_t num);
    void set(const Vector3* position, const Vector3* normals, uint32_t num);

    void set(
        const Vector3* position,
        const Colorf* colors,
        const Vector2* texcoords,
        uint32_t num);

    void set(
        const Vector3* position,
        const Colorf* colors,
        const Vector3* normals,
        uint32_t num);

    void set(
        const Vector3* position,
        const Vector2* texcoords,
        const Vector3* normals,
        uint32_t num);

    void set(
        const Vector3* position,
        const Colorf* colors,
        const Vector2* texcoords,
        const Vector3* normals,
        uint32_t num);

    void reset(
        void* vertices,
        uint32_t vertex_size,
        uint32_t vertex_num,
        const VertexAttrib* vertex_layout,
        MemoryAllocator* release_delegate = nullptr);
    void copy(uint32_t index, const void* vertices, uint32_t copy_num);


    const VertexAttrib* getVertexLayout() const;
    uint32_t getVertexNum() const;
    uint32_t getVertexSize() const;
    void* getBuffer();
    const void* getBuffer() const;
    uint32_t getBufferSize() const;

    void transferData();

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(VertexBuffer* vbuffer = nullptr);
        WeakRef(const WeakRef& rhv);
        virtual ~WeakRef();

        VertexBuffer* operator->();
        void operator=(const WeakRef& rhv);
        void operator=(VertexBuffer* vbuffer);

        operator VertexBuffer*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        VertexBuffer* mBuffer;
    };

private:
    uint32_t mVertexNum;
    uint32_t mVertexSize;
    void* mBuffer;
    uint32_t mBufferSize;
    const VertexAttrib* mVertexLayout;
    MemoryAllocator* mReleaseDelegate;
};

}

#endif
