/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_BUFFER_H_INCLUDED_
#define SG_BUFFER_H_INCLUDED_

#include "sg/Config.h"
#include "sg/SharedObject.h"

namespace sg {

class SG_PUBLIC_API Buffer : public SharedObject {
public:

    enum Type {
        ARRAY_BUFFER = 0,
        ELEMENT_ARRAY_BUFFER,

        // not(OpenGL ES 2.0)
        COPY_READ_BUFFER,
        COPY_WRITE_BUFFER,
        PIXEL_PACK_BUFFER,
        PIXEL_UNPACK_BUFFER,
        TRANSFORM_FEEDBACK_BUFFER,
        UNIFORM_BUFFER,
    };

    enum Usage {
        STREAM_DRAW = 0,
        STATIC_DRAW,
        DYNAMIC_DRAW,

        // not(OpenGL ES 2.0)
        STREAM_READ,
        STREAM_COPY,
        STATIC_READ,
        STATIC_COPY,
        DYNAMIC_READ,
        DYNAMIC_COPY,
    };

    Buffer(Type type, Usage usage);
    virtual ~Buffer();

    uint32_t getName() const;
    Type getType() const;
    Usage getUsage() const;

    void setUsage(Usage usage);

protected:
    bool transfer(uint32_t len, const void* data);
    bool transfer(uint32_t offset, uint32_t len, const void* data);
    
public:
    uint32_t mName;
    Type mType;
    Usage mUsage;
};

}

#endif
