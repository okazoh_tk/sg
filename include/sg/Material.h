/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_MATERIAL_H_INCLUDED_
#define SG_MATERIAL_H_INCLUDED_

#include "sg/Property.h"

namespace sg {

class MaterialImpl;
class Program;

#define SG_UNIFORM_MVP_MATRIX       "gMVPMatrix"
#define SG_UNIFORM_MV_MATRIX        "gMVMatrix"
#define SG_UNIFORM_M_MATRIX         "gMMatrix"
#define SG_UNIFORM_NORMAL_MATRIX    "gNormalMatrix"
#define SG_UNIFORM_COLOR            "gColor"
#define SG_UNIFORM_TEXTURE          "gTexture"
#define SG_UNIFORM_COLOR_DIFFUSE    "gDiffuse"
#define SG_UNIFORM_COLOR_SPECULAR   "gSpecular"
#define SG_UNIFORM_COLOR_AMBIENT    "gAmbient"
#define SG_UNIFORM_COLOR_EMISSIVE   "gEmissive"
#define SG_UNIFORM_OPACITY          "gOpacity"
#define SG_UNIFORM_SHININESS        "gShininess"
#define SG_UNIFORM_GLOBAL_AMBIENT   "gGlobalAmbient"
#define SG_UNIFORM_CAMERA_POSITION  "gCameraPos"

class SG_PUBLIC_API Material {
public:
    Material();
    virtual ~Material();

    void setProgram(Program* program);
    Program* getProgram();
    bool hasProgram() const;

    void set(const char* key, const Property& prop);
    void remove(const char* key);
    void reserve(const char* key);
    bool hasKey(const char* key) const;

    void printProperties();

    virtual bool update(Context& gl);
    virtual void restore(Context& gl);

    // for Mesh
    void copyProperties(Context& gl);
    void cleanupAfterDraw();

protected:
    virtual Property* findProperty(const char* key);

private:
    void setUniform(int32_t location, const Property& prop);

private:
    MaterialImpl* mImpl;
};

}

#endif
