/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_INDEX_BUFFER_H_INCLUDED_
#define SG_INDEX_BUFFER_H_INCLUDED_

#include "sg/Buffer.h"

namespace sg {
class MemoryAllocator;

class SG_PUBLIC_API IndexBuffer : public Buffer {
public:
    IndexBuffer(Usage usage = DYNAMIC_DRAW);
    virtual ~IndexBuffer();

    bool isInt() const;
    bool isShort() const;
    const void* getIndices() const;
    uint32_t getIndicesNum() const;
    uint32_t getBufferSize() const;
    
    bool reserve(uint32_t indices_num, bool is_short_type = true);
    void clear();

    /** Clone indices data.
     */
    void set(const uint16_t* indices, uint32_t indices_num);

    /** Clone indices data.
    */
    void set(const uint32_t* indices, uint32_t indices_num);

    void copy(uint32_t index, const uint16_t* indices, uint32_t copy_num);
    void copy(uint32_t index, const uint32_t* indices, uint32_t copy_num);

    void reset(
        void* indices,
        uint32_t indices_num,
        bool is_short,
        MemoryAllocator* release_delegate = nullptr);

    /** Transfer data to GPU.
     */
    void transferData();

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(IndexBuffer* ibuffer = nullptr);
        WeakRef(const WeakRef& rhv);
        virtual ~WeakRef();

        IndexBuffer* operator->();
        void operator=(const WeakRef& rhv);
        void operator=(IndexBuffer* ibuffer);

        operator IndexBuffer*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        IndexBuffer* mBuffer;
    };

private:
    uint32_t mIsInt: 1;
    uint32_t mIndicesNum: 31;
    uint32_t mBufferSize;
    void* mIndices;
    MemoryAllocator* mReleaseDelegate;
};

}

#endif
