/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_IDEVICE_H_INCLUDED_
#define SG_IDEVICE_H_INCLUDED_

#include "sg/Config.h"

namespace sg {
   
class IPointer;
class IButton;
class IKey;

class SG_PUBLIC_API IDevice {
public:

    IDevice();
    virtual ~IDevice() = 0;

    virtual IPointer* getPointerInterface() = 0;
    virtual IButton* getButtonInterface() = 0;
    virtual IKey* getKeyInterface() = 0;
};


struct PointerState {
    int32_t id;
    int32_t x, y;
};

class SG_PUBLIC_API IPointer {
public:
    IPointer();
    virtual ~IPointer() = 0;

    virtual const PointerState* getState(int32_t id) const = 0;
};

struct ButtonState {
    int32_t id;
    bool pressed;
};

class SG_PUBLIC_API IButton {
public:
    IButton();
    virtual ~IButton() = 0;

    virtual const ButtonState* getState(int32_t id) const = 0;
};

struct KeyState {
    int32_t ch;
    bool pressed;
};

class SG_PUBLIC_API IKey {
public:
    IKey();
    virtual ~IKey() = 0;

    virtual const KeyState* getState(int32_t ch) const = 0;
};

}

#endif
