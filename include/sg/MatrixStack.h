/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_MATRIX_STACK_H_INCLUDED_
#define SG_MATRIX_STACK_H_INCLUDED_

#include "sg/Config.h"
#include "sg/MathUtil.h"
#include "sg/SharedObject.h"

namespace sg {

class MatrixStackImpl;

class SG_PUBLIC_API MatrixStack : public SharedObject {
public:

    MatrixStack();
    virtual ~MatrixStack();

    /** Get Model-View-Projection matrix.
     */
    void getModelViewProjectionMatrix(Matrix4& out);
    void getMVPMatrix(Matrix4& out);

    /** Get Model-View matrix.
     */
    void getModelViewMatrix(Matrix4& out);

    /** Get Model matrix.
    */
    void getModelMatrix(Matrix4& out);

    Position3 getCameraPosition();

    // camera
    void project(const Perspective& perspective);
    void project(const Frustum& frustum);
    void project(const Ortho& ortho);

    void push();
    void pop();

    void identity();

    void lookAt(const Position3& eye, const Position3& at, const Vector3& up);
    void cameraPose(const Position3& position, const Quaternion& pose);

    void translate(const Vector3& vec);
    void rotate(float angle, const Vector3& coord);
    void rotate(const Quaternion& quat);
    void scale(const Vector3& factor);
    void multiply(const Matrix4& matrix);

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(MatrixStack* stack = nullptr);
        WeakRef(const WeakRef& rhv);
        virtual ~WeakRef();

        MatrixStack* operator->();
        void operator=(const WeakRef& rhv);
        void operator=(MatrixStack* stack);

        operator MatrixStack*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        MatrixStack* mStack;
    };
private:
    MatrixStackImpl* mImpl;
};

}

#endif
