/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_EVENT_DRIVER_H_INCLUDED_
#define SG_EVENT_DRIVER_H_INCLUDED_

#include "sg/Event.h"

namespace sg {

class EventDriverImpl;

class SG_PUBLIC_API EventDriver {
public:

    void exitLoop();

    void addEventListener(KeyEventListener* listener, int32_t priority);
    void addEventListener(ButtonEventListener* listener, int32_t priority);
    void addEventListener(PointerEventListener* listener, int32_t priority);
    void addEventListener(UIEventListener* listener, int32_t priority);
    void addEventListener(RendererEventListener* listener, int32_t priority);
    void addEventListener(SystemEventListener* listener, int32_t priority);

    void removeEventListener(KeyEventListener* listener);
    void removeEventListener(ButtonEventListener* listener);
    void removeEventListener(PointerEventListener* listener);
    void removeEventListener(UIEventListener* listener);
    void removeEventListener(RendererEventListener* listener);
    void removeEventListener(SystemEventListener* listener);

    Size getWindowSize() const;
    float getFps() const;
    const Device& getDevice() const;

protected:
    EventDriver(const Size& window_size);
    virtual ~EventDriver();

protected:
    EventDriverImpl* mImpl;
};

}

#endif
