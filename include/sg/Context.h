/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_CONTEXT_H_INCLUDED_
#define SG_CONTEXT_H_INCLUDED_

#include "sg/Config.h"
#include "sg/Mesh.h"
#include "sg/Material.h"
#include "sg/Model.h"
#include "sg/Setting.h"
#include "sg/MatrixStack.h"
#include "sg/Light.h"

#define SG_COLOR_BUFFER_BIT     (0x01)
#define SG_DEPTH_BUFFER_BIT     (0x02)
#define SG_STENCIL_BUFFER_BIT   (0x04)
#define SG_CLEAR_ALL            (SG_COLOR_BUFFER_BIT|SG_DEPTH_BUFFER_BIT|SG_STENCIL_BUFFER_BIT)

namespace sg {

class ContextImpl;
class RenderTarget;

#if 0

enum StringParameter {
    GL_EXTENSIONS,
    GL_VENDOR,
    GL_RENDERER,
    GL_VERSION,
    GL_SHADING_LANGUAGE_VERSION,
};

enum HintTarget {
    GL_FRAGMENT_SHADER_DERIVATIVE_HINT,
    GL_GENERATE_MIPMAP_HINT
};

enum HintMode {
    GL_FASTEST,
    GL_NICEST,
    GL_DONT_CARE
};

enum Format {
    SG_RGBA,
    SG_RGBA_INTEGER
};

enum DataType {
    SG_UNSIGNED_BYTE,
    SG_UNSIGNED_INT,
    SG_INT,
    SG_FLOAT
};
#endif

class SG_PUBLIC_API Context : public Setting {
public:

    Context(const Size& window_size);
    virtual ~Context();

    void pushSetting();
    void popSetting();

    Size getCurrentTargetSize() const;

    void clear(uint8_t mask = SG_CLEAR_ALL);
    void clear(const Color& color, uint8_t mask = SG_CLEAR_ALL);
    void clear(float red, float green, float blue, float alpha, uint8_t mask = SG_CLEAR_ALL);
    void clearColor(float red, float green, float blue, float alpha);
    void clearDepth(float d);
    void clearStencil(int32_t s);
    void viewport(const Viewport& viewport);
    void viewport(int32_t x, int32_t y, int32_t width, int32_t height);

    MatrixStack& getDefaultMatrix();
    MatrixStack& getCurrentMatrix();
    void begin();
    void begin(MatrixStack* matrix);
    void begin(MatrixStack* matrix, RenderTarget* target);
    void end();

    void drawAxes();
    void drawGrid();

    void draw(Mesh& mesh, Material& material);
    void draw(Model& model);

    void place(
        Model& model,
        const Vector3& translate,
        const Vector3& scale,
        const Quaternion& rotate);

    void registProgram(const char* name, Program* new_program, bool release_on_lost_ref = false);
    Program* findProgram(const char* name);

    void addLight(Light* light);
    void removeLight(Light* light);
    uint32_t getLightNum();
    Light* getLight(uint32_t index);

protected:

#if 0

    void getFloatv(Parameter pname, float *data);
    void getIntegerv(Parameter pname, int32_t *data);
    void getBooleanv(Parameter pname, bool *data);
    const char *getString(StringParameter name);
    const char *getString(StringParameter name, int32_t index);
    int32_t getError(void);

    void colorMask(bool red, bool green, bool blue, bool alpha);

    void hint(HintTarget target, HintMode mode);
    void readPixels(int32_t x, int32_t y, int32_t width, int32_t height, Format format, DataType type, void *pixels);

    //void LineWidth (float width);
    //void DrawArrays (GLenum mode, GLint first, GLsizei count);
    //void DrawElements (GLenum mode, GLsizei count, GLenum type, const void *indices);
    //void Finish (void);
    //void Flush (void);


    //GLboolean IsBuffer (GLuint buffer);
    //GLboolean IsEnabled (GLenum cap);
    //GLboolean IsFramebuffer (GLuint framebuffer);
    //GLboolean IsRenderbuffer (GLuint renderbuffer);
    //GLboolean IsTexture (GLuint texture);
#endif

#if 0
    // buffer
    void BindBuffer (GLenum target, GLuint buffer);
    void BufferData (GLenum target, GLsizeiptr size, const void *data, GLenum usage);
    void BufferSubData (GLenum target, GLintptr offset, GLsizeiptr size, const void *data);
    void DeleteBuffers (GLsizei n, const GLuint *buffers);
    void GenBuffers (GLsizei n, GLuint *buffers);
    void GetBufferParameteriv (GLenum target, GLenum pname, GLint *params);
    void PolygonOffset (float factor, float units);
    
    // vertex attribute
    void DisableVertexAttribArray (GLuint index);
    void EnableVertexAttribArray (GLuint index);
    void VertexAttrib1f (GLuint index, float x);
    void VertexAttrib1fv (GLuint index, const float *v);
    void VertexAttrib2f (GLuint index, float x, float y);
    void VertexAttrib2fv (GLuint index, const float *v);
    void VertexAttrib3f (GLuint index, float x, float y, float z);
    void VertexAttrib3fv (GLuint index, const float *v);
    void VertexAttrib4f (GLuint index, float x, float y, float z, float w);
    void VertexAttrib4fv (GLuint index, const float *v);
    void VertexAttribPointer (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer);

    // framebuffer
    void BindFramebuffer (GLenum target, GLuint framebuffer);
    GLenum CheckFramebufferStatus (GLenum target);
    void DeleteFramebuffers (GLsizei n, const GLuint *framebuffers);
    void FramebufferRenderbuffer (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
    void FramebufferTexture2D (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
    void GenFramebuffers (GLsizei n, GLuint *framebuffers);
    void GetFramebufferAttachmentParameteriv (GLenum target, GLenum attachment, GLenum pname, GLint *params);

    // renderbuffer
    void BindRenderbuffer (GLenum target, GLuint renderbuffer);
    void DeleteRenderbuffers (GLsizei n, const GLuint *renderbuffers);
    void GenRenderbuffers (GLsizei n, GLuint *renderbuffers);
    void GetRenderbufferParameteriv (GLenum target, GLenum pname, GLint *params);
    void RenderbufferStorage (GLenum target, GLenum internalformat, GLsizei width, GLsizei height);

    // program
    GLuint CreateProgram (void);
    void DeleteProgram (GLuint program);
    void AttachShader (GLuint program, GLuint shader);
    void BindAttribLocation (GLuint program, GLuint index, const GLchar *name);
    void DetachShader (GLuint program, GLuint shader);
    void GetUniformfv (GLuint program, GLint location, float *params);
    void GetUniformiv (GLuint program, GLint location, GLint *params);
    GLint GetUniformLocation (GLuint program, const GLchar *name);
    GLboolean IsProgram (GLuint program);
    void LinkProgram (GLuint program);
    void GetActiveAttrib (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
    void GetActiveUniform (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
    void GetAttachedShaders (GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders);
    GLint GetAttribLocation (GLuint program, const GLchar *name);
    void GetProgramiv (GLuint program, GLenum pname, GLint *params);
    void GetProgramInfoLog (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
    void UseProgram (GLuint program);
    void ValidateProgram (GLuint program);
    void Uniform1f (GLint location, float v0);
    void Uniform1fv (GLint location, GLsizei count, const float *value);
    void Uniform1i (GLint location, GLint v0);
    void Uniform1iv (GLint location, GLsizei count, const GLint *value);
    void Uniform2f (GLint location, float v0, float v1);
    void Uniform2fv (GLint location, GLsizei count, const GLfloat *value);
    void Uniform2i (GLint location, GLint v0, GLint v1);
    void Uniform2iv (GLint location, GLsizei count, const GLint *value);
    void Uniform3f (GLint location, float v0, float v1, float v2);
    void Uniform3fv (GLint location, GLsizei count, const float *value);
    void Uniform3i (GLint location, GLint v0, GLint v1, GLint v2);
    void Uniform3iv (GLint location, GLsizei count, const GLint *value);
    void Uniform4f (GLint location, float v0, float v1, float v2, float v3);
    void Uniform4fv (GLint location, GLsizei count, const float *value);
    void Uniform4i (GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
    void Uniform4iv (GLint location, GLsizei count, const GLint *value);
    void UniformMatrix2fv (GLint location, GLsizei count, GLboolean transpose, const float *value);
    void UniformMatrix3fv (GLint location, GLsizei count, GLboolean transpose, const float *value);
    void UniformMatrix4fv (GLint location, GLsizei count, GLboolean transpose, const float *value);
#endif

private:
    Context(const Context&) NONCOPYABLE;
    void operator=(const Context&) NONCOPYABLE;

private:
    ContextImpl* mImpl;
};

}

#endif
