/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_SETTING_H_INCLUDED_
#define SG_SETTING_H_INCLUDED_

#include "sg/Config.h"

enum SGBlendFactor {
    SG_ZERO = 0,
    SG_ONE,
    SG_SRC_COLOR,
    SG_ONE_MINUS_SRC_COLOR,
    SG_DST_COLOR,
    SG_ONE_MINUS_DST_COLOR,
    SG_SRC_ALPHA,
    SG_ONE_MINUS_SRC_ALPHA,
    SG_DST_ALPHA,
    SG_ONE_MINUS_DST_ALPHA,
    SG_CONSTANT_COLOR,
    SG_ONE_MINUS_CONSTANT_COLOR,
    SG_CONSTANT_ALPHA,
    SG_ONE_MINUS_CONSTANT_ALPHA,
    SG_SRC_ALPHA_SATURATE
};

enum SGBlendEquation {
    SG_FUNC_ADD = 0,
    SG_FUNC_SUBTRACT,
    SG_FUNC_REVERSE_SUBTRACT,
    SG_MIN,
    SG_MAX
};

enum SGFace {
    SG_FRONT = 0,
    SG_BACK,
    SG_FRONT_AND_BACK,
};

enum SGFunction {
    SG_NEVER = 0,
    SG_LESS,
    SG_LEQUAL,
    SG_GREATER,
    SG_GEQUAL,
    SG_EQUAL,
    SG_NOTEQUAL,
    SG_ALWAYS
};

enum SGStencilOp {
    SG_KEEP = 0,
    SG_SET_ZERO,
    SG_REPLACE,
    SG_INCR,
    SG_INCR_WRAP,
    SG_DECR,
    SG_DECR_WRAP,
    SG_INVERT,
};

enum SGFrontFace {
    SG_CW,
    SG_CCW,
};

namespace sg {

struct SettingData;

class SG_PUBLIC_API Setting {
public:

    class SG_PUBLIC_API Blend {
    public:
        void enable(bool enable);
        void on();
        void off();
        void color(float r, float g, float b, float a);
        void color(const Colorf& col);
        void func(
            SGBlendFactor src, SGBlendFactor dst);
        void func(
            SGBlendFactor srcRGB, SGBlendFactor dstRGB,
            SGBlendFactor srcAlpha, SGBlendFactor dstAlpha);
        void equation(SGBlendEquation mode_rgb, SGBlendEquation mode_alpha);

    private:
        Blend();
        ~Blend();
        void init(SettingData**);

        Blend(const Blend&) NONCOPYABLE;
        void operator=(const Blend&)NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } blend;

    class SG_PUBLIC_API Culling {
    public:
        void enable(bool enable);
        void on();
        void off();
        void face(SGFace f);
        void front(SGFrontFace f);

    private:
        Culling();
        ~Culling();
        void init(SettingData**);

        Culling(const Culling&) NONCOPYABLE;
        void operator=(const Culling&)NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } culling;

    class SG_PUBLIC_API DepthTest {
    public:
        void enable(bool enable);
        void on();
        void off();

        void depthFunc(SGFunction f);
        void mask(bool m);
        void range(float n, float f);

    private:
        DepthTest();
        ~DepthTest();
        void init(SettingData**);

        DepthTest(const DepthTest&) NONCOPYABLE;
        void operator=(const DepthTest&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } depthTest;

    class SG_PUBLIC_API Dither {
    public:
        void enable(bool enable);
        void on();
        void off();

    private:
        Dither();
        ~Dither();
        void init(SettingData**);

        Dither(const Dither&) NONCOPYABLE;
        void operator=(const Dither&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } dither;

    class SG_PUBLIC_API PolygonOffset {
    public:
        void enable(bool enable);
        void on();
        void off();

        void set(float factor, float units);

    private:
        PolygonOffset();
        ~PolygonOffset();
        void init(SettingData**);

        PolygonOffset(const PolygonOffset&) NONCOPYABLE;
        void operator=(const PolygonOffset&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } polygonOffset;

    class SG_PUBLIC_API PrimitiveRestartFixedIndex {
    public:
        void enable(bool enable);
        void on();
        void off();

    private:
        PrimitiveRestartFixedIndex();
        ~PrimitiveRestartFixedIndex();
        void init(SettingData**);

        PrimitiveRestartFixedIndex(const PrimitiveRestartFixedIndex&) NONCOPYABLE;
        void operator=(const PrimitiveRestartFixedIndex&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } primitiveRestartFixedIndex;

    class SG_PUBLIC_API SampleAlphaToCoverage {
    public:
        void enable(bool enable);
        void on();
        void off();

    private:
        SampleAlphaToCoverage();
        ~SampleAlphaToCoverage();
        void init(SettingData**);

        SampleAlphaToCoverage(const SampleAlphaToCoverage&) NONCOPYABLE;
        void operator=(const SampleAlphaToCoverage&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } sampleAlphaToCoverage;

    class SG_PUBLIC_API SampleCoverage {
    public:
        void enable(bool enable);
        void on();
        void off();

        void set(float value, bool invert);

    private:
        SampleCoverage();
        ~SampleCoverage();
        void init(SettingData**);

        SampleCoverage(const SampleCoverage&) NONCOPYABLE;
        void operator=(const SampleCoverage&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } sampleCoverage;

    class SG_PUBLIC_API Scissor {
    public:
        void enable(bool enable);
        void on();
        void off();

        void box(int32_t x, int32_t y, uint32_t width, uint32_t height);

    private:
        Scissor();
        void init(SettingData**);
        ~Scissor();

        Scissor(const Scissor&) NONCOPYABLE;
        void operator=(const Scissor&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } scissor;

    class SG_PUBLIC_API Stencil {
    public:
        void enable(bool enable);
        void on();
        void off();

        void func(SGFunction f, int32_t r, uint32_t m);
        void func(SGFace face, SGFunction f, int32_t r, uint32_t m);
        void op(SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass);
        void op(SGFace face, SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass);

    private:
        Stencil();
        ~Stencil();
        void init(SettingData**);

        Stencil(const Stencil&) NONCOPYABLE;
        void operator=(const Stencil&) NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } stencil;

    class SG_PUBLIC_API WireFrame {
    public:
        void enable(bool enable);
        bool isOn() const;
        void on();
        void off();
        void color(const Colorf& c);
        const Colorf& color() const;

    private:
        WireFrame();
        ~WireFrame();
        void init(SettingData**);

        WireFrame(const WireFrame&) NONCOPYABLE;
        void operator=(const WireFrame&)NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } wireframe;

    class SG_PUBLIC_API Lighting {
    public:
        void enable(bool enable);
        bool isOn() const;
        void on();
        void off();

    private:
        Lighting();
        ~Lighting();
        void init(SettingData**);

        Lighting(const Lighting&) NONCOPYABLE;
        void operator=(const Lighting&)NONCOPYABLE;

        friend class Setting;
        SettingData** mData;
    } lighing;

private:
    friend class Context;

    Setting();
    ~Setting();
    void init(SettingData**);

    Setting(const Setting&) NONCOPYABLE;
    void operator=(const Setting&) NONCOPYABLE;
};

}

#endif
