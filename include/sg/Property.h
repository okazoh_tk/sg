/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_PROPERTY_H_INCLUDED_
#define SG_PROPERTY_H_INCLUDED_

#include "sg/Config.h"
#include "sg/Texture.h"
#include "sg/MatrixStack.h"

namespace sg {

class Texture;

enum DataType {
    TYPE_BOOL = 0,
    TYPE_BVEC2,
    TYPE_BVEC3,
    TYPE_BVEC4,
    TYPE_INT,
    TYPE_IVEC2,
    TYPE_IVEC3,
    TYPE_IVEC4,
    TYPE_FLOAT,
    TYPE_VEC2,
    TYPE_VEC3,
    TYPE_VEC4,
    TYPE_MAT2,
    TYPE_MAT3,
    TYPE_MAT4,
    TYPE_MATRIX_STACK,
    TYPE_MVP_MATRIX = TYPE_MATRIX_STACK,
    TYPE_MV_MATRIX,
    TYPE_M_MATRIX,
    TYPE_TEXTURE,
};

class SG_PUBLIC_API Property {
public:

    virtual ~Property();
    bool isDirty() const;
    void clear();
    void dirty();
    DataType getType() const;
    uint32_t getCount() const;
    virtual const void* getPointer() const;

    Property* clone() const;
    bool copyFrom(const Property& src);

    void print(const char* key);

protected:
    Property(DataType type);

protected:
    uint32_t mDirty : 1;
    uint32_t mType : 5;
    uint32_t mCount : 26;
};

#define DECLARE_PROPERTY_PRIM(name, type, id) \
class SG_PUBLIC_API name##Property : public Property { \
public: \
    name##Property();\
    name##Property(type value);\
    name##Property(const name##Property& rhv);\
    virtual ~name##Property();\
    \
    virtual const void* getPointer() const;\
    \
    void operator=(type value);\
    void set(type value);\
    void set(const type* values, uint32_t count);\
    \
    operator type() const;\
    type get(uint32_t index) const;\
    const type* get() const;\
    type* getPointerForWrite();\
    \
private:\
    union {\
        type value;\
        type* pointer;\
    } mValue;\
}

#define DECLARE_PROPERTY(name, type, id) \
class SG_PUBLIC_API name##Property : public Property { \
public: \
    name##Property();\
    name##Property(const name##Property& rhv);\
    name##Property(const type& rhv);\
    virtual ~name##Property();\
    \
    virtual const void* getPointer() const;\
    \
    void operator=(const type& value);\
    void set(const type& value);\
    void set(const type* values, uint32_t count);\
    \
    operator const type() const;\
    const type get(uint32_t index) const;\
    const type* get() const;\
    type* getPointerForWrite();\
    \
private:\
    union {\
        type value;\
        type* pointer;\
    } mValue;\
}


DECLARE_PROPERTY_PRIM(Bool, bool, TYPE_BOOL);
DECLARE_PROPERTY(BoolVector2, BoolVector2, TYPE_BVEC2);
DECLARE_PROPERTY(BoolVector3, BoolVector3, TYPE_BVEC3);
DECLARE_PROPERTY(BoolVector4, BoolVector4, TYPE_BVEC4);
DECLARE_PROPERTY_PRIM(Int, int32_t, TYPE_INT);
DECLARE_PROPERTY(IntVector2, IntVector2, TYPE_IVEC2);
DECLARE_PROPERTY(IntVector3, IntVector3, TYPE_IVEC3);
DECLARE_PROPERTY(IntVector4, IntVector4, TYPE_IVEC4);
DECLARE_PROPERTY_PRIM(Float, float, TYPE_FLOAT);
DECLARE_PROPERTY(Vector2, Vector2, TYPE_VEC2);
DECLARE_PROPERTY(Vector3, Vector3, TYPE_VEC3);
DECLARE_PROPERTY(Vector4, Vector4, TYPE_VEC4);
DECLARE_PROPERTY(Matrix2, Matrix2, TYPE_MAT2);
DECLARE_PROPERTY(Matrix3, Matrix3, TYPE_MAT3);
DECLARE_PROPERTY(Matrix4, Matrix4, TYPE_MAT4);

class SG_PUBLIC_API MatrixStackProperty : public Property {
public:
    MatrixStackProperty();
    MatrixStackProperty(MatrixStack* stack);
    MatrixStackProperty(const MatrixStackProperty& rhv);
    virtual ~MatrixStackProperty();

    virtual const void* getPointer() const;

    void operator=(MatrixStack* value);
    void set(MatrixStack* value);

    operator MatrixStack*() const;
    MatrixStack* get() const;

protected:
    MatrixStackProperty(DataType type);
    MatrixStackProperty(DataType type, MatrixStack* stack);
    MatrixStackProperty(DataType type, const MatrixStackProperty& rhv);

private:
    MatrixStack::WeakRef mValue;
};

typedef MatrixStackProperty MVPMatrixProperty;

class SG_PUBLIC_API MVMatrixProperty : public MatrixStackProperty {
public:
    MVMatrixProperty();
    MVMatrixProperty(MatrixStack* stack);
    MVMatrixProperty(const MVMatrixProperty& rhv);
    virtual ~MVMatrixProperty();
};

class SG_PUBLIC_API MMatrixProperty : public MatrixStackProperty {
public:
    MMatrixProperty();
    MMatrixProperty(MatrixStack* stack);
    MMatrixProperty(const MMatrixProperty& rhv);
    virtual ~MMatrixProperty();
};

class SG_PUBLIC_API TextureProperty : public Property {
public:
    TextureProperty();
    TextureProperty(Texture* texture);
    TextureProperty(const TextureProperty& rhv);
    virtual ~TextureProperty();

    virtual const void* getPointer() const;

    void operator=(Texture* value);
    void set(Texture* value);

    operator Texture*() const;
    Texture* get() const;

private:
    Texture::WeakRef mValue;
};

}

#endif
