/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_EVENT_H_INCLUDED_
#define SG_EVENT_H_INCLUDED_

#include "sg/Config.h"
#include "sg/Device.h"

namespace sg {

class Context;
class EventDriver;
class EventDriverImpl;
class EventListenerChain;

class SG_PUBLIC_API KeyEvent {
public:

    enum Type {
        KEY_DOWN,
        KEY_UP,
    };

    KeyEvent(Type type, const Device::Key::State& state);
    ~KeyEvent();

    Type getType() const;
    int32_t getKey() const;
    bool isPressed() const;

private:
    KeyEvent(const KeyEvent&) NONCOPYABLE;
    void operator=(const KeyEvent&)NONCOPYABLE;

private:
    Type mType;
    Device::Key::State mState;
};


class SG_PUBLIC_API ButtonEvent {
public:
    enum Type {
        BUTTON_DOWN,
        BUTTON_UP,
    };

    ButtonEvent(Type type, const Device::Button::State& state);
    ~ButtonEvent();

    Type getType() const;
    int32_t getID() const;
    bool isPressed() const;

private:
    ButtonEvent(const ButtonEvent&) NONCOPYABLE;
    void operator=(const ButtonEvent&)NONCOPYABLE;

private:
    Type mType;
    const Device::Button::State& mState;
};


class SG_PUBLIC_API PointerEvent {
public:

    enum Type {
        POINTER_MOVE,
        POINTER_DOWN,
        POINTER_UP,
    };

    PointerEvent(Type type, const Device::Pointer::State& state);
    ~PointerEvent();

    Type getType() const;
    int32_t getID() const;

    int32_t getX() const;
    int32_t getY() const;

private:
    PointerEvent(const PointerEvent&) NONCOPYABLE;
    void operator=(const PointerEvent&)NONCOPYABLE;

private:
    Type mType;
    const Device::Pointer::State& mState;
};


class SG_PUBLIC_API ScrollEvent {
public:
    ScrollEvent(int32_t x_offset, int32_t y_offset);
    ~ScrollEvent();

    int32_t getX() const;
    int32_t getY() const;

private:
    int32_t mX, mY;
};


class SG_PUBLIC_API IEventListener {
protected:
    IEventListener();
    virtual ~IEventListener() = 0;

protected:
    void removeChain();

    EventListenerChain* getOwner();

private:
    void resetOwner(EventListenerChain* owner);

private:
    friend class EventListenerChain;
    EventListenerChain* mOwner;
};


class SG_PUBLIC_API KeyEventListener : public IEventListener {
public:
    KeyEventListener();
    virtual ~KeyEventListener() = 0;

    static const char* getName();

    virtual void onKeyEvent(EventDriver& caller, const KeyEvent& e) = 0;
};

class SG_PUBLIC_API ButtonEventListener : public IEventListener {
public:
    ButtonEventListener();
    virtual ~ButtonEventListener() = 0;

    static const char* getName();

    virtual void onButtonEvent(EventDriver& caller, const ButtonEvent& e) = 0;
};

class SG_PUBLIC_API PointerEventListener : public IEventListener {
public:
    PointerEventListener();
    virtual ~PointerEventListener() = 0;

    static const char* getName();

    virtual void onPointerEvent(EventDriver& caller, const PointerEvent& e) = 0;
};

class SG_PUBLIC_API UIEventListener : public IEventListener {
public:
    UIEventListener();
    virtual ~UIEventListener() = 0;

    static const char* getName();

    virtual void onScroll(EventDriver& caller, const ScrollEvent& e);
};

class SG_PUBLIC_API RendererEventListener : public IEventListener {
public:
    RendererEventListener();
    virtual ~RendererEventListener() = 0;

    static const char* getName();

    virtual void onUpdate(EventDriver& caller) = 0;
    virtual void onDraw(EventDriver& caller, Context& gl) = 0;
};

class SG_PUBLIC_API SystemEventListener : public IEventListener {
public:
    SystemEventListener();
    virtual ~SystemEventListener() = 0;

    static const char* getName();

    virtual void onLaunching(EventDriver& caller);
    virtual void onLaunched(EventDriver& caller) = 0;
    virtual void onSuspend(EventDriver& caller);
    virtual void onResume(EventDriver& caller);
    virtual void onDestroy(EventDriver& caller);
};

}

#endif
