/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_GRAPHICS_TYPES_H_INCLUDED_
#define SG_GRAPHICS_TYPES_H_INCLUDED_

#include "sg/Config.h"

namespace sg {

struct SG_PUBLIC_API Format {

    enum Component : uint32_t {
        // ES 2.0
        RGB = 0x0000,
        RGBA = 0x0100,
        LUMINANCE = 0x0200,
        LUMINANCE_ALPHA = 0x0300,
        ALPHA = 0x0400,

        // OES_depth_texture
        DEPTH_COMPONENT = 0x0500,

        // ES 3.0
        RED = 0x0600,
        RED_INTEGER = 0x0700,
        RG = 0x0800,
        RG_INTEGER = 0x0900,
        RGB_INTEGER = 0x0A00,
        RGBA_INTEGER = 0x0B00,
        DEPTH_STENCIL = 0x0C00,
    };

    enum Type : uint32_t {
        // ES 2.0
        BYTE = 0x0000,
        UNSIGNED_SHORT_5_6_5 = 0x0001,
        UNSIGNED_SHORT_4_4_4_4 = 0x0002,
        UNSIGNED_SHORT_5_5_5_1 = 0x0003,

        // OES_depth_texture
        UNSIGNED_SHORT = 0x0004,
        UNSIGNED_INT = 0x0005,

        // ES 3.0
        UNSIGNED_BYTE = 0x0006,
        SHORT = 0x0007,
        INT = 0x0008,
        HALF_FLOAT = 0x0009,
        FLOAT = 0x000A,
        UNSIGNED_INT_2_10_10_10_REV = 0x000B,
        UNSIGNED_INT_10F_11F_11F_REV = 0x000C,
        UNSIGNED_INT_5_9_9_9_REV = 0x000D,
        UNSIGNED_INT_24_8 = 0x000E,
        FLOAT_32_UNSIGNED_INT_24_8_REV = 0x000F,
    };
};

enum PixelFormat : uint32_t {
    SG_A8 = Format::ALPHA | Format::BYTE,      // H [AAAA AAAA] L
    SG_RGB_565 = Format::RGB | Format::UNSIGNED_SHORT_5_6_5,  // H [BBBB BGGG] [GGGR RRRR] L
    SG_RGBA_4444 = Format::RGBA | Format::UNSIGNED_SHORT_4_4_4_4,   // H [AAAA BBBB] [GGGG RRRR] L
    SG_RGBA_5551 = Format::RGBA | Format::UNSIGNED_SHORT_5_5_5_1,   // H [ABBB BBGG] [GGGR RRRR] L
    SG_RGBA_8888 = Format::RGBA | Format::BYTE,   // H [AAAA AAAA] [BBBB BBBB] [GGGG GGGG][RRRR RRRR] L
    SG_DEPTH = Format::DEPTH_COMPONENT | Format::UNSIGNED_SHORT,
};

}

#endif
