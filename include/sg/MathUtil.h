/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_MATH_H_INCLUDED_
#define SG_MATH_H_INCLUDED_

#include "sg/Config.h"

namespace sg {

struct SG_PUBLIC_API Vector2 {
    SG_DECLARE_POD(Vector2);

    static Vector2 make(float x, float y);
    static Vector2 makeNormalized(float x, float y);
    static Vector2 lerp(const Vector2& s, const Vector2& e, float t);

    void normalize();

    Vector2 operator+(const Vector2& rhv) const;
    Vector2 operator-(const Vector2& rhv) const;
    float dot(const Vector2& rhv) const;

    float x;
    float y;
};

struct SG_PUBLIC_API Vector3 {
    SG_DECLARE_POD(Vector3);

    static Vector3 make(float x, float y, float z);
    static Vector3 makeNormalized(float x, float y, float z);
    static Vector3 lerp(const Vector3& s, const Vector3& e, float t);

    void normalize();

    Vector3 operator+(const Vector3& rhv) const;
    const Vector3& operator+=(const Vector3& rhv);
    Vector3 operator-(const Vector3& rhv) const;
    const Vector3& operator-=(const Vector3& rhv);

    Vector3 operator*(float rhv) const;
    const Vector3& operator*=(float rhv);

    Vector3 cross(const Vector3& rhv) const;
    float dot(const Vector3& rhv) const;

    float x;
    float y;
    float z;
};

struct SG_PUBLIC_API Vector4 {
    SG_DECLARE_POD(Vector4);

    static Vector4 make(float x, float y, float z, float w);
    static Vector4 makeNormalized(float x, float y, float z, float w);
    static Vector4 lerp(const Vector4& s, const Vector4& e, float t);

    void normalize();

    Vector4 operator+(const Vector4& rhv) const;
    Vector4 operator-(const Vector4& rhv) const;
    float dot(const Vector4& rhv) const;

    float x;
    float y;
    float z;
    float w;
};

struct SG_PUBLIC_API Quaternion {
    SG_DECLARE_POD(Quaternion);

    static Quaternion make(float w, float x, float y, float z);

    void normalize();
    void inverse();

    static Quaternion rotate(float angle, float x, float y, float z);
    static Quaternion rotate(float pitch, float yaw, float roll);

    static Quaternion lerp(const Quaternion& s, const Quaternion& e, float t);
    static Quaternion slerp(const Quaternion& s, const Quaternion& e, float t);

    Quaternion operator*(const Quaternion& rhv) const;
    const Quaternion& operator*=(const Quaternion& rhv);
    Vector3 operator*(const Vector3& rhv) const;
    void eulerAngles(float* pitch, float* yaw, float* roll) const;

    float x;
    float y;
    float z;
    float w;
};

struct BoolVector2 { bool x, y; };
struct BoolVector3 { bool x, y, z; };
struct BoolVector4 { bool x, y, z, w; };
struct IntVector2 { int32_t x, y; };
struct IntVector3 { int32_t x, y, z; };
struct IntVector4 { int32_t x, y, z, w; };
struct Matrix2 { float m[2 * 2]; };

struct SG_PUBLIC_API Matrix3 {
    float m[3 * 3];
};

struct SG_PUBLIC_API Matrix4 {

    static Matrix4 make(const float m[4*4]);

    void identity();
    void inverse();
    void transpose();

    float m[4 * 4];
};

typedef Vector3 Position3;

enum ProjectionType {
    SG_PERSPECTIVE,
    SG_FRUSTUM,
    SG_ORTHO,
};

struct Perspective {
    float fovy, aspect, znear, zfar;
};

struct Frustum {
    float left, right, bottom, top, znear, zfar;
};

struct Ortho {
    float left, right, bottom, top, znear, zfar;
};

inline void dprint(const char* label, const Quaternion& quat, bool euler_angles = false)
{
    (void)label;
    (void)quat;
    (void)euler_angles;
    if (euler_angles) {
        float p, y, r;
        quat.eulerAngles(&p, &y, &r);
        LOGD("%s: (pitch, yaw, roll)=(%f, %f, %f)",
            label,
            p, y, r);
    } else {
        LOGD("%s: (w, x, y, z)=(%f, %f, %f, %f)",
            label,
            quat.w, quat.x, quat.y, quat.z);
    }
}

inline void dprint(const char* label, const Vector3& vec)
{
    (void)label;
    (void)vec;

    LOGD("%s: (x, y, z)=(%f, %f, %f)",
        label,
        vec.x, vec.y, vec.z);
}

inline void dprint(const char* label, const Vector4& vec)
{
    (void)label;
    (void)vec;

    LOGD("%s: (x, y, z, w)=(%f, %f, %f, %f)",
        label,
        vec.x, vec.y, vec.z, vec.w);
}

}
#endif
