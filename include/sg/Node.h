/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_NODE_H_INCLUDED_
#define SG_NODE_H_INCLUDED_

#include "sg/Config.h"

namespace sg {

class Context;

class SG_PUBLIC_API Node {
public:
    Node();
    virtual ~Node() = 0;

    Node* getParent() const;
    Node* getPrevSibling() const;
    Node* getNextSibling() const;
    Node* getFirstChild() const;
    Node* getLastChild() const;

    void addChild(Node& child);
    void removeChild(Node& child);

    bool update();
    void updatePost();
    bool draw(Context& gl);
    void drawPost(Context& gl);

protected:
    virtual bool onUpdate();
    virtual void onUpdatePost();
    virtual bool onDraw(Context& gl);
    virtual void onDrawPost(Context& gl);

private:
    Node* mParent;
    Node* mPrevSibling;
    Node* mNextSibling;
    Node* mFirstChild;
    Node* mLastChild;
};

class SG_PUBLIC_API NodeVisitor {
public:
    NodeVisitor();
    virtual ~NodeVisitor() = 0;

    void traverse(Node& root);

protected:
    virtual bool visit(Node& node) = 0;
    virtual void visitPost(Node& node) = 0;
};

class SG_PUBLIC_API NodeUpdater : public NodeVisitor {
public:
    NodeUpdater();
    virtual ~NodeUpdater();

    static void updateTree(Node& root);

protected:
    virtual bool visit(Node& node);
    virtual void visitPost(Node& node);
};

class SG_PUBLIC_API NodeRenderer : public NodeVisitor {
public:
    NodeRenderer(Context& gl);
    virtual ~NodeRenderer();

    static void drawTree(Context& gl, Node& root);

protected:
    virtual bool visit(Node& node);
    virtual void visitPost(Node& node);

private:
    Context* mContext;
};

class SG_PUBLIC_API NodeDestroyer : public NodeVisitor {
public:
    NodeDestroyer();
    virtual ~NodeDestroyer();

    static void destroyAfterRemove(Node& root);

protected:
    virtual bool visit(Node& node);
    virtual void visitPost(Node& node);
};

}

#endif
