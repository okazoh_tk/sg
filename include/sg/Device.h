/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_DEVICE_H_INCLUDED_
#define SG_DEVICE_H_INCLUDED_

#include "sg/Config.h"


enum ButtonID {
    SG_BUTTON_0 = 0,
    SG_BUTTON_1 = 1,
    SG_BUTTON_2 = 2,
    SG_BUTTON_3 = 3,
    SG_BUTTON_4 = 4,
    SG_BUTTON_5 = 5,
    SG_BUTTON_6 = 6,
    SG_BUTTON_7 = 7,

    SG_MOUSE_BUTTON_LEFT = SG_BUTTON_0,
    SG_MOUSE_BUTTON_RIGHT = SG_BUTTON_1,
    SG_MOUSE_BUTTON_MIDDLE = SG_BUTTON_0,
};

enum VirtualKeyCode : uint32_t {
    SG_VK_UNKNOWN       = 0x00,
    //SG_VK_            = 0x01,
    //SG_VK_            = 0x02,
    //SG_VK_            = 0x03,
    //SG_VK_            = 0x04,
    //SG_VK_            = 0x05,
    //SG_VK_            = 0x06,
    //SG_VK_            = 0x07,
    SG_VK_BACKSPACE     = 0x08,
    SG_VK_TAB           = 0x09,
    //SG_VK_            = 0x0A,
    //SG_VK_            = 0x0B,
    //SG_VK_            = 0x0C,
    SG_VK_ENTER         = 0x0D,
    //SG_VK_            = 0x0E,
    //SG_VK_            = 0x0F,
    //SG_VK_            = 0x10,
    //SG_VK_            = 0x11,
    //SG_VK_            = 0x12,
    //SG_VK_            = 0x13,
    //SG_VK_            = 0x14,
    //SG_VK_            = 0x15,
    //SG_VK_            = 0x16,
    //SG_VK_            = 0x17,
    //SG_VK_            = 0x18,
    //SG_VK_            = 0x19,
    //SG_VK_            = 0x1A,
    SG_VK_ESCAPE        = 0x1B,
    //SG_VK_            = 0x1C,
    //SG_VK_            = 0x1D,
    //SG_VK_            = 0x1E,
    //SG_VK_            = 0x1F,
    SG_VK_SPACE         = 0x20,
    //SG_VK_            = 0x21,
    //SG_VK_            = 0x22,
    //SG_VK_            = 0x23,
    //SG_VK_            = 0x24,
    //SG_VK_            = 0x25,
    //SG_VK_            = 0x26,
    //SG_VK_            = 0x27,
    //SG_VK_            = 0x28,
    //SG_VK_            = 0x29,
    //SG_VK_            = 0x2A,
    //SG_VK_            = 0x2B,
    //SG_VK_            = 0x2C,
    //SG_VK_            = 0x2D,
    //SG_VK_            = 0x2E,
    //SG_VK_            = 0x2F,
    SG_VK_0             = 0x30,
    SG_VK_1             = 0x31,
    SG_VK_2             = 0x32,
    SG_VK_3             = 0x33,
    SG_VK_4             = 0x34,
    SG_VK_5             = 0x35,
    SG_VK_6             = 0x36,
    SG_VK_7             = 0x37,
    SG_VK_8             = 0x38,
    SG_VK_9             = 0x39,
    //SG_VK_            = 0x3A,
    //SG_VK_            = 0x3B,
    //SG_VK_            = 0x3C,
    //SG_VK_            = 0x3D,
    //SG_VK_            = 0x3E,
    //SG_VK_            = 0x3F,
    //SG_VK_            = 0x40,
    SG_VK_A             = 0x41,
    SG_VK_B             = 0x42,
    SG_VK_C             = 0x43,
    SG_VK_D             = 0x44,
    SG_VK_E             = 0x45,
    SG_VK_F             = 0x46,
    SG_VK_G             = 0x47,
    SG_VK_H             = 0x48,
    SG_VK_I             = 0x49,
    SG_VK_J             = 0x4A,
    SG_VK_K             = 0x4B,
    SG_VK_L             = 0x4C,
    SG_VK_M             = 0x4D,
    SG_VK_N             = 0x4E,
    SG_VK_O             = 0x4F,
    SG_VK_P             = 0x50,
    SG_VK_Q             = 0x51,
    SG_VK_R             = 0x52,
    SG_VK_S             = 0x53,
    SG_VK_T             = 0x54,
    SG_VK_U             = 0x55,
    SG_VK_V             = 0x56,
    SG_VK_W             = 0x57,
    SG_VK_X             = 0x58,
    SG_VK_Y             = 0x59,
    SG_VK_Z             = 0x5A,
    //SG_VK_            = 0x5B,
    //SG_VK_            = 0x5C,
    //SG_VK_            = 0x5D,
    //SG_VK_            = 0x5E,
    //SG_VK_            = 0x5F,
    //SG_VK_            = 0x60,
    //SG_VK_            = 0x61,
    //SG_VK_            = 0x62,
    //SG_VK_            = 0x63,
    //SG_VK_            = 0x64,
    SG_VK_PAGE_UP       = 0x65,
    SG_VK_PAGE_DOWN     = 0x66,
    SG_VK_F1            = 0x67,
    SG_VK_F2            = 0x68,
    SG_VK_F3            = 0x69,
    SG_VK_F4            = 0x70,
    SG_VK_F5            = 0x71,
    SG_VK_F6            = 0x72,
    SG_VK_F7            = 0x73,
    SG_VK_F8            = 0x74,
    SG_VK_F9            = 0x75,
    SG_VK_F10           = 0x76,
    SG_VK_F11           = 0x77,
    SG_VK_F12           = 0x78,
    //SG_VK_            = 0x79,
    //SG_VK_            = 0x7A,
    SG_VK_LEFT          = 0x7B,
    SG_VK_RIGHT         = 0x7C,
    SG_VK_DOWN          = 0x7D,
    SG_VK_UP            = 0x7E,
    //SG_VK_            = 0x7B,
    //SG_VK_            = 0x7C,
    //SG_VK_            = 0x7D,
    //SG_VK_            = 0x7E,
    //SG_VK_            = 0x7F,

    SG_VK_LEFT_SHIFT    = 0x80,
    SG_VK_LEFT_CONTROL  = 0x81,
    SG_VK_LEFT_ALT      = 0x82,
    SG_VK_RIGHT_SHIFT   = 0x83,
    SG_VK_RIGHT_CONTROL = 0x84,
    SG_VK_RIGHT_ALT     = 0x85,
    //SG_VK_            = 0x86,
    //SG_VK_            = 0x87,
    //SG_VK_            = 0x88,
    //SG_VK_            = 0x89,
    //SG_VK_            = 0x8A,
    //SG_VK_            = 0x8B,
    //SG_VK_            = 0x8C,
    //SG_VK_            = 0x8D,
    //SG_VK_            = 0x8E,
    //SG_VK_            = 0x8F,

    SG_VK_MAX,
};

namespace sg {

class IDevice;
class IPointer;
class IButton;
class IKey;

struct PointerState;
struct ButtonState;
struct KeyState;

class SG_PUBLIC_API Device {
public:

    class SG_PUBLIC_API Pointer {
    public:

        bool isSupported() const;

        class SG_PUBLIC_API State {
        public:
            struct Position {
                int32_t x, y;
            };

            State(const State& rhv);
            ~State();

            int32_t getID() const;
            int32_t getX() const;
            int32_t getY() const;
            Position getPosition() const;

            bool isSupported() const;

        private:
            friend class Pointer;
            State(const PointerState* state);
            void operator=(const State& rhv) NONCOPYABLE;

        private:
            const PointerState* mState;
        };

        State operator()(int32_t id) const;

    private:
        friend class Device;

        Pointer(IPointer* ifc);
        ~Pointer();

    private:
        IPointer* mInterface;
    } pointer;

    class SG_PUBLIC_API Button {
    public:

        class SG_PUBLIC_API State {
        public:
            State(const State& rhv);
            ~State();

            int32_t getID() const;
            bool isSupported() const;
            bool isPressed() const;
            bool isReleased() const;

        private:
            friend class Button;
            State(const ButtonState* state);
            void operator=(const State& rhv) NONCOPYABLE;

        private:
            const ButtonState* mState;
        };

        bool isSupported() const;

        State operator()(int32_t id) const;

    private:
        friend class Device;

        Button(IButton* ifc);
        ~Button();

    private:
        IButton* mInterface;
    } button;

    class SG_PUBLIC_API Key {
    public:
        bool isSupported() const;

        class SG_PUBLIC_API State {
        public:
            State(const State& rhv);
            ~State();

            int32_t getChar() const;
            bool isSupported() const;
            bool isPressed() const;
            bool isReleased() const;

        private:
            friend class Key;
            State(const KeyState* state);
            void operator=(const State& rhv) NONCOPYABLE;

        private:
            const KeyState* mState;
        };

        State operator()(int32_t ch) const;

    private:
        friend class Device;

        Key(IKey* ifc);
        ~Key();

    private:
        IKey* mInterface;
    } key;

    Device(IDevice* ifc);
    ~Device();
};

}

#endif
