/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_PLATFORM_H_INCLUDED_
#define SG_PLATFORM_H_INCLUDED_

/* OS switch
*/
#if defined(__APPLE__)
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR == 1
// iOS simulator
#define ON_IOS(fn) fn()
#define TARGET_OS "iOS"
#define TARGET_OS_IOS 1
#elif TARGET_OS_IPHONE == 1
// iOS
#define ON_IOS(fn) fn()
#define TARGET_OS "iOS"
#define TARGET_OS_IOS 1
#else
// Mac OS X
#define ON_MAC_X(fn) fn()
#define TARGET_OS "Mac OS X"
#endif

#define ON_MAC(fn) fn()

#ifndef TARGET_OS_MAC
#define TARGET_OS_MAC 1
#endif

#elif defined(_WIN32) && (!defined(__CYGWIN__) && !defined(__CYGWIN32__))
#ifdef _WIN64
#define ON_WIN64(fn) fn()
#define TARGET_OS "Windows(64bit)"
#else
#define ON_WIN32(fn) fn()
#define TARGET_OS "Windows(32bit)"
#endif

#define ON_WIN(fn) fn()

#define TARGET_OS_WIN 1

#elif defined(__ANDROID__)

#define ON_ANDROID(fn) fn()
#define ON_LINUX(fn) fn()
#define TARGET_OS_ANDROID 1
#define TARGET_OS_LINUX 1
#define TARGET_OS "Android"
#elif defined(__linux)
// Linux
#define ON_LINUX(fn) fn()
#define TARGET_OS_LINUX 1
#define TARGET_OS "Linux"
#elif defined(__unix) || defined(__unix__)
// Unix
#define ON_UNIX(fn) fn()
#define TARGET_OS_UNIX 1
#define TARGET_OS "Unix"
#else
#define TARGET_OS_UNKNOWN
#define TARGET_OS "Unknown"
#endif


#ifndef ON_IOS
#define ON_IOS(fn) do{}while(0)
#endif
#ifndef ON_MAC_X
#define ON_MAC_X(fn) do{}while(0)
#endif
#ifndef ON_MAC
#define ON_MAC(fn) do{}while(0)
#endif
#ifndef ON_WIN
#define ON_WIN(fn) do{}while(0)
#endif
#ifndef ON_WIN32
#define ON_WIN32(fn) do{}while(0)
#endif
#ifndef ON_WIN64
#define ON_WIN64(fn) do{}while(0)
#endif
#ifndef ON_LINUX
#define ON_LINUX(fn) do{}while(0)
#endif
#ifndef ON_ANDROID
#define ON_ANDROID(fn) do{}while(0)
#endif
#ifndef ON_UNIX
#define ON_UNIX(fn) do{}while(0)
#endif

/* CPU switch
*/
#if defined(__i386__) || defined(__i386) || defined(_M_IX86) || defined(_X86_)
#define TARGET_CPU_BIT 32
#define TARGET_CPU_X86
#define TARGET_CPU "x86"
#elif defined(__amd64__) || defined(_M_AMD64)
#define TARGET_CPU_BIT 64
#define TARGET_CPU_AMD64
#define TARGET_CPU "AMD64"
#elif defined(__ia64__) || defined(_M_IA64) || defined(__x86_64__)
#define TARGET_CPU_BIT 64
#define TARGET_CPU_IA64
#define TARGET_CPU "IA64"
#elif defined(__thumb__) || defined(_M_ARMT)
#define TARGET_CPU_BIT 32
#define TARGET_CPU_ARMT
#define TARGET_CPU "ARM(thumb)"
#elif defined(__aarch64__)
#define TARGET_CPU_BIT 64
#define TARGET_CPU_ARM64
#define TARGET_CPU "ARM64"
#elif defined(__arm__) || defined(__TARGET_ARCH_ARM) || defined(_M_ARM)
#define TARGET_CPU_BIT 32
#define TARGET_CPU_ARM
#define TARGET_CPU "ARM"
#elif defined(__ppc64__)
#define TARGET_CPU_BIT 64
#define TARGET_CPU_PPC
#define TARGET_CPU "PowerPC(64bit)"
#elif defined(__ppc__) || defined(_M_PPC)
#define TARGET_CPU_BIT 32
#define TARGET_CPU_PPC
##define TARGET_CPU "PowerPC(32bit)"
elif defined(__mips__) || defined(__mips)
#define TARGET_CPU_BIT 32
#define TARGET_CPU_MIPS
#define TARGET_CPU "MIPS"
#else
#define TARGET_CPU_BIT 32
#define TARGET_CPU_UNKNOWN
#define TARGET_CPU "Unknown"
#endif


/* standard type definitions.
*/
#if defined(_MSC_VER) && (_MSC_VER <= 1600)
typedef __int8 int8_t;
typedef unsigned __int8 uint8_t;
typedef __int16 int16_t;
typedef unsigned __int16 uint16_t;
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;

#ifdef _WIN64
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;
#else
typedef int32_t intptr_t;
typedef uint32_t uintptr_t;
#endif

#else
#include <stdint.h>
#endif


#if defined(TARGET_OS_WIN)
#define __func__ __FUNCTION__
#endif

/* Compiler
*/
#if __cplusplus >= 201103L || (defined(TARGET_OS_WIN) && (_MSC_VER >= 1800))
#define NONCOPYABLE =delete
#else
#define NONCOPYABLE
#endif

/* visibility.
*/
#if defined(UTILS_USED_IN_DLL) && (__GNUC__ >= 4 || defined(__clang__))
#define UTILS_DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#else
#define UTILS_DLL_LOCAL
#endif

/* debug.
 */
#if !defined(_DEBUG) && !defined(NDEBUG)
#define NDEBUG
#endif

/* compile validator.
*/
#if defined(VALIDATE_PLATFORM)

#if defined(TARGET_OS_UNKNOWN)
#error No Support: Unknown OS.
#endif

#if defined(TARGET_CPU_UNKNOWN)
#error No Support: Unknown CPU Architecture.
#endif

#define STRING2(x) #x
#define STRING(x) STRING2(x)

#if defined(_MSC_VER)
#pragma message("[Info] Compiling under MSVC...")
#pragma message("[Info] Target OS  : " TARGET_OS)
#pragma message("[Info] Target CPU : " TARGET_CPU "/" STRING(TARGET_CPU_BIT))
#else
#if defined(__clang__)
#pragma message "[Info] Compiling under clang..."
#elif defined(__GNU__)
#pragma message "[Info] Compiling under GCC..."
#else
#pragma message "[Info] Compiling under unknown compiler..."
#endif
#pragma message "[Info] Target OS  : " TARGET_OS
#pragma message "[Info] Target CPU : " TARGET_CPU "/" STRING(TARGET_CPU_BIT)
#endif

#undef STRING
#undef STRING2

#endif

#endif
