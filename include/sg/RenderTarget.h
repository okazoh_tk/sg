/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_RENDER_TARGET_H_INCLUDED_
#define SG_RENDER_TARGET_H_INCLUDED_

#include "sg/Config.h"
#include "sg/SharedObject.h"
#include "sg/Texture.h"

namespace sg {

class RenderBuffer;

#define SG_MAX_COLOR_BUFFER 8

class SG_PUBLIC_API RenderTarget : public SharedObject {
public:

    struct SG_PUBLIC_API Parameter {
    public:
        Parameter();
        Parameter(const Size& size);
        Parameter(const Size& size, Texture::Format color_format);
        Parameter(const Size& size, Texture::Format color_format, bool depth_as_texture);
        Parameter(const Parameter& rhv);

        Size size;
        struct {
            uint8_t num;
            struct {
                Texture::Format format;
            } buffers[SG_MAX_COLOR_BUFFER];
        } color;

        struct {
            bool enable;
            bool as_texture;
        } depth;

        struct {
            bool enable;
        } stencil;
    };

    RenderTarget(const Parameter& param);
    virtual ~RenderTarget();

    void clear();

    void bind();
    void unbind();

    void prepare();

    const Parameter& getParameter() const;
    Texture* getColorBuffer(uint8_t index = 0);
    Texture* getDepthBuffer();

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(RenderTarget* target = nullptr);
        WeakRef(const WeakRef& rhv);
        virtual ~WeakRef();

        RenderTarget* operator->();
        void operator=(const WeakRef& rhv);
        void operator=(RenderTarget* target);

        operator RenderTarget*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        RenderTarget* mTarget;
    };

private:
    Parameter mParam;
    uint32_t mName;
    RenderBuffer* mColorBuffers;
    RenderBuffer* mDepthBuffer;
    RenderBuffer* mStencilBuffer;
};

}

#endif
