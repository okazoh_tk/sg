/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_CONFIG_H_INCLUDED_
#define SG_CONFIG_H_INCLUDED_

#include "sg/Platform.h"

#if defined(_WIN32) || defined(__CYGWIN__)
#ifdef SG_COMPILE_AS_DLL
#ifdef __GNUC__
#define SG_PUBLIC_API __attribute__ ((dllexport))
#else
#define SG_PUBLIC_API __declspec(dllexport)
#endif
#else
#ifdef __GNUC__
#define SG_PUBLIC_API __attribute__ ((dllimport))
#else
#define SG_PUBLIC_API __declspec(dllimport)
#endif
#endif
#define SG_LOCAL_API
#else
#if __GNUC__ >= 4 || __clang__
#define SG_PUBLIC_API __attribute__ ((visibility ("default")))
#define SG_LOCAL_API  __attribute__ ((visibility ("hidden")))
#else
#define SG_PUBLIC_API
#define SG_LOCAL_API
#endif
#endif


#if 0
/* Select OpenGL for Android.
 */
#if defined(TARGET_OS_ANDROID)

#include <android/api-level.h>
#if __ANDROID_API__ >= 21
#define SG_OPENGL_ES 31
#elif __ANDROID_API__ >= 18
#define SG_OPENGL_ES 3
#elif __ANDROID_API__ >= 8
#define SG_OPENGL_ES 2
#else
// invalid platform
#endif

/* iOS version.
 */
#elif defined(TARGET_OS_IOS)

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000 // 7.0 or over
#define SG_OPENGL_ES 3
#else
#define SG_OPENGL_ES 2
#endif

/* Windows version.
 */
#elif defined(TARGET_OS_WIN)

#include <Windows.h>
#ifndef ENABLE_GLFW
#define ENABLE_GLFW 1
#endif

#if defined(_MSC_VER) && (_MSC_VER >= 1700)
#define GLM_FORCE_CXX03
#endif

#elif defined(TARGET_OS_MAC)

#ifndef ENABLE_GLFW
#define ENABLE_GLFW 1
#endif

/* Other platform version
 */
#else

#endif


#ifndef ENABLE_GLFW
#define ENABLE_GLFW 0
#endif
#ifndef ENABLE_WIN32WINDOW
#define ENABLE_WIN32WINDOW 0
#endif


#if defined(SG_OPENGL_ES)
#define ON_GLES(exp) [&](){exp;}()
#define ON_GL(exp)   ((void)0)
#else
#define ON_GLES(exp) ((void)0)
#define ON_GL(exp)   [&](){exp;}()
#endif

#endif

//#define SG_DETECT_MEMORY_LEAK
#if defined(TARGET_OS_WIN) && defined(SG_DETECT_MEMORY_LEAK)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#if !defined(TARGET_OS_WIN)
#define strcpy_s(d, d_len, s)   strcpy(d, s)
#endif

#if __cplusplus >= 201103L
#define SG_DECLARE_POD(type)    type() = default
#else
#define SG_DECLARE_POD(type)
#endif

#ifndef COUNT_OF
#define COUNT_OF(a)     (sizeof(a)/sizeof(a[0]))
#endif

namespace sg {

enum Error : int32_t {
    SG_NO_ERROR = 0,

    //
    SG_ERROR_INVALID_ARGUMENT = 1000,
    SG_ERROR_INVALID_OPERATION,
    SG_ERROR_NO_BUILDER,
    SG_ERROR_INIT_BUILDER,
};

struct SG_PUBLIC_API Size {
    SG_DECLARE_POD(Size);

    uint16_t width;
    uint16_t height;
};


struct SG_PUBLIC_API Viewport {
    SG_DECLARE_POD(Viewport);

    int16_t x, y;
    uint16_t width, height;
};

struct SG_PUBLIC_API Point {
    SG_DECLARE_POD(Point);

    int16_t x;
    int16_t y;
};

struct SG_PUBLIC_API Rect {
    SG_DECLARE_POD(Rect);

    Point point;
    Size size;
};

struct SG_PUBLIC_API Color {
    SG_DECLARE_POD(Color);

    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

struct SG_PUBLIC_API Colorf {
    SG_DECLARE_POD(Colorf);

    float r;
    float g;
    float b;
    float a;
};

}
#include "sg/Log.h"

#endif
