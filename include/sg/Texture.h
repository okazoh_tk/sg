/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_TEXTURE_H_INCLUDED_
#define SG_TEXTURE_H_INCLUDED_

#include "sg/Config.h"
#include "sg/SharedObject.h"
#include "sg/GraphicsTypes.h"

namespace sg {

class Context;
class MemoryAllocator;


class SG_PUBLIC_API Texture : public SharedObject {
public:

    /* Internal format */
    enum Format {
        RGB = 0,
        RGBA,
        ALPHA,
        LUMINANCE,
        LUMINANCE_ALPHA,
        DEPTH,
    };

    Texture();
    virtual ~Texture();

    void set(
        uint16_t width,
        uint16_t height,
        Format internal_format);

    void set(
        uint16_t width,
        uint16_t height,
        Format internal_format,
        PixelFormat pixel_format);

    void set(
        uint16_t width,
        uint16_t height,
        Format internal_format,
        void* pixels,
        PixelFormat pixel_format,
        MemoryAllocator* release_delegate = nullptr);

    uint16_t getWidth() const;
    uint16_t getHeight() const;
    Format getFormat() const;
    uint32_t getName() const;

    void bind();
    void unbind();

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(Texture* texture = nullptr);
        WeakRef(const WeakRef& rhv);
        virtual ~WeakRef();

        Texture* operator->();
        void operator=(const WeakRef& rhv);
        void operator=(Texture* texture);

        operator Texture*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        Texture* mTexture;
    };

#if 0
    // Texture
    void ActiveTexture (GLenum texture);
    void BindTexture (GLenum target, GLuint texture);
    void CompressedTexImage2D (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data);
    void CompressedTexSubImage2D (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data);
    void CopyTexImage2D (GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border);
    void CopyTexSubImage2D (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);
    void DeleteTextures (GLsizei n, const GLuint *textures);
    void GenTextures (GLsizei n, GLuint *textures);
    void GetTexParameterfv (GLenum target, GLenum pname, GLfloat *params);
    void GetTexParameteriv (GLenum target, GLenum pname, GLint *params);
    void GetVertexAttribfv (GLuint index, GLenum pname, GLfloat *params);
    void GetVertexAttribiv (GLuint index, GLenum pname, GLint *params);
    void GetVertexAttribPointerv (GLuint index, GLenum pname, void **pointer);
    void TexImage2D (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels);
    void TexParameterf (GLenum target, GLenum pname, GLfloat param);
    void TexParameterfv (GLenum target, GLenum pname, const GLfloat *params);
    void TexParameteri (GLenum target, GLenum pname, GLint param);
    void TexParameteriv (GLenum target, GLenum pname, const GLint *params);
    void TexSubImage2D (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels);
    void PixelStorei (GLenum pname, GLint param);
    void GenerateMipmap (GLenum target);
#endif

private:
    uint32_t mName;
    uint16_t mWidth, mHeight;
    Format mFormat;
    void* mPixels;
    uint32_t mPixelFormat;
    MemoryAllocator* mReleaseDelegate;
    bool mNeedSync;
};

}

#endif
