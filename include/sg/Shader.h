/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_SHADER_H_INCLUDED_
#define SG_SHADER_H_INCLUDED_

#include "sg/Config.h"
#include "sg/System.h"

namespace sg {

class MemoryAllocator;

class SG_PUBLIC_API Shader {
public:

    enum ShaderType {
        VERTEX_SHADER,
        FRAGMENT_SHADER
    };

    static const char* HEADER_PRECISION;
    static const char* HEADER_PRECISION_HIGH;
    static const char* HEADER_PRECISION_MEDIAM;
    static const char* HEADER_PRECISION_LOW;

    Shader(ShaderType type, const char* source);
    Shader(ShaderType type, const char* source[], uint32_t num);
    Shader(ShaderType type, const FilePath& filepath);
    ~Shader();

    uint32_t getName() const;

    bool prepare();

    static uint32_t compile(ShaderType type, const char* source, uint32_t source_len);
    static void destroy(uint32_t name);

protected:
    void setAllocatedSource(const char* source);

private:
    uint32_t mName;
    ShaderType mType;
    const char* mSource;
};

class SG_PUBLIC_API VertexShader : public Shader {
public:
    VertexShader(const char* source);
    VertexShader(const char* sources[], uint32_t num);
    VertexShader(const FilePath& filepath);
};

class SG_PUBLIC_API FragmentShader : public Shader {
public:
    FragmentShader(const char* source);
    FragmentShader(const char* sources[], uint32_t num);
    FragmentShader(const FilePath& filepath);
};

}

#endif
