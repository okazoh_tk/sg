/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_APPLICATION_INCLUDED_
#define SG_APPLICATION_INCLUDED_

#include "sg/Config.h"
#include "sg/Context.h"
#include "sg/Event.h"

namespace sg {

class Builder;
class ApplicationImpl;
class CameraActor;

class SG_PUBLIC_API Application {
public:
    Application();
    Application(const Size& window_size);
    Application(int argc, char** argv);
    Application(const Size& window_size, int argc, char** argv);
    virtual ~Application();

    int32_t run();

    void exit();

    void setBuilder(Builder* builder);

    void addActor(CameraActor* actor);

    float getFps() const;

    void addEventListener(KeyEventListener* listener, int32_t priority);
    void addEventListener(ButtonEventListener* listener, int32_t priority);
    void addEventListener(PointerEventListener* listener, int32_t priority);
    void addEventListener(UIEventListener* listener, int32_t priority);
    void addEventListener(RendererEventListener* listener, int32_t priority);
    void addEventListener(SystemEventListener* listener, int32_t priority);

    virtual void setup() = 0;
    virtual void update() = 0;
    virtual void draw(Context& gc) = 0;

    virtual void onKeyDown(const KeyEvent& e);
    virtual void onKeyUp(const KeyEvent& e);
    virtual void onButtonDown(const ButtonEvent& e);
    virtual void onButtonUp(const ButtonEvent& e);
    virtual void onPointerMove(const PointerEvent& e);
    virtual void onPointerDown(const PointerEvent& e);
    virtual void onPointerUp(const PointerEvent& e);

    virtual void onScroll(const ScrollEvent& e);

private:
    // DOM'T PERMIT COPY
    Application(const Application&) NONCOPYABLE;
    Application& operator=(const Application&) NONCOPYABLE;

private:
    ApplicationImpl* mImpl;
};

}

#endif
