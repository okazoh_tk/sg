/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_UTILS_LOG_H_INCLUDED_
#define SG_UTILS_LOG_H_INCLUDED_

#include "sg/Platform.h"
#include "sg/Config.h"

/* logging
*/
#ifndef LOG_TAG
#define LOG_TAG "sg"
#endif


#if defined(__ANDROID__)

#include <cutils/log.h>

#else


#if !defined(UTILS_USE_CUSTOM_LOG_FUNCTION)

#include <stdio.h>

#define LOG_VERBOSE 4
#define LOG_DEBUG   3
#define LOG_INFO    2
#define LOG_WARN    1
#define LOG_ERROR   0

#define log_print(prio, tag, ...) ((void)sg_dprintf(prio, tag, __VA_ARGS__))

#endif

//#define CONDITION(cond) (__builtin_expect((cond)!=0, 0))
#define CONDITION(cond) (cond)


#ifndef LOGE
#define LOGE(...) log_print(LOG_ERROR, LOG_TAG, __VA_ARGS__)
#endif
#ifndef LOGE_IF
#define LOGE_IF(cond, ...) \
    (CONDITION(cond) ? ((void)LOGE(__VA_ARGS__)) : (void)0)
#endif


#ifndef LOGW
#define LOGW(...) log_print(LOG_WARN, LOG_TAG, __VA_ARGS__)
#endif
#ifndef LOGW_IF
#define LOGW_IF(cond, ...) \
    (CONDITION(cond) ? ((void)LOGW(__VA_ARGS__)) : (void)0)
#endif


#ifndef SG_REMOVE_LOGI
#define SG_REMOVE_LOGI 0
#endif

#ifndef LOGI
#if SG_REMOVE_LOGI == 0
#define LOGI(...) log_print(LOG_INFO, LOG_TAG, __VA_ARGS__)
#else
#define LOGI(...) []{}()
#endif
#endif
#ifndef LOGI_IF
#if SG_REMOVE_LOGI == 0
#define LOGI_IF(cond, ...) \
    (CONDITION(cond) ? ((void)LOGI(__VA_ARGS__)) : (void)0)
#else
#define LOGI_IF(cond, ...) []{}()
#endif
#endif


#ifndef SG_REMOVE_LOGD
#if defined(NDEBUG)
#define SG_REMOVE_LOGD 1
#else
#define SG_REMOVE_LOGD 0
#endif
#endif

#ifndef LOGD
#if SG_REMOVE_LOGD == 0
#define LOGD(...) log_print(LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
#define LOGD(...) []{}()
#endif
#endif
#ifndef LOGD_IF
#if SG_REMOVE_LOGD == 0
#define LOGD_IF(cond, ...) \
    (CONDITION(cond) ? ((void)LOGD(__VA_ARGS__)) : (void)0)
#else
#define LOGD_IF(cond, ...) []{}()
#endif
#endif


#ifndef SG_REMOVE_LOGV
#if defined(NDEBUG)
#define SG_REMOVE_LOGV 1
#else
#define SG_REMOVE_LOGV 0
#endif
#endif

#ifndef LOGV
#if SG_REMOVE_LOGV == 0
#define LOGV(...) log_print(LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#else
#define LOGV(...) []{}()
#endif
#endif
#ifndef LOGV_IF
#if SG_REMOVE_LOGV == 0
#define LOGV_IF(cond, ...) \
    (CONDITION(cond) ? ((void)LOGV(__VA_ARGS__)) : (void)0)
#else
#define LOGV_IF(cond, ...) []{}()
#endif
#endif

#endif


/* assertion
*/
#include <assert.h>
#include <stdlib.h> // abort

#ifndef ASSERT
#define ASSERT(exp) assert(exp)
#endif
#ifndef ASSERT_RET
#ifdef NDEBUG
#define ASSERT_RET(exp, ret) if (!(exp)) return ret
#else
#define ASSERT_RET(exp, ret) if (!(exp)) {assert(exp); return ret;}
#endif
#endif

#ifndef VERIFY
#define VERIFY(exp) if (!(exp)) abort()
#endif

extern "C" {

SG_PUBLIC_API void sg_set_log_level(int32_t level);
SG_PUBLIC_API void sg_dprintf(int32_t level, const char* label, const char* fmt, ...);

}

#endif
