/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_PROGRAM_H_INCLUDED_
#define SG_PROGRAM_H_INCLUDED_

#include "sg/Config.h"
#include "sg/Shader.h"
#include "sg/Property.h"
#include "sg/SharedObject.h"

namespace sg {

class Camera;
class Context;
class VertexBuffer;
struct VertexAttrib;

class SG_PUBLIC_API Program : public SharedObject {
public:
    Program();
    virtual ~Program();

    void setShader(
        const char* vertex_shader_source,
        const char* fragment_shader_source);

    void setShader(
        const char* vertex_shader_sources[], uint32_t vs_num,
        const char* fragment_shader_sources[], uint32_t fs_num);

    void setShaderFromFile(
        const char* vertex_shader_path,
        const char* fragment_shader_path);

    bool prepare();

    void setupLayout(const VertexAttrib* layout);

    uint32_t getName() const;

    static uint32_t link(uint32_t vertex_shader, uint32_t fragment_shader);
    static void destroy(uint32_t& program);

    class SG_PUBLIC_API WeakRef : protected Observer {
    public:
        WeakRef(Program* program = nullptr);
        WeakRef(const WeakRef& program);
        virtual ~WeakRef();

        Program* operator->();
        void operator=(const WeakRef& program);
        void operator=(Program* program);

        operator Program*() const;

    protected:
        virtual void onReleased(SharedObject* caller);

    private:
        Program* mProgram;
    };

private:
    Program(const Program& ) NONCOPYABLE;
    void operator=(const Program&) NONCOPYABLE;

private:
    uint32_t mProgram;
    VertexShader* mVertexShader;
    FragmentShader* mFragmentShader;
};

}

#endif
