/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_TIME_H_INCLUDED_
#define SG_TIME_H_INCLUDED_

#include "sg/Config.h"

namespace sg {

class SG_PUBLIC_API Time {
public:
    typedef uint64_t time_type;

    static const Time now();

    Time();
    Time(uint64_t value);
    Time(const Time& rh);

    void operator=(const Time& rh);
    const Time operator-(const Time& rh) const;

    operator time_type() const;

private:
    time_type mUsec;
};

class SG_PUBLIC_API ThreadTimes {
public:
    typedef Time::time_type time_type;

    static const ThreadTimes now();

    ThreadTimes();
    ThreadTimes(time_type kernel_usec, time_type user_usec);
    ThreadTimes(const ThreadTimes& rh);

    void operator=(const ThreadTimes& rh);
    const ThreadTimes operator-(const ThreadTimes& rh) const;

private:
    time_type mKernelUsec;
    time_type mUserUsec;
};

}

#endif
