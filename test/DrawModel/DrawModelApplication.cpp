
#include "DrawModelApplication.h"

using namespace sg;

DrawModelApplication::DrawModelApplication()
    : mAxisModel(nullptr)
    , mGridModel(nullptr)
    , mBoxModel(nullptr)
    , mRectangleModel(nullptr)
{
}

void DrawModelApplication::createAxisModel()
{
    mAxisModel = new AxisModel();

    mAxisModel->setLine(0.5f);
}

void DrawModelApplication::createGridModel()
{
    mGridModel = new GridModel();

    mGridModel->setPlaneXY(0.5, 0.1);
}

void DrawModelApplication::createBoxModel()
{
    mBoxModel = new BoxModel();

    mBoxModel->setVertices(Vector3{ 0.5, 0.5, 0.5 }, Colorf{ 0.8, 0.8, 0.8, 1 });
}

void DrawModelApplication::createRectangleModel()
{
    mRectangleModel = new RectangleModel();

    mRectangleModel->setColor(Colorf{1, 0, 0, 1});
    mRectangleModel->setSize(0.5, 0.5);
}

void DrawModelApplication::setup()
{
    createAxisModel();
    createGridModel();
    createBoxModel();
    createRectangleModel();

    mCameraActor.setPosition(Position3{0, 1, 6});
    addActor(&mCameraActor);
}

void DrawModelApplication::update()
{
}

void DrawModelApplication::draw(Context& gl)
{
    mCameraActor.apply(gl);

    gl.culling.off();
    gl.depthTest.on();
    gl.clear(1, 1, 1, 1);
    gl.drawGrid();

    float offset = 2;
    auto translate = Vector3::make(-offset * 1.5f, 1, 0);
    auto scale = Vector3::make(1, 1, 1);
    Quaternion rotate = Quaternion::make(1, 0, 0, 0);

    if (mAxisModel) {
        gl.place(*mAxisModel, translate, scale, rotate);
    }
    translate.x += offset;

    if (mGridModel) {
        gl.place(*mGridModel, translate, scale, rotate);
    }
    translate.x += offset;

    if (mBoxModel) {
        gl.place(*mBoxModel, translate, scale, rotate);
    }
    translate.x += offset;

    if (mRectangleModel) {
        gl.place(*mRectangleModel, translate, scale, rotate);
    }
    translate.x += offset;
}

DrawModelApplication::~DrawModelApplication()
{
    if (mAxisModel) {
        delete mAxisModel;
    }
    if (mGridModel) {
        delete mGridModel;
    }
    if (mBoxModel) {
        delete mBoxModel;
    }
    if (mRectangleModel) {
        delete mRectangleModel;
    }
}
