
#include "sg/Application.h"
#include "sg/CameraActor.h"

// models
#include "sg/model/AxisModel.h"
#include "sg/model/GridModel.h"
#include "sg/model/BoxModel.h"
#include "sg/model/RectangleModel.h"


class DrawModelApplication : public sg::Application
{
public:
    DrawModelApplication();
    virtual ~DrawModelApplication();

    void createAxisModel();
    void createGridModel();
    void createBoxModel();
    void createRectangleModel();

    virtual void setup();
    virtual void update();
    virtual void draw(sg::Context& gl);

private:
    sg::AxisModel* mAxisModel;
    sg::GridModel* mGridModel;
    sg::BoxModel* mBoxModel;
    sg::RectangleModel* mRectangleModel;
    sg::CameraActor mCameraActor;
};
