
#include "LoadModelApplication.h"

LoadModelApplication::LoadModelApplication()
{
}

LoadModelApplication::~LoadModelApplication()
{
}

void LoadModelApplication::setup()
{
    mModel.load("models/teapot/teapot.obj");
//    mModel.load("models/cube/cube.obj");

    addActor(&mCameraActor);
}

void LoadModelApplication::update()
{
    mModel.update();
}

void LoadModelApplication::draw(Context& gl)
{
    //gl.wireframe.on();
    gl.culling.on();
    gl.depthTest.on();

    mCameraActor.apply(gl);

    gl.clear(1, 1, 1, 1);
    gl.drawGrid();
    gl.drawAxes();

    gl.draw(mModel);
}

void LoadModelApplication::onKeyDown(const KeyEvent& e)
{
    (void)e;
}

void LoadModelApplication::onKeyUp(const KeyEvent& e)
{
    (void)e;
}

void LoadModelApplication::onPointerMove(const PointerEvent& e)
{
    (void)e;
}

void LoadModelApplication::onPointerDown(const PointerEvent& e)
{
    (void)e;
}

void LoadModelApplication::onPointerUp(const PointerEvent& e)
{
    (void)e;
}

void LoadModelApplication::onButtonDown(const ButtonEvent& e)
{
    (void)e;
}

void LoadModelApplication::onButtonUp(const ButtonEvent& e)
{
    (void)e;
}

void LoadModelApplication::onScroll(const ScrollEvent& e)
{
}
