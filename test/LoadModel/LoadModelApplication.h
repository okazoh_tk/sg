
#ifndef LOAD_MODEL_APPLICATION_H_INCLUDED_
#define LOAD_MODEL_APPLICATION_H_INCLUDED_

#include "sg/Application.h"
#include "sg/CameraActor.h"
#include "sg/model/ModelAsset.h"

using namespace sg;

class LoadModelApplication : public Application {
public:
    LoadModelApplication();
    virtual ~LoadModelApplication();

    virtual void setup();
    virtual void update();
    virtual void draw(Context& gc);

    virtual void onKeyDown(const KeyEvent& e);
    virtual void onKeyUp(const KeyEvent& e);

    virtual void onPointerMove(const PointerEvent& e);
    virtual void onPointerDown(const PointerEvent& e);
    virtual void onPointerUp(const PointerEvent& e);
    virtual void onButtonDown(const ButtonEvent& e);
    virtual void onButtonUp(const ButtonEvent& e);
    virtual void onScroll(const ScrollEvent& e);

private:
    ModelAsset mModel;
    CameraActor mCameraActor;
};

#endif
