
#ifndef RENDER_TARGET_APP_H_INCLUDED_
#define RENDER_TARGET_APP_H_INCLUDED_

#include "sg/Application.h"
#include "sg/model/BoxModel.h"
#include "sg/model/RectangleModel.h"
#include "sg/RenderTarget.h"
#include "sg/CameraActor.h"

using namespace sg;

class RenderTargetApp : public Application {
public:
    RenderTargetApp();
    virtual ~RenderTargetApp();

    virtual void setup();
    virtual void update();
    virtual void draw(Context& gc);
    
private:
    RenderTarget mRenderTarget;
    CameraActor mCameraActor;
    BoxModel mBox;
    RectangleModel mBoard;
};

#endif
