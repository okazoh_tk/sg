
#include "RenderTargetApp.h"

RenderTargetApp::RenderTargetApp()
    : mRenderTarget(RenderTarget::Parameter({ 640, 480 }))
{
}

RenderTargetApp::~RenderTargetApp()
{
}

void RenderTargetApp::setup()
{
    Colorf colors[] = {
        { 1, 1, 1, 1},
        { 1, 0, 1, 1},
        { 1, 1, 0, 1},
        { 1, 0, 0, 1},
        { 0, 1, 0, 1},
        { 0, 0, 0, 1},
        { 0, 1, 1, 1},
        { 0, 0, 1, 1},
    };

    mBox.setVertices({0.5f, 0.5f, 0.5f}, colors);

    mRenderTarget.prepare();

    mBoard.setSize(1.f, 1.f);
    mBoard.setTexture(mRenderTarget.getColorBuffer());

    mCameraActor.setProjection(
                               Perspective{ 45, 640 / (float)480, 0.1f, 1000 });
    mCameraActor.setViewport({ 0, 0, 640, 480 });

    addActor(&mCameraActor);
}

void RenderTargetApp::update()
{
}

void RenderTargetApp::draw(Context& gl)
{
    gl.begin(nullptr, &mRenderTarget);

    gl.culling.on();
    gl.depthTest.on();

    mCameraActor.apply(gl);

    gl.clear(1, 1, 1, 1);
    //gl.drawGrid();
    //gl.drawAxes();
    gl.draw(mBox);

    gl.end();

    gl.clear(1, 1, 1, 1);
    gl.drawGrid();
    gl.drawAxes();
    gl.draw(mBoard);
}
