
#define VALIDATE_PLATFORM
#include "RenderTargetApp.h"

int main(int argc, char* argv[])
{
    RenderTargetApp app;

    return app.run();
}
