
#include "TestApplication.h"

TestApplication::TestApplication()
    : Application(Size{640, 480})
{
}

TestApplication::~TestApplication()
{
}

void TestApplication::setup()
{
    Colorf colors[8] = {
        { 1, 1, 1, 1 },
        { 1, 0, 1, 1 },
        { 1, 1, 0, 1 },
        { 1, 0, 0, 1 },
        { 0, 1, 0, 1 },
        { 0, 0, 0, 1 },
        { 0, 1, 1, 1 },
        { 0, 0, 1, 1 },
    };

    mBox.setVertices({ 0.5f, 0.5f, 0.5f }, colors);
    
    addActor(&mCameraActor);
}

void TestApplication::update()
{
}

void TestApplication::draw(Context& gl)
{
    gl.culling.on();
    gl.depthTest.on();

    mCameraActor.apply(gl);

    gl.clear(1, 1, 1, 1);
    gl.drawGrid();
    gl.drawAxes();
    gl.draw(mBox);

    gl.place(
        mBox,
        Vector3{ 2, 1, -1},
        Vector3{ 0.2, 0.2, 0.2 },
        Quaternion::rotate(0,45,45));
}
