
#define VALIDATE_PLATFORM
#include "TestApplication.h"

int main(int argc, char* argv[])
{
    TestApplication app;

    return app.run();
}
