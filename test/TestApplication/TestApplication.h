
#ifndef TEST_APPLICATION_H_INCLUDED_
#define TEST_APPLICATION_H_INCLUDED_

#include "sg/Application.h"
#include "sg/model/GradientModel.h"
#include "sg/model/BoxModel.h"
#include "sg/CameraActor.h"

using namespace sg;

class TestApplication : public Application {
public:
    TestApplication();
    virtual ~TestApplication();

    virtual void setup();
    virtual void update();
    virtual void draw(Context& gc);

private:
    BoxModel mBox;
    CameraActor mCameraActor;
};

#endif
