
#ifndef CUSTOM_EVENT_LISTENERS_H_INCLUDED_
#define CUSTOM_EVENT_LISTENERS_H_INCLUDED_

#include "sg/Event.h"

class MyKeyEventListener : public sg::KeyEventListener {
public:
    MyKeyEventListener();
    virtual ~MyKeyEventListener();
    virtual void onKeyEvent(sg::EventDriver& caller, const sg::KeyEvent& e);

private:
    uint32_t mCallCount_onKeyEvent;
};

class MyButtonEventListener : public sg::ButtonEventListener {
public:
    MyButtonEventListener();
    virtual ~MyButtonEventListener();
    virtual void onButtonEvent(sg::EventDriver& caller, const sg::ButtonEvent& e);

private:
    uint32_t mCallCount_onButtonEvent;
};

class MyPointerEventListener : public sg::PointerEventListener {
public:
    MyPointerEventListener();
    virtual ~MyPointerEventListener();
    virtual void onPointerEvent(sg::EventDriver& caller, const sg::PointerEvent& e);

private:
    uint32_t mCallCount_onPointerEvent;
};

class MyUIEventListener : public sg::UIEventListener {
public:
    MyUIEventListener();
    virtual ~MyUIEventListener();
    virtual void onScroll(sg::EventDriver& caller, const sg::ScrollEvent& e);

private:
    uint32_t mCallCount_onScroll;
};

class MyRendererEventListener : public sg::RendererEventListener {
public:
    MyRendererEventListener();
    virtual ~MyRendererEventListener();
    virtual void onUpdate(sg::EventDriver& caller);
    virtual void onDraw(sg::EventDriver& caller, sg::Context& gl);

private:
    uint32_t mCallCount_onUpdate;
    uint32_t mCallCount_onDraw;
};

class MySystemEventListener : public sg::SystemEventListener {
public:
    MySystemEventListener();
    virtual ~MySystemEventListener();
    virtual void onLaunching(sg::EventDriver& caller);
    virtual void onLaunched(sg::EventDriver& caller);
    virtual void onSuspend(sg::EventDriver& caller);
    virtual void onResume(sg::EventDriver& caller);
    virtual void onDestroy(sg::EventDriver& caller);

private:
    uint32_t mCallCount_onLaunching;
    uint32_t mCallCount_onLaunched;
    uint32_t mCallCount_onSuspend;
    uint32_t mCallCount_onResume;
    uint32_t mCallCount_onDestroy;
};

#endif
