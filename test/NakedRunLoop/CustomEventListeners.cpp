
#define LOG_TAG "test"
#include "CustomEventListeners.h"
#include "sg/EventDriver.h"
#include "sg/Context.h"

#define REPORT(fnc) LOGI(#fnc " was called %d times.", mCallCount_##fnc)

MyKeyEventListener::MyKeyEventListener()
    : mCallCount_onKeyEvent(0)
{
    LOGI("called %s.", __func__);
}

MyKeyEventListener::~MyKeyEventListener()
{
    LOGI("called %s.", __func__);
    REPORT(onKeyEvent);
}

void MyKeyEventListener::onKeyEvent(sg::EventDriver& caller, const sg::KeyEvent& e)
{
    (void)caller; // suppress warning
    (void)e; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onKeyEvent;

    // Pressed 'Q', then exit event loop.
    if (e.getType() == sg::KeyEvent::KEY_DOWN &&
        e.getKey() == SG_VK_Q)
    {
        caller.exitLoop();
    }
}


MyButtonEventListener::MyButtonEventListener()
    : mCallCount_onButtonEvent(0)
{
    LOGI("called %s.", __func__);
}

MyButtonEventListener::~MyButtonEventListener()
{
    LOGI("called %s.", __func__);
    REPORT(onButtonEvent);
}

void MyButtonEventListener::onButtonEvent(sg::EventDriver& caller, const sg::ButtonEvent& e)
{
    (void)caller; // suppress warning
    (void)e; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onButtonEvent;
}


MyPointerEventListener::MyPointerEventListener()
    : mCallCount_onPointerEvent(0)
{
    LOGI("called %s.", __func__);
}

MyPointerEventListener::~MyPointerEventListener()
{
    LOGI("called %s.", __func__);
    REPORT(onPointerEvent);
}

void MyPointerEventListener::onPointerEvent(sg::EventDriver& caller, const sg::PointerEvent& e)
{
    (void)caller; // suppress warning
    (void)e; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onPointerEvent;
}


MyUIEventListener::MyUIEventListener()
    : mCallCount_onScroll(0)
{
    LOGI("called %s.", __func__);
}

MyUIEventListener::~MyUIEventListener()
{
    LOGI("called %s.", __func__);
    REPORT(onScroll);
}

void MyUIEventListener::onScroll(sg::EventDriver& caller, const sg::ScrollEvent& e)
{
    (void)caller; // suppress warning
    (void)e; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onScroll;
}


MyRendererEventListener::MyRendererEventListener()
    : mCallCount_onUpdate(0)
    , mCallCount_onDraw(0)
{
    LOGI("called %s.", __func__);
}

MyRendererEventListener::~MyRendererEventListener()
{
    LOGI("called %s.", __func__);
    REPORT(onUpdate);
    REPORT(onDraw);
}

void MyRendererEventListener::onUpdate(sg::EventDriver& caller)
{
    (void)caller; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onUpdate;
}

void MyRendererEventListener::onDraw(sg::EventDriver& caller, sg::Context& gl)
{
    (void)caller; // suppress warning
    (void)gl; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onDraw;
}


MySystemEventListener::MySystemEventListener()
    : mCallCount_onLaunching(0)
    , mCallCount_onLaunched(0)
    , mCallCount_onSuspend(0)
    , mCallCount_onResume(0)
    , mCallCount_onDestroy(0)
{
    LOGI("called %s.", __func__);
}

MySystemEventListener::~MySystemEventListener()
{
    LOGI("called %s.", __func__);
    REPORT(onLaunching);
    REPORT(onLaunched);
    REPORT(onSuspend);
    REPORT(onResume);
    REPORT(onDestroy);
}

void MySystemEventListener::onLaunching(sg::EventDriver& caller)
{
    (void)caller; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onLaunching;
}

void MySystemEventListener::onLaunched(sg::EventDriver& caller)
{
    (void)caller; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onLaunched;
}

void MySystemEventListener::onSuspend(sg::EventDriver& caller)
{
    (void)caller; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onSuspend;
}

void MySystemEventListener::onResume(sg::EventDriver& caller)
{
    (void)caller; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onResume;
}

void MySystemEventListener::onDestroy(sg::EventDriver& caller)
{
    (void)caller; // suppress warning
    LOGI("called %s.", __func__);
    ++mCallCount_onDestroy;
}
