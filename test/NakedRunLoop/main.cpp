
#include "sg/EventLooper.h"
#include "CustomEventListeners.h"

int main(int argc, char* argv[])
{
    uint32_t frame_no = 0;

    // Construct EventLooper with window size. EventLooper is derived from EventDriver.
    sg::EventLooper event_driver({ 640, 480 });

    {
        // Custom event listener.
        MyKeyEventListener key_event_listener;
        MyButtonEventListener button_event_listener;
        MyPointerEventListener pointer_event_listener;
        MyUIEventListener ui_event_listener;
        MyRendererEventListener renderer_event_listener;
        MySystemEventListener system_event_listener;

        // Add event listers.
        event_driver.addEventListener(&key_event_listener, 0);
        event_driver.addEventListener(&button_event_listener, 0);
        event_driver.addEventListener(&pointer_event_listener, 0);
        event_driver.addEventListener(&ui_event_listener, 0);
        event_driver.addEventListener(&renderer_event_listener, 0);
        event_driver.addEventListener(&system_event_listener, 0);

        // Setup EventDriver.
        if (event_driver.setup()) {

            // Execute frame events.
            while (event_driver.handleEvents()) {
                ++frame_no;
            }

            // Terminate EventDriver. After this, don't access EventDriver.
            event_driver.terminate();
        }
    }

    return 0;
}
