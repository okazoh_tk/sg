
CXXFLAGS	+=-std=c++11 -Wall -I$(SG_ROOT)/include

SRCS:=$(wildcard $(SG_SRCDIR)/src/*.cpp)
OBJS:=$(SRCS:%.cpp=$(SG_OUTDIR)/%.o)
DEPS:=$(SRCS:%.cpp=$(SG_OUTDIR)/%.d)


$(SG_OUTDIR)/%.o:$(SG_SRCDIR)/%.cpp $(SG_OUTDIR)/%.d $(SG_OUTDIR)
	$(CXX) $(CXXFLAGS) -o $@ $<

$(SG_OUTDIR)/%.d:$(SG_SRCDIR)/%.cpp $(SG_OUTDIR)
	set -e; $(CXX) -M $(CXXFLAGS) $< \
	| sed 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@; \
	[ -s $@ ] || rm -f $@

.PHONY: clean

clean:
	-rm -rf $(SG_OUTDIR)/* $(SG_DISTDIR)/*

-include $(DEPS)

