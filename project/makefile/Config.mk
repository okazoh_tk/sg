
PLATFORM_ARCH   ?=$(shell uname)_$(shell uname -m)

SG_OUTDIR   =$(SG_ROOT)/objects/sg/$(PLATFORM_ARCH)
SG_SRCDIR   =$(SG_ROOT)/src
SG_DISTDIR  =$(SG_ROOT)/dist/sg/$(PLATFORM_ARCH)

$(SG_OUTDIR) $(SG_DISTDIR):
	@mkdir -p $@
