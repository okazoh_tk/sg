

Build on Visual Studio
--------------------

1. Clone 

	```
	git clone https://bitbucket.org/okazoh_tk/graphics-3d-binaries.git third_party
	```

2. Build third party libraries

	* glfw
	* assimp

3. Build sg library
	Open project/vs2013/sg.sln and build.
