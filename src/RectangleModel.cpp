/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/model/RectangleModel.h"
#include "sg/Context.h"

namespace sg {

RectangleModel::RectangleModel()
    : RectangleModel(Buffer::STATIC_DRAW, Buffer::STATIC_DRAW)
{
    // delegate PlaneModel(usage, usage)
}

RectangleModel::RectangleModel(Buffer::Usage vbuffer_usage, Buffer::Usage ibuffer_usage)
    : BasicModel(vbuffer_usage, ibuffer_usage)
    , mWidth(1.f)
    , mHeight(1.f)
{
    Colorf diffuse = {1,1,1,1};
    setColor(diffuse);

    setPrimitiveType(SG_TRIANGLE_STRIP);
}

RectangleModel::~RectangleModel()
{
}

void RectangleModel::setColor(const Colorf& color)
{
    setProperty(SG_UNIFORM_COLOR, color);
}

void RectangleModel::setTexture(Texture* texture)
{
    setProperty(SG_UNIFORM_TEXTURE, texture);

    needPrepare(); // remake buffer
}

void RectangleModel::setSize(float width, float height)
{
    if (width != mWidth || height != mHeight) {
        mWidth = width;
        mHeight = height;

        needPrepare(); // remake buffer
    }
}

bool RectangleModel::prepare(Context& gl)
{
    // make vertex and index buffer.

    /*
     +y
     0
     +--+ 2
     | /|
     |/ |
     +--+ 3  +x
     1

     0,1,2 -> 2,1,3
     */
    uint16_t indices[] = {0,1,2,3};

    float xmin = mWidth / -2.f;
    float ymin = mHeight / -2.f;
    float xmax = xmin + mWidth;
    float ymax = ymin + mHeight;
    Vector3 vertices[] = {
        { xmin, ymax, 0 }, // 0
        { xmin, ymin, 0 }, // 1
        { xmax, ymax, 0 }, // 2
        { xmax, ymin, 0 }, // 3
    };

    auto& material = getMaterial();
    auto& vbuffer = getVertexBuffer();
    auto& ibuffer = getIndexBuffer();
    bool has_texture = material.hasKey(SG_UNIFORM_TEXTURE);
    if (has_texture) {
        Vector2 texcoords[] = {
            { 0.f, 1.f }, // 0
            { 0.f, 0.f }, // 1
            { 1.f, 1.f }, // 2
            { 1.f, 0.f }  // 3
        };
        vbuffer.set(vertices, texcoords, COUNT_OF(vertices));

    } else {
        vbuffer.set(vertices, COUNT_OF(vertices));
    }

    ibuffer.set(indices, COUNT_OF(indices));

    // setup material
    return setProgramIfEmpty(
        gl,
        has_texture ? "system.texture_diffuse" : "system.solid_fill");
}

}
