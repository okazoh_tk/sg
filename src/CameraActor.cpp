/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/CameraActor.h"
#include "sg/Event.h"
#include "sg/MatrixStack.h"
#include "sg/EventDriver.h"
#include "sg/Context.h"
#include "Utility.h"

namespace sg {

class SG_LOCAL_API PoseController
{
public:

    class SG_LOCAL_API IPoseChangeListener {
    public:
        virtual void onUpdatePose(const Quaternion& new_pose) = 0;
    };

    PoseController()
        : mListener(nullptr)
    {
    }

    void set(IPoseChangeListener* listener)
    {
        mListener = listener;
    }

    void notify()
    {
        if (mListener) {
            mListener->onUpdatePose(mPose);
        }
    }

    void onButtonEvent(EventDriver& caller, const ButtonEvent& e)
    {
        if (e.getType() == ButtonEvent::BUTTON_DOWN) {
            if (e.getID() == SG_MOUSE_BUTTON_RIGHT) {
                mDiffEulerAngles = Vector3{ 0, 0, 0 };

                auto pos = caller.getDevice().pointer(0).getPosition();
                mCenter.x = (float)pos.x;
                mCenter.y = (float)pos.y;

                mCameraMoving = true;

                //dprint("start mouse moving.", mPose, true);
            }
        }
        else if (e.getType() == ButtonEvent::BUTTON_UP) {
            if (e.getID() == SG_MOUSE_BUTTON_RIGHT) {
                mCameraMoving = false;
                mEulerAngles += mDiffEulerAngles;
                //dprint("end mouse moving.", mPose, true);
            }
        }
    }

    bool onPointerEvent(
        EventDriver& /*caller*/,
        const PointerEvent& e,
        const Viewport& viewport)
    {
        if (!mCameraMoving) {
            return false;
        }

        float yaw = -((e.getX() - viewport.x) - mCenter.x) / (viewport.width / 2.f);
        float pitch = -((e.getY() - viewport.y) - mCenter.y) / (viewport.height / 2.f);
        float fov = 45.f;

        mDiffEulerAngles.x = pitch * fov;
        mDiffEulerAngles.y = yaw * fov;

        // update pose
        mPose = Quaternion::rotate(
            mEulerAngles.x + mDiffEulerAngles.x,
            mEulerAngles.y + mDiffEulerAngles.y,
            mEulerAngles.z);

        notify();

        return true;
    }

    void setPose(float pitch, float yaw, float roll)
    {
        mPose = Quaternion::rotate(pitch, yaw, roll);
        mEulerAngles.x = pitch;
        mEulerAngles.y = yaw;
        mEulerAngles.z = roll;

        notify();
    }

    void setPose(const Quaternion& pose)
    {
        mPose = pose;
        mPose.eulerAngles(&mEulerAngles.x, &mEulerAngles.y, &mEulerAngles.z);

        notify();
    }

    const Quaternion& getPose() const {
        return mPose;
    }

private:
    PoseController(const PoseController& rhv) NONCOPYABLE;
    void operator=(const PoseController& rhv) NONCOPYABLE;

private:
    Quaternion mPose;
    bool mCameraMoving = false;
    Vector2 mCenter;
    Vector3 mEulerAngles;
    Vector3 mDiffEulerAngles;
    IPoseChangeListener* mListener;
};

class SG_LOCAL_API PositionController
    : public PoseController::IPoseChangeListener
{
public:
    PositionController()
        : mPos({ 0.f, 1.f, 1.5f })
        , mLength({ 0.1f, 0.1f, 0.1f })
    {
    }

    virtual ~PositionController() {
    }

    virtual void onUpdatePose(const Quaternion& pose)
    {
        mRight = pose * Vector3{ mLength.x, 0, 0 };
        mUp = pose * Vector3{ 0, mLength.y, 0 };
        mForward = pose * Vector3{ 0, 0, -mLength.z };
    }

    bool onKeyEvent(EventDriver& /*caller*/, const KeyEvent& e)
    {
        if (e.getType() != KeyEvent::KEY_DOWN) {
            return false;
        }

        bool handled = true;
        switch (e.getKey()) {
        case SG_VK_UP:
        case SG_VK_W: // forward
            mPos += mForward;
            break;

        case SG_VK_DOWN:
        case SG_VK_S: // backward
            mPos -= mForward;
            break;

        case SG_VK_RIGHT:
        case SG_VK_D: // right
            mPos += mRight;
            break;

        case SG_VK_LEFT:
        case SG_VK_A:
            mPos -= mRight;
            break;

        case SG_VK_PAGE_UP:
        case SG_VK_E:
            mPos += mUp;
            break;

        case SG_VK_PAGE_DOWN:
        case SG_VK_Q:
            mPos -= mUp;
            break;

            //case SG_VK_R:
            //    mPose = Quaternion::rotate(0, 0, 0);
            //    break;
        default:
            handled = false;
            break;
        }

        return handled;
    }

    void setPosition(const Position3& position) {
        mPos = position;
    }

    const Position3& getPosition() const {
        return mPos;
    }

private:
    PositionController(const PositionController& rhv) NONCOPYABLE;
    void operator=(const PositionController& rhv) NONCOPYABLE;

private:
    Position3 mPos;
    Vector3 mLength;
    Vector3 mRight, mUp, mForward;
};

class SG_LOCAL_API CameraActorImpl
    : public KeyEventListener
    , public PointerEventListener
    , public ButtonEventListener
{
public:

    CameraActorImpl()
        : mAdjustTarget(true)
        , mPoseController()
        , mPositionController()
    {
        mProjection.set(Perspective{ 45, 1, 0.1f, 1000 });
        mPoseController.set(&mPositionController);
        setPose(0, 0, 0);
    }

    virtual ~CameraActorImpl()
    {
    }

    void adjustTarget(bool enable)
    {
        mAdjustTarget = enable;
    }

    bool isAdjustTarget() const
    {
        return mAdjustTarget;
    }

    void setViewport(const Viewport& viewport)
    {
        mViewport = viewport;
        adjustTarget(false);
    }

    Viewport getViewport() const
    {
        return mViewport;
    }

    void setProjection(const Perspective& perspective)
    {
        mProjection.set(perspective);
        adjustTarget(false);
    }

    void setProjection(const Frustum& frustum)
    {
        mProjection.set(frustum);
        adjustTarget(false);
    }

    void setProjection(const Ortho& ortho)
    {
        mProjection.set(ortho);
        adjustTarget(false);
    }

    ProjectionType getProjectionType() const
    {
        return mProjection.type;
    }

    bool getProjection(Perspective& perspective) const
    {
        bool is_perspective = mProjection.isPerspective();
        if (is_perspective) {
            perspective = mProjection.value.perspective;
        }
        return is_perspective;
    }

    bool getProjection(Frustum& frustum) const
    {
        bool is_frustum = mProjection.isFrustum();
        if (is_frustum) {
            frustum = mProjection.value.frustum;
        }
        return is_frustum;
    }

    bool getProjection(Ortho& ortho) const
    {
        bool is_ortho = mProjection.isOrtho();
        if (is_ortho) {
            ortho = mProjection.value.ortho;
        }
        return is_ortho;
    }

    void setPosition(const Position3& position)
    {
        mPositionController.setPosition(position);
    }

    Position3 getPosition() const
    {
        return mPositionController.getPosition();
    }

    void setPose(float pitch, float yaw, float roll)
    {
        mPoseController.setPose(pitch, yaw, roll);
    }

    void setPose(const Quaternion& pose)
    {
        mPoseController.setPose(pose);
    }

    Quaternion getPose() const
    {
        return mPoseController.getPose();
    }

    virtual void onKeyEvent(EventDriver& caller, const KeyEvent& e)
    {
        mPositionController.onKeyEvent(caller, e);
    }

    virtual void onButtonEvent(EventDriver& caller, const ButtonEvent& e)
    {
        mPoseController.onButtonEvent(caller, e);
    }

    virtual void onPointerEvent(EventDriver& caller, const PointerEvent& e)
    {
        mPoseController.onPointerEvent(caller, e, mViewport);
    }

    void apply(Context& gl)
    {
        updateSize(gl);

        // 1. set viewport
        gl.viewport(mViewport);

        auto& mat = gl.getCurrentMatrix();

        // 2. reset matrix
        mat.identity();

        // 3. set camera projection matrix
        switch (mProjection.type) {
        case SG_PERSPECTIVE: mat.project(mProjection.value.perspective); break;
        case SG_FRUSTUM: mat.project(mProjection.value.frustum); break;
        case SG_ORTHO: mat.project(mProjection.value.ortho); break;
        }

        // 4. set camera pose and position
        mat.cameraPose(
            mPositionController.getPosition(),
            mPoseController.getPose());
    }

    void updateSize(Context& gl)
    {
        if (!mAdjustTarget) {
            return;
        }

        auto size = gl.getCurrentTargetSize();
        mViewport.x = 0;
        mViewport.y = 0;
        mViewport.width = size.width;
        mViewport.height = size.height;

        switch (mProjection.type) {
        case SG_PERSPECTIVE:
            mProjection.value.perspective.aspect = (float)size.width / size.height;
            break;

        case SG_FRUSTUM:
            // todo:
//            mProjection.value.frustum;
            break;

        case SG_ORTHO:
            // todo:
///            mProjection.value.ortho;
            break;
        }
    }

private:
    bool mAdjustTarget;
    Viewport mViewport;
    Projection mProjection;
    PositionController mPositionController;
    PoseController mPoseController;
};

CameraActor::CameraActor()
    : mImpl(new CameraActorImpl())
{
}

CameraActor::~CameraActor()
{
    delete mImpl;
}

void CameraActor::regist(EventDriver& event_driver, int32_t priority)
{
    event_driver.addEventListener(static_cast<KeyEventListener*>(mImpl), priority);
    event_driver.addEventListener(static_cast<PointerEventListener*>(mImpl), priority);
    event_driver.addEventListener(static_cast<ButtonEventListener*>(mImpl), priority);
}

void CameraActor::adjustTarget(bool enable)
{
    mImpl->adjustTarget(enable);
}

bool CameraActor::isAdjustTarget() const
{
    return mImpl->isAdjustTarget();
}

void CameraActor::setProjection(const Perspective& perspective)
{
    mImpl->setProjection(perspective);
}

void CameraActor::setProjection(const Frustum& frustum)
{
    mImpl->setProjection(frustum);
}

void CameraActor::setProjection(const Ortho& ortho)
{
    mImpl->setProjection(ortho);
}

ProjectionType CameraActor::getProjectionType() const
{
    return mImpl->getProjectionType();
}

bool CameraActor::getProjection(Perspective& perspective) const
{
    return mImpl->getProjection(perspective);
}

bool CameraActor::getProjection(Frustum& frustum) const
{
    return mImpl->getProjection(frustum);
}

bool CameraActor::getProjection(Ortho& ortho) const
{
    return mImpl->getProjection(ortho);
}

void CameraActor::setViewport(const Viewport& viewport)
{
    mImpl->setViewport(viewport);
}

Viewport CameraActor::getViewport() const
{
    return mImpl->getViewport();
}

void CameraActor::setPosition(const Position3& position)
{
    mImpl->setPosition(position);
}

Position3 CameraActor::getPosition() const
{
    return mImpl->getPosition();
}

void CameraActor::setPose(float pitch, float yaw, float roll)
{
    mImpl->setPose(pitch, yaw, roll);
}

void CameraActor::setPose(const Quaternion& pose)
{
    mImpl->setPose(pose);
}

Quaternion CameraActor::getPose() const
{
    return mImpl->getPose();
}

void CameraActor::apply(Context& gl)
{
    mImpl->apply(gl);
}

}
