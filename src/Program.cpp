/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Program.h"
#include "sg/Context.h"
#include "sg/VertexBuffer.h"
#include "OpenGL.h"
#include "sg/MemoryAllocator.h"

namespace sg {

Program::Program()
    : mProgram(0)
    , mVertexShader(nullptr)
    , mFragmentShader(nullptr)
{
}

Program::~Program()
{
    clearObserver();
    if (mVertexShader) { delete mVertexShader; }
    if (mFragmentShader) { delete mFragmentShader; }
    destroy(mProgram);
}

uint32_t Program::getName() const
{
    return mProgram;
}

void Program::setShader(
    const char* vertex_shader_source,
    const char* fragment_shader_source)
{
    if (mVertexShader) {
        delete mVertexShader;
        destroy(mProgram);
    }
    if (mFragmentShader) {
        delete mFragmentShader;
        destroy(mProgram);
    }
    mVertexShader = new VertexShader(vertex_shader_source);
    mFragmentShader = new FragmentShader(fragment_shader_source);
}

void Program::setShader(
    const char* vertex_shader_sources[], uint32_t vs_num,
    const char* fragment_shader_sources[], uint32_t fs_num)
{
    if (mVertexShader) {
        delete mVertexShader;
        destroy(mProgram);
    }
    if (mFragmentShader) {
        delete mFragmentShader;
        destroy(mProgram);
    }
    mVertexShader = new VertexShader(vertex_shader_sources, vs_num);
    mFragmentShader = new FragmentShader(fragment_shader_sources, fs_num);
}

void Program::setShaderFromFile(
    const char* vertex_shader_path,
    const char* fragment_shader_path)
{
    if (mVertexShader) {
        delete mVertexShader;
        destroy(mProgram);
    }
    if (mFragmentShader) {
        delete mFragmentShader;
        destroy(mProgram);
    }

    mVertexShader = new VertexShader(FilePath(vertex_shader_path));
    mFragmentShader = new FragmentShader(FilePath(fragment_shader_path));
}


bool Program::prepare()
{
    if (!mVertexShader || !mFragmentShader) {
        return false;
    }

    if (0 == mProgram) {
        if (mVertexShader->prepare() && mFragmentShader->prepare()) {
            mProgram = link(mVertexShader->getName(), mFragmentShader->getName());
        }
    }

    return mProgram != 0;
}

static const uint32_t types[] = {
    GL_BYTE,  // BYTE
    GL_UNSIGNED_BYTE,  // UNSIGNED_BYTE
    GL_SHORT,  // SHORT
    GL_UNSIGNED_SHORT,  // UNSIGNED_SHORT
    GL_INT,  // INT
    GL_UNSIGNED_INT,  // UNSIGNED_INT
    GL_FLOAT,  // FLOAT
    GL_FIXED,  // FIXED
#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    GL_HALF_FLOAT,
    GL_INT_2_10_10_10_REV,
    GL_UNSIGNED_INT_2_10_10_10_REV,
#endif
};

void Program::setupLayout(const VertexAttrib* attribs)
{
    uint32_t stride = VertexBuffer::computeVertexSize(attribs);
    uint32_t offset = 0;
    auto layout = attribs;
    while (layout->name) {
        auto index = glGetAttribLocation(mProgram, layout->name);
        if (index >= 0) {
            invokeGL(glVertexAttribPointer(
                index,
                layout->type->num,
                types[layout->type->type],
                layout->type->normalized ? GL_TRUE : GL_FALSE,
                stride,
                reinterpret_cast<const void*>(offset)));

            invokeGL(glEnableVertexAttribArray(index));
        }
        
        offset += VertexBuffer::computeAttribSize(layout);        
        ++layout;
    }
}

uint32_t Program::link(uint32_t vertex_shader, uint32_t fragment_shader)
{
    if (vertex_shader == 0 || fragment_shader == 0) {
        // todo: print error
        return 0;
    }

    auto program = glCreateProgram();
    if (program == 0) {
        // todo: print error
        return 0;
    }

    invokeGL(glAttachShader(program, vertex_shader));
    invokeGL(glAttachShader(program, fragment_shader));

    invokeGL(glLinkProgram(program));

    GLint is_ready;
    invokeGL(glGetProgramiv(program, GL_LINK_STATUS, &is_ready));
    if (is_ready == GL_FALSE) {

        GLint log_len;
        invokeGL(glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_len));
        log_len += 1;
        GLchar* log = (GLchar*)sysAlloc(log_len);
        GLsizei real_len;
        invokeGL(glGetProgramInfoLog(program, log_len, &real_len, log));
        log[real_len] = '\0';
        LOGE("Shader linking error: %s", log);
        sysFree(log);

        destroy(program);
    }

    return program;
}

void Program::destroy(uint32_t& program)
{
    if (program) {
        glDeleteProgram(program);
        program = 0;
    }
}


Program::WeakRef::WeakRef(Program* program)
    : mProgram(program)
{
    if (mProgram) {
        mProgram->addObserver(this);
    }
}

Program::WeakRef::WeakRef(const WeakRef& program)
    : WeakRef(program.mProgram)
{
}

Program::WeakRef::~WeakRef()
{
    if (mProgram) {
        mProgram->removeObserver(this);
        mProgram = nullptr;
    }
}

Program* Program::WeakRef::operator->()
{
    return mProgram;
}

void Program::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mProgram);
}

void Program::WeakRef::operator=(Program* program)
{
    if (mProgram == program) {
        return;
    }

    if (mProgram) {
        mProgram->removeObserver(this);
    }

    mProgram = program;

    if (mProgram) {
        mProgram->addObserver(this);
    }
}

Program::WeakRef::operator Program*() const
{
    return mProgram;
}

void Program::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mProgram = nullptr;
}

}
