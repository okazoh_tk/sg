/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_SIMPLE_DEVICE_H_INCLUDED_
#define SG_SIMPLE_DEVICE_H_INCLUDED_

#include "sg/IDevice.h"

namespace sg {

class SG_LOCAL_API SimpleDevice : public IDevice {
public:
    SimpleDevice();
    virtual ~SimpleDevice();

    class SG_LOCAL_API PointerDevice : public IPointer {
    public:
        PointerDevice();
        virtual  ~PointerDevice();

        bool validate(int32_t id) const;
        virtual const PointerState* getState(int32_t id) const;
        void move(int32_t id, int32_t x, int32_t y);

    private:
        PointerDevice(const PointerDevice&) NONCOPYABLE;
        void operator=(const PointerDevice&) NONCOPYABLE;

    private:
        PointerState pointers[10];
    };

    class SG_LOCAL_API ButtonDevice : public IButton {
    public:
        ButtonDevice();
        virtual  ~ButtonDevice();

        bool validate(int32_t id) const;
        virtual const ButtonState* getState(int32_t id) const;
        void press(int32_t ch);
        void release(int32_t ch);

    private:
        ButtonDevice(const ButtonDevice&) NONCOPYABLE;
        void operator=(const ButtonDevice&) NONCOPYABLE;

    private:
        ButtonState buttons[8];
    };

    class SG_LOCAL_API KeyDevice : public IKey {
    public:
        KeyDevice();
        virtual  ~KeyDevice();

        bool validate(int32_t ch) const;
        virtual const KeyState* getState(int32_t ch) const;
        void press(int32_t ch);
        void release(int32_t ch);

    private:
        KeyDevice(const KeyDevice&) NONCOPYABLE;
        void operator=(const KeyDevice&)NONCOPYABLE;

    private:
        KeyState keys[255];
    };

    virtual IPointer* getPointerInterface();
    virtual IButton* getButtonInterface();
    virtual IKey* getKeyInterface();
    
    PointerDevice& pointer();
    ButtonDevice& button();
    KeyDevice& key();

private:
    PointerDevice mPointer;
    ButtonDevice mButton;
    KeyDevice mKey;
};

}

#endif
