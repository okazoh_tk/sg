/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/System.h"
#include "sg/MemoryAllocator.h"

namespace sg {

FilePath::FilePath(const char* path)
    : mPath(getDefaultMemoryAllocator()->duplicate(path))
    , mIsTemp(false)
{
}

FilePath::FilePath(const char* path, const TempBuffer& )
    : mPath(path)
    , mIsTemp(true)
{
}

FilePath::~FilePath()
{
    if (mPath && !mIsTemp) {
        sysFree((void*)mPath);
    }
}

const char* FilePath::getPath() const
{
    return mPath;
}


char* File::read(const FilePath& path, MemoryAllocator* allocator)
{
    const char* filepath = path.getPath();
    FILE* fp = fopen(filepath, "rt");
    if (!fp) {
        LOGE("failed to open '%s'.", filepath);
        return nullptr;
    }

    char* buffer = nullptr;
    fpos_t size;
    fseek(fp, 0, SEEK_END);
    if (0 == fgetpos(fp, &size)) {

        uint32_t buffer_size = (uint32_t)size + 1;
        buffer = (char*)allocator->allocate(buffer_size);
        fseek(fp, 0, SEEK_SET);
        auto read_size = fread(buffer, 1, buffer_size, fp);
        buffer[read_size] = '\0';
    } else {
        LOGE("failed to fgetpos.");
    }

    //LOGE("%s", buffer);

    fclose(fp);

    return buffer;
}

}
