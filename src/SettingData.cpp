/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "SettingData.h"
#include "OpenGL.h"

namespace sg {

static const GLenum blend_funcs[] = {
    GL_ZERO,
    GL_ONE,
    GL_SRC_COLOR,
    GL_ONE_MINUS_SRC_COLOR,
    GL_DST_COLOR,
    GL_ONE_MINUS_DST_COLOR,
    GL_SRC_ALPHA,
    GL_ONE_MINUS_SRC_ALPHA,
    GL_DST_ALPHA,
    GL_ONE_MINUS_DST_ALPHA,
    GL_CONSTANT_COLOR,
    GL_ONE_MINUS_CONSTANT_COLOR,
    GL_CONSTANT_ALPHA,
    GL_ONE_MINUS_CONSTANT_ALPHA,
    GL_SRC_ALPHA_SATURATE
};

static const GLenum modes[] = {
    GL_FUNC_ADD,
    GL_FUNC_SUBTRACT,
    GL_FUNC_REVERSE_SUBTRACT,
#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    GL_MIN,
    GL_MAX
#endif
};

static const GLenum funcs[] = {
    GL_NEVER,
    GL_LESS,
    GL_LEQUAL,
    GL_GREATER,
    GL_GEQUAL,
    GL_EQUAL,
    GL_NOTEQUAL,
    GL_ALWAYS
};

static const GLenum faces[] = {
    GL_FRONT,
    GL_BACK,
    GL_FRONT_AND_BACK
};

static const GLenum front_faces[] = {
    GL_CW,
    GL_CCW,
};

static const GLenum ops[] = {
    GL_KEEP,
    GL_ZERO,
    GL_REPLACE,
    GL_INCR,
    GL_INCR_WRAP,
    GL_DECR,
    GL_DECR_WRAP,
    GL_INVERT,
};


void SettingData::init()
{
    auto size = sizeof(*this);
    memset(this, 0, size);

    enable_blend = false;
    blend.init();

    enable_culling = false;
    culling.init();

    enable_depth_test = false;
    depth_test.init();

    enable_dither = true;

    enable_polygon_offset = false;
    polygon_offset.init();

    enable_primitive_restart_fixed_index = false;
    enable_rasterizer_discard = false;
    enable_sample_alpha_to_coverage = false;

    enable_sample_coverage = false;
    sample_coverage.init();

    enable_scissor_test = false;
    scissor.init();

    enable_stencil_test = false;
    stencil.init();

    // rendering settings
    renderer.init();
}

void SettingData::clearDirty()
{
    dirty_blend = 0;
    dirty_culling = 0;
    dirty_depth_test = 0;
    dirty_dither = 0;
    dirty_polygon_offset = 0;
    dirty_primitive_restart_fixed_index = 0;
    dirty_rasterizer_discard = 0;
    dirty_sample_alpha_to_coverage = 0;
    dirty_sample_coverage = 0;
    dirty_scissor_test = 0;
    dirty_stencil_test = 0;
}

void SettingData::rollback(const SettingData& dirties)
{
    // rollback whole settings

    if (dirties.dirty_blend) {
        if (enable_blend) {
            invokeGL(glEnable(GL_BLEND));
        } else {
            invokeGL(glDisable(GL_BLEND));
        }
        blend.rollbackFrom(dirties.blend);
    }

    if (dirties.dirty_culling) {
        if (enable_culling) {
            invokeGL(glEnable(GL_CULL_FACE));
        } else {
            invokeGL(glDisable(GL_CULL_FACE));
        }
        culling.rollbackFrom(dirties.culling);
    }

    if (dirties.dirty_depth_test) {
        if (enable_depth_test) {
            invokeGL(glEnable(GL_DEPTH_TEST));
        } else {
            invokeGL(glDisable(GL_DEPTH_TEST));
        }
        depth_test.rollbackFrom(dirties.depth_test);
    }

    if (dirties.dirty_dither) {
        if (enable_dither) {
            invokeGL(glEnable(GL_DITHER));
        } else {
            invokeGL(glDisable(GL_DITHER));
        }
    }

    if (dirties.dirty_polygon_offset) {
        if (enable_polygon_offset) {
            invokeGL(glEnable(GL_POLYGON_OFFSET_FILL));
        } else {
            invokeGL(glDisable(GL_POLYGON_OFFSET_FILL));
        }
        polygon_offset.rollbackFrom(dirties.polygon_offset);
    }

#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    if (dirties.dirty_primitive_restart_fixed_index) {
        if (enable_primitive_restart_fixed_index) {
            invokeGL(glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX));
        } else {
            invokeGL(glDisable(GL_PRIMITIVE_RESTART_FIXED_INDEX));
        }
    }

    if (dirties.dirty_rasterizer_discard) {
        if (enable_rasterizer_discard) {
            invokeGL(glEnable(GL_RASTERIZER_DISCARD));
        } else {
            invokeGL(glDisable(GL_RASTERIZER_DISCARD));
        }
    }
#endif

    if (dirties.dirty_sample_alpha_to_coverage) {
        if (enable_sample_alpha_to_coverage) {
            invokeGL(glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE));
        } else {
            invokeGL(glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE));
        }
    }

    if (dirties.dirty_sample_coverage) {
        if (enable_sample_coverage) {
            invokeGL(glEnable(GL_SAMPLE_COVERAGE));
        } else {
            invokeGL(glDisable(GL_SAMPLE_COVERAGE));
        }
        sample_coverage.rollbackFrom(dirties.sample_coverage);
    }

    if (dirties.dirty_scissor_test) {
        if (enable_scissor_test) {
            invokeGL(glEnable(GL_SCISSOR_TEST));
        } else {
            invokeGL(glDisable(GL_SCISSOR_TEST));
        }
        scissor.rollbackFrom(dirties.scissor);
    }

    if (dirties.dirty_stencil_test) {
        if (enable_stencil_test) {
            invokeGL(glEnable(GL_STENCIL_TEST));
        } else {
            invokeGL(glDisable(GL_STENCIL_TEST));
        }
        stencil.rollbackFrom(dirties.stencil);
    }
}

#define toggle(param, enable, capability) \
    bool diff = ((param == 1) == enable ? false : true); \
    if (diff) { \
        if (enable) {\
            invokeGL(glEnable(capability));\
        } else {\
            invokeGL(glDisable(capability));\
        }\
        param = (enable ? 1 : 0);\
    }\
    return diff

bool SettingData::setBlend(bool enable)
{
    toggle(enable_blend, enable, GL_BLEND);
}

bool SettingData::setCulling(bool enable)
{
    toggle(enable_culling, enable, GL_CULL_FACE);
}

bool SettingData::setDepthTest(bool enable)
{
    toggle(enable_depth_test, enable, GL_DEPTH_TEST);
}

bool SettingData::setDither(bool enable)
{
    toggle(enable_dither, enable, GL_DITHER);
}

bool SettingData::setPolygonOffset(bool enable)
{
    toggle(enable_polygon_offset, enable, GL_POLYGON_OFFSET_FILL);
}

bool SettingData::setPrimitiveRestartFixedIndex(bool enable)
{
#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    toggle(enable_primitive_restart_fixed_index, enable, GL_PRIMITIVE_RESTART_FIXED_INDEX);
#else
    (void)enable;
    return false;
#endif
}

bool SettingData::setRasterizerDiscard(bool enable)
{
#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    toggle(enable_rasterizer_discard, enable, GL_RASTERIZER_DISCARD);
#else
    (void)enable;

    return false;
#endif
}

bool SettingData::setSampleAlphaToCoverage(bool enable)
{
    toggle(enable_sample_alpha_to_coverage, enable, GL_SAMPLE_ALPHA_TO_COVERAGE);
}

bool SettingData::setSampleCoverage(bool enable)
{
    toggle(enable_sample_coverage, enable, GL_SAMPLE_COVERAGE);
}

bool SettingData::setScissorTest(bool enable)
{
    toggle(enable_scissor_test, enable, GL_SCISSOR_TEST);
}

bool SettingData::setStencilTest(bool enable)
{
    toggle(enable_stencil_test, enable, GL_STENCIL_TEST);
}


void SettingData::Blend::init()
{
    red = green = blue = alpha = 0;
    sfactor_rgb = sfactor_a = (uint32_t)SG_ONE;
    dfactor_rgb = dfactor_a = (uint32_t)SG_ZERO;
    equation_rgb = equation_a = (uint32_t)SG_FUNC_ADD;
}

bool SettingData::Blend::color(float r, float g, float b, float a)
{
    if (r != red || g != green || b != blue || a != alpha) {
        red = r;
        green = g;
        blue = b;
        alpha = a;
        invokeGL(glBlendColor(r, g, b, a));
        return true;
    }
    return false;
}

bool SettingData::Blend::function(SGBlendFactor src, SGBlendFactor dst)
{
    if (!equalsFunc(src, dst, src, dst)) {
        sfactor_rgb = (uint32_t)src;
        dfactor_rgb = (uint32_t)dst;
        sfactor_a = (uint32_t)src;
        dfactor_a = (uint32_t)dst;
        invokeGL(glBlendFunc(blend_funcs[src], blend_funcs[dst]));
        return true;
    }
    return false;
}

bool SettingData::Blend::function(
    SGBlendFactor srcRGB, SGBlendFactor dstRGB,
    SGBlendFactor srcAlpha, SGBlendFactor dstAlpha)
{
    if (!equalsFunc(srcRGB, dstRGB, srcAlpha, dstAlpha))
    {
        sfactor_rgb = (uint32_t)srcRGB;
        dfactor_rgb = (uint32_t)dstRGB;
        sfactor_a = (uint32_t)srcAlpha;
        dfactor_a = (uint32_t)dstAlpha;
        invokeGL(glBlendFuncSeparate(
            blend_funcs[srcRGB], blend_funcs[dstRGB],
            blend_funcs[srcAlpha], blend_funcs[dstAlpha]));
        return true;
    }
    return false;
}

bool SettingData::Blend::equation(SGBlendEquation mode)
{
    ASSERT_RET(mode < COUNT_OF(modes), false);

    if (equalsEqu(mode, mode)) {
        equation_rgb = (uint32_t)mode;
        equation_a = (uint32_t)mode;
        invokeGL(glBlendEquation(modes[mode]));
        return true;
    }
    return false;
}

bool SettingData::Blend::equation(
    SGBlendEquation mode_rgb, SGBlendEquation mode_alpha)
{
    ASSERT_RET(mode_rgb < COUNT_OF(modes), false);
    ASSERT_RET(mode_alpha < COUNT_OF(modes), false);

    if (!equalsEqu(mode_rgb, mode_alpha)) {
        equation_rgb = (uint32_t)mode_rgb;
        equation_a = (uint32_t)mode_alpha;
        invokeGL(glBlendEquationSeparate(modes[mode_rgb], modes[mode_alpha]));
        return true;
    }
    return false;
}

void SettingData::Blend::rollbackFrom(const Blend& current)
{
    if (!equalsColor(current)) {
        invokeGL(glBlendColor(red, green, blue, alpha));
    }
    if (!equalsFunc(current)) {
        invokeGL(glBlendFuncSeparate(
            blend_funcs[sfactor_rgb], blend_funcs[dfactor_rgb],
            blend_funcs[sfactor_a], blend_funcs[dfactor_a]));
    }
    if (!equalsEqu(current)) {
        invokeGL(glBlendEquationSeparate(modes[equation_rgb], modes[equation_a]));
    }
}


void SettingData::Culling::init()
{
    face_ = SG_BACK;
}

bool SettingData::Culling::face(SGFace f)
{
    if (face_ != (uint8_t)f) {
        face_ = (uint8_t)f;
        invokeGL(glCullFace(faces[f]));
        return true;
    }
    return false;
}

bool SettingData::Culling::front(SGFrontFace f)
{
    if (front_ != (uint8_t)f) {
        front_ = (uint8_t)f;
        invokeGL(glFrontFace(front_faces[f]));
        return true;
    }
    return false;
}

void SettingData::Culling::rollbackFrom(const Culling& current)
{
    if (face_ != current.face_) {
        invokeGL(glCullFace(faces[face_]));
    }
    if (front_ != current.front_) {
        invokeGL(glFrontFace(front_faces[front_]));
    }
}


void SettingData::DepthTest::init()
{
    func_ = SG_LESS;
    mask_ = true;
    near_ = 0;
    far_ = 0;
}

bool SettingData::DepthTest::function(SGFunction f)
{
    if (func_ != (uint8_t)f) {
        func_ = (uint8_t)f;
        invokeGL(glDepthFunc(funcs[f]));
        return true;
    }
    return false;
}

bool SettingData::DepthTest::mask(bool m)
{
    if (mask_ != m) {
        mask_ = m;
        invokeGL(glDepthMask(m ? GL_TRUE : GL_FALSE));
        return true;
    }
    return false;
}

bool SettingData::DepthTest::range(float n, float f)
{
    if (near_ != n || far_ != f) {
        near_ = n;
        far_ = f;
        invokeGL(glDepthRangef(n, f));
        return true;
    }
    return false;
}

void SettingData::DepthTest::rollbackFrom(const DepthTest& current)
{
    if (mask_ != current.mask_) {
        invokeGL(glDepthMask(mask_ ? GL_TRUE : GL_FALSE));
    }
    if (near_ != current.near_ || far_ != current.far_) {
        invokeGL(glDepthRangef(near_, far_));
    }
}


void SettingData::PolygonOffset::init()
{
    factor_ = units_ = 0;
}

bool SettingData::PolygonOffset::set(float factor, float units)
{
    if (factor_ != factor || units_ != units) {
        factor_ = factor;
        units_ = units;
        invokeGL(glPolygonOffset(factor, units));
        return true;
    }
    return false;
}

void SettingData::PolygonOffset::rollbackFrom(const PolygonOffset& current)
{
    if (factor_ != current.factor_ || units_ != current.units_) {
        invokeGL(glPolygonOffset(factor_, units_));
    }
}


void SettingData::SampleCoverage::init()
{
    value_ = 1.0;
    invert_ = false;
}

bool SettingData::SampleCoverage::set(float value, bool invert)
{
    if (value_ != value || invert_ != invert) {
        value_ = value;
        invert_ = invert;
        invokeGL(glSampleCoverage(value, invert ? GL_TRUE : GL_FALSE));
        return true;
    }
    return false;
}

void SettingData::SampleCoverage::rollbackFrom(const SampleCoverage& current)
{
    if (value_ != current.value_ || invert_ != current.invert_) {
        invokeGL(glSampleCoverage(value_, invert_ ? GL_TRUE : GL_FALSE));
    }
}


void SettingData::Scissor::init()
{
    GLint value[4];
    invokeGL(glGetIntegerv(GL_SCISSOR_BOX, value));
    x_ = value[0];
    y_ = value[1];
    width_ = value[2];
    height_ = value[3];
}

bool SettingData::Scissor::box(
    int32_t x, int32_t y, uint32_t width, uint32_t height)
{
    if (!equals(x, y, width, height)) {
        x_ = x;
        y_ = y;
        width_ = width;
        height_ = height;
        invokeGL(glScissor(x, y, width, height));
        return true;
    }
    return false;
}

void SettingData::Scissor::rollbackFrom(const Scissor& current)
{
    if (!equals(current)) {
        invokeGL(glScissor(x_, y_, width_, height_));
    }
}


void SettingData::Stencil::init()
{
    func_[0].set(SG_ALWAYS, 0, 0xffffffff);
    func_[1] = func_[0];
    op_[0].set(SG_KEEP, SG_KEEP, SG_KEEP);
    op_[1] = op_[0];
    mask_[0] = mask_[1] = 0xffffffff;
}

bool SettingData::Stencil::func(SGFunction f, int32_t r, uint32_t m)
{
    if (!func_[0].equals(f, r, m) || !func_[1].equals(f, r, m)) {
        invokeGL(glStencilFunc(funcs[f], r, m));
        func_[0].set(f, r, m);
        func_[1] = func_[0];
        return true;
    }
    return false;
}

bool SettingData::Stencil::func(SGFace face, SGFunction f, int32_t r, uint32_t m)
{
    bool diff = false;
    switch (face) {
    case SG_FRONT:
        if (!func_[0].equals(f, r, m)) {
            func_[0].set(f, r, m);
            diff = true;
        }
        break;
    case SG_BACK:
        if (!func_[1].equals(f, r, m)) {
            func_[1].set(f, r, m);
            diff = true;
        }
        break;
    case SG_FRONT_AND_BACK:
        if (!func_[0].equals(f, r, m) || !func_[1].equals(f, r, m)) {
            func_[0].set(f, r, m);
            func_[1] = func_[1];
            diff = true;
        }
        break;
    }
    if (diff) {
        invokeGL(glStencilFuncSeparate(faces[face], funcs[f], r, m));
        return true;
    }
    return false;
}

bool SettingData::Stencil::op(SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass)
{
    if (!op_[0].equals(sfail, dfail, dppass) ||
        !op_[1].equals(sfail, dfail, dppass))
    {
        invokeGL(glStencilFunc(ops[sfail], ops[dfail], ops[dppass]));
        op_[0].set(sfail, dfail, dppass);
        op_[1] = op_[0];
        return true;
    }
    return false;
}

bool SettingData::Stencil::op(SGFace face, SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass)
{
    bool diff = false;
    switch (face) {
    case SG_FRONT:
        if (!op_[0].equals(sfail, dfail, dppass)) {
            op_[0].set(sfail, dfail, dppass);
            diff = true;
        }
        break;
    case SG_BACK:
        if (!op_[1].equals(sfail, dfail, dppass)) {
            op_[1].set(sfail, dfail, dppass);
            diff = true;
        }
        break;
    case SG_FRONT_AND_BACK:
        if (!op_[0].equals(sfail, dfail, dppass) ||
            !op_[1].equals(sfail, dfail, dppass))
        {
            op_[0].set(sfail, dfail, dppass);
            op_[0] = op_[1];
            diff = true;
        }
        break;
    }
    if (diff) {
        invokeGL(glStencilOpSeparate(faces[face], ops[sfail], ops[dfail], ops[dppass]));
        return true;
    }
    return false;
}

void SettingData::Stencil::Func::set(
    SGFunction f, int32_t r, uint32_t m)
{
    func = (uint8_t)f;
    ref = r;
    mask = m;
}

void SettingData::Stencil::Op::set(
    SGStencilOp sfail, SGStencilOp dpfail, SGStencilOp dppass)
{
    sfail_ = (uint8_t)sfail;
    dpfail_ = (uint8_t)dpfail;
    dppass_ = (uint8_t)dppass;
}

bool SettingData::Stencil::mask(uint32_t m)
{
    if (mask_[0] != m || mask_[1] != m) {
        mask_[0] = mask_[1] = m;
        invokeGL(glStencilMask(m));
        return true;
    }
    return false;
}

bool SettingData::Stencil::mask(SGFace face, uint32_t m)
{
    bool diff = false;
    switch (face) {
    case SG_FRONT:
        if (mask_[0] != m) {
            mask_[0] = m;
            diff = true;
        }
        break;
    case SG_BACK:
        if (mask_[1] != m) {
            mask_[1] = m;
            diff = true;
        }
        break;
    case SG_FRONT_AND_BACK:
        if (mask_[0] != m || mask_[1] != m) {
            mask_[0] = mask_[1] = m;
            diff = true;
        }
        break;
    }
    if (diff) {
        invokeGL(glStencilMaskSeparate(faces[face], m));
        return true;
    }
    return false;
}

void SettingData::Stencil::rollbackFrom(const Stencil& current)
{
    GLenum face[] = {GL_FRONT, GL_BACK};

    for (int i = 0; i < 2; ++i) {
        if (!func_[i].equals(current.func_[i])) {
            invokeGL(glStencilFuncSeparate(
                face[i], funcs[func_[i].func], func_[i].ref, func_[i].mask));
        }
        if (!op_[i].equals(current.op_[i])) {
            invokeGL(glStencilOpSeparate(
                face[i], op_[0].sfail_, op_[0].dpfail_, op_[0].dppass_));
        }
        if (mask_[i] != current.mask_[i]) {
            invokeGL(glStencilMaskSeparate(face[i], mask_[0]));
        }
    }
}


void SettingData::Renderer::init()
{
    enable_wireframe = 0;
    wire_color = {0,0,0,1};
    enable_lighting = 1;
}

}
