/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/MatrixStack.h"

#ifdef TARGET_OS_WIN

// align 16 byte because of SSE.

static void* operator new(size_t size, int align) {
    return ::_aligned_malloc(size, align);
}

template<class T>
static void destroy(T* p, int /*align*/) {
    if (p) {
        p->~T();
        _aligned_free(p);
    }
}

#define SG_ELEMENT_NEW(type)    new(16) type
#define SG_ELEMENT_DELETE(ptr)  destroy(ptr, 16)

// disable "nonstandard extension used"
#pragma warning(disable:4201)
#endif

#include "Utility.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

namespace sg {

class SG_LOCAL_API MatrixStackImpl {
public:

    MatrixStackImpl()
        : mDirtyProjection(true)
        , mDirtyView(true)
        , mDirtyModel(true)
    {
        setProjection(Ortho{ -1, 1, -1, 1, -1, 1 });
        mModelViewProjectionMatrix = SG_ELEMENT_NEW(glm::mat4);
        mModelViewMatrix = SG_ELEMENT_NEW(glm::mat4);
        mViewMatrix = SG_ELEMENT_NEW(glm::mat4);
    }

    ~MatrixStackImpl() {
        SG_ELEMENT_DELETE(mViewMatrix);
        SG_ELEMENT_DELETE(mModelViewMatrix);
        SG_ELEMENT_DELETE(mModelViewProjectionMatrix);
    }

    void compute()
    {
        if (!mDirtyProjection && !mDirtyView && !mDirtyModel) {
            return ;
        }

        glm::mat4 proj;
        auto& value = mProjection.value;
        switch (mProjection.type) {
        case SG_PERSPECTIVE:
            proj = glm::perspective(
                value.perspective.fovy,
                value.perspective.aspect,
                value.perspective.znear,
                value.perspective.zfar);
            break;
        case SG_FRUSTUM:
            proj = glm::frustum(
                value.frustum.left,
                value.frustum.right,
                value.frustum.bottom,
                value.frustum.top,
                value.frustum.znear,
                value.frustum.zfar);
            break;
        case SG_ORTHO:
            proj = glm::ortho(
                value.ortho.left,
                value.ortho.right,
                value.ortho.bottom,
                value.ortho.top,
                value.ortho.znear,
                value.ortho.zfar);
            break;
        }
        mDirtyProjection = false;

        if (mDirtyView || mDirtyModel) {
            *mModelViewMatrix = (*mViewMatrix) * (*mModelMatrix.last());
            mDirtyView = false;
            mDirtyModel = false;
        }

        *mModelViewProjectionMatrix = proj * (*mModelViewMatrix);
    }

    void getMVPMatrix(Matrix4& out)
    {
        compute();

        memcpy(out.m, glm::value_ptr(*mModelViewProjectionMatrix), sizeof(Matrix4));
    }

    void getMVMatrix(Matrix4& out)
    {
        compute();

        memcpy(out.m, glm::value_ptr(*mModelViewMatrix), sizeof(Matrix4));
    }

    void getMMatrix(Matrix4& out)
    {
        memcpy(out.m, glm::value_ptr(*mModelMatrix.last()), sizeof(Matrix4));
    }

    void setProjection(const Perspective& perspective)
    {
        if (mDirtyProjection || mProjection != perspective) {
            mProjection.set(perspective);
            mDirtyProjection = true;
        }
    }

    void setProjection(const Frustum& frustum)
    {
        if (mDirtyProjection || mProjection != frustum) {
            mProjection.set(frustum);
            mDirtyProjection = true;
        }
    }

    void setProjection(const Ortho& ortho)
    {
        if (mDirtyProjection || mProjection != ortho) {
            mProjection.set(ortho);
            mDirtyProjection = true;
        }
    }

    void push()
    {
        auto last = mModelMatrix.last();
        auto new_one = mModelMatrix.push();
        *new_one = *last;
    }

    void pop()
    {
        mModelMatrix.pop();
    }

    void identity()
    {
        *mModelMatrix.last() = glm::mat4();
    }

    void lookAt(
        const Position3& eye, const Position3& at, const Vector3& up)
    {
        *mViewMatrix *= glm::lookAt(
            glm::vec3(eye.x, eye.y, eye.z),
            glm::vec3(at.x, at.y, at.z),
            glm::vec3(up.x, up.y, up.z));

        mDirtyView = true;
    }

    void cameraPose(const Position3& position, const Quaternion& pose)
    {
        *mViewMatrix =
            glm::mat4_cast(glm::inverse(glm::quat(pose.w, pose.x, pose.y, pose.z)))
            * glm::translate(glm::mat4(), glm::vec3(-position.x, -position.y, -position.z));

        mDirtyView = true;
    }

    void getCameraPosition(Position3& position)
    {
        position.x = -(*mViewMatrix)[3].x;
        position.y = -(*mViewMatrix)[3].y;
        position.z = -(*mViewMatrix)[3].z;
    }

    void translate(const Vector3& vec)
    {
        auto mat = mModelMatrix.last();

        *mat = glm::translate(
            *mat,
            glm::vec3(vec.x, vec.y, vec.z));

        mDirtyModel = true;
    }

    void rotate(float angle, const Vector3& coord)
    {
        auto mat = mModelMatrix.last();

        *mat = glm::rotate(
            *mat,
            angle,
            glm::vec3(coord.x, coord.y, coord.z));

        mDirtyModel = true;
    }

    void rotate(const Quaternion& quat)
    {
        *(mModelMatrix.last()) *= glm::mat4_cast(
            glm::quat(quat.w, quat.x, quat.y, quat.z));

        mDirtyModel = true;
    }

    void scale(const Vector3& factor)
    {
        auto mat = mModelMatrix.last();
        *mat = glm::scale(
            *mat,
            glm::vec3(factor.x, factor.y, factor.z));

        mDirtyModel = true;
    }

    void multiply(const Matrix4& matrix)
    {
        auto mat = mModelMatrix.last();

        *mat *= glm::make_mat4(matrix.m);

        mDirtyModel = true;
    }


    bool mDirtyProjection;
    bool mDirtyView;
    bool mDirtyModel;

    glm::mat4* mModelViewProjectionMatrix;
    glm::mat4* mModelViewMatrix;
    glm::mat4* mViewMatrix;
    Projection mProjection;
    Stack<glm::mat4> mModelMatrix;
};

MatrixStack::MatrixStack()
    : mImpl(new MatrixStackImpl())
{
}

MatrixStack::~MatrixStack()
{
    clearObserver();
    delete mImpl;
}

void MatrixStack::getModelViewProjectionMatrix(Matrix4& out)
{
    mImpl->getMVPMatrix(out);
}

void MatrixStack::getMVPMatrix(Matrix4& out)
{
    getModelViewProjectionMatrix(out);
}

void MatrixStack::getModelViewMatrix(Matrix4& out)
{
    mImpl->getMVMatrix(out);
}

void MatrixStack::getModelMatrix(Matrix4& out)
{
    mImpl->getMMatrix(out);
}

void MatrixStack::project(const Perspective& perspective)
{
    mImpl->setProjection(perspective);
}

void MatrixStack::project(const Frustum& frustum)
{
    mImpl->setProjection(frustum);
}

void MatrixStack::project(const Ortho& ortho)
{
    mImpl->setProjection(ortho);
}

void MatrixStack::push()
{
    mImpl->push();
}

void MatrixStack::pop()
{
    mImpl->pop();
}

void MatrixStack::identity()
{
    mImpl->identity();
}

void MatrixStack::lookAt(
    const Position3& eye, const Position3& at, const Vector3& up)
{
    mImpl->lookAt(eye, at, up);
}

void MatrixStack::cameraPose(const Position3& position, const Quaternion& pose)
{
    mImpl->cameraPose(position, pose);
}

Position3 MatrixStack::getCameraPosition()
{
    Position3 position;
    mImpl->getCameraPosition(position);
    return position;
}

void MatrixStack::translate(const Vector3& vec)
{
    mImpl->translate(vec);
}

void MatrixStack::rotate(float angle, const Vector3& coord)
{
    mImpl->rotate(angle, coord);
}

void MatrixStack::rotate(const Quaternion& quat)
{
    mImpl->rotate(quat);
}

void MatrixStack::scale(const Vector3& factor)
{
    mImpl->scale(factor);
}

void MatrixStack::multiply(const Matrix4& matrix)
{
    mImpl->multiply(matrix);
}

MatrixStack::WeakRef::WeakRef(MatrixStack* stack)
    : mStack(stack)
{
    if (mStack) {
        mStack->addObserver(this);
    }
}

MatrixStack::WeakRef::WeakRef(const WeakRef& rhv)
    : WeakRef(rhv.mStack)
{
}

MatrixStack::WeakRef::~WeakRef()
{
    if (mStack) {
        mStack->removeObserver(this);
        mStack = nullptr;
    }
}

MatrixStack* MatrixStack::WeakRef::operator->()
{
    return mStack;
}

void MatrixStack::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mStack);
}

void MatrixStack::WeakRef::operator=(MatrixStack* stack)
{
    if (mStack == stack) {
        return;
    }

    if (mStack) {
        mStack->removeObserver(this);
    }

    mStack = stack;

    if (mStack) {
        mStack->addObserver(this);
    }
}

MatrixStack::WeakRef::operator MatrixStack*() const
{
    return mStack;
}

void MatrixStack::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mStack = nullptr;
}

}
