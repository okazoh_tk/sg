/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Property.h"
#include "sg/MemoryAllocator.h"
#include <cstring>
#include <string>

namespace sg {

Property::Property(DataType type)
    : mDirty(1)
    , mType((uint32_t)type)
    , mCount(1)
{
}

Property::~Property()
{
}

bool Property::isDirty() const
{
    return mDirty != 0;
}

void Property::clear()
{
    mDirty = 0;
}

void Property::dirty()
{
    mDirty = true;
}

DataType Property::getType() const
{
    return (DataType)mType;
}

uint32_t Property::getCount() const
{
    return mCount;
}

const void* Property::getPointer() const
{
    return 0;
}

template<class T>
std::string to_string(const T* values, uint32_t count) {
    std::string str(std::to_string(values[0]));
    for (uint32_t i = 1; i < count; ++i) {
        str += ", " + std::to_string(values[i]);
    }

    return str;
}


void Property::print(const char* key)
{
    std::string values = "unknown";

    switch (getType()) {
    case TYPE_BOOL:
        values = std::to_string(*((BoolProperty*)this));
        break;
    case TYPE_BVEC2:
        values = to_string<bool>((const bool*)this->getPointer(), 2);
        break;
    case TYPE_BVEC3:
        values = to_string<bool>((const bool*)this->getPointer(), 3);
        break;
    case TYPE_BVEC4:
        values = to_string<bool>((const bool*)this->getPointer(), 4);
        break;
    case TYPE_INT:
        values = std::to_string((int32_t)(*(IntProperty*)this));
        break;
    case TYPE_IVEC2:
        values = to_string<int32_t>((const int32_t*)this->getPointer(), 2);
        break;
    case TYPE_IVEC3:
        values = to_string<int32_t>((const int32_t*)this->getPointer(), 3);
        break;
    case TYPE_IVEC4:
        values = to_string<int32_t>((const int32_t*)this->getPointer(), 4);
        break;
    case TYPE_FLOAT:
        values = std::to_string((float)(*(FloatProperty*)this));
        break;
    case TYPE_VEC2:
        values = to_string<float>((const float*)this->getPointer(), 2);
        break;
    case TYPE_VEC3:
        values = to_string<float>((const float*)this->getPointer(), 3);
        break;
    case TYPE_VEC4:
        values = to_string<float>((const float*)this->getPointer(), 4);
        break;
    case TYPE_MAT2:
        values = to_string<float>((const float*)this->getPointer(), 4);
        break;
    case TYPE_MAT3:
        values = to_string<float>((const float*)this->getPointer(), 9);
        break;
    case TYPE_MAT4:
        values = to_string<float>((const float*)this->getPointer(), 16);
        break;
    case TYPE_MATRIX_STACK:
    {
        auto ptr = (sg::MatrixStack*)*((const MatrixStackProperty*)this);
        if (ptr) {
            Matrix4 mat;
            ptr->getModelViewProjectionMatrix(mat);
            values = to_string<float>(mat.m, 16);
        }
        else {
            values = "\"null\"";
        }
    }
        break;
    case TYPE_MV_MATRIX:
    {
        auto ptr = (sg::MatrixStack*)*((const MatrixStackProperty*)this);
        if (ptr) {
            Matrix4 mat;
            ptr->getModelViewMatrix(mat);
            values = to_string<float>(mat.m, 16);
        }
        else {
            values = "\"null\"";
        }
    }
        break;
    case TYPE_M_MATRIX:
    {
        auto ptr = (sg::MatrixStack*)*((const MatrixStackProperty*)this);
        if (ptr) {
            Matrix4 mat;
            ptr->getModelMatrix(mat);
            values = to_string<float>(mat.m, 16);
        }
        else {
            values = "\"null\"";
        }
    }
        break;
    case TYPE_TEXTURE:
    default:
        break;
    }
    LOGD("\"%s\":[%s]", key, values.c_str());
}


#define _clone(type) new type((const type&)*this)
Property* Property::clone() const
{
    switch (getType()) {
    case TYPE_BOOL:
        return _clone(BoolProperty);
    case TYPE_BVEC2:
        return _clone(BoolVector2Property);
    case TYPE_BVEC3:
        return _clone(BoolVector3Property);
    case TYPE_BVEC4:
        return _clone(BoolVector4Property);
    case TYPE_INT:
        return _clone(IntProperty);
    case TYPE_IVEC2:
        return _clone(IntVector2Property);
    case TYPE_IVEC3:
        return _clone(IntVector3Property);
    case TYPE_IVEC4:
        return _clone(IntVector4Property);
    case TYPE_FLOAT:
        return _clone(FloatProperty);
    case TYPE_VEC2:
        return _clone(Vector2Property);
    case TYPE_VEC3:
        return _clone(Vector3Property);
    case TYPE_VEC4:
        return _clone(Vector4Property);
    case TYPE_MAT2:
        return _clone(Matrix2Property);
    case TYPE_MAT3:
        return _clone(Matrix3Property);
    case TYPE_MAT4:
        return _clone(Matrix4Property);
    case TYPE_MATRIX_STACK:
        return new MatrixStackProperty(*((const MatrixStackProperty*)this));
    case TYPE_MV_MATRIX:
        return new MVMatrixProperty(*((const MVMatrixProperty*)this));
    case TYPE_M_MATRIX:
        return new MMatrixProperty(*((const MMatrixProperty*)this));
    case TYPE_TEXTURE:
        return new TextureProperty(*((const TextureProperty*)this));
    default:
        break;
    }

    return nullptr;
}

#define _copy(type) ((type*)this)->set(((const type&)src).get(), src.getCount())
bool Property::copyFrom(const Property& src)
{
    if (getType() != src.getType()) {
        return false;
    }

    switch (getType()) {
    case TYPE_INT:
        _copy(IntProperty);
        break;
    case TYPE_IVEC2:
        _copy(IntVector2Property);
        break;
    case TYPE_IVEC3:
        _copy(IntVector3Property);
        break;
    case TYPE_IVEC4:
        _copy(IntVector4Property);
        break;
    case TYPE_FLOAT:
        _copy(FloatProperty);
        break;
    case TYPE_VEC2:
        _copy(Vector2Property);
        break;
    case TYPE_VEC3:
        _copy(Vector3Property);
        break;
    case TYPE_VEC4:
        _copy(Vector4Property);
        break;
    case TYPE_MAT2:
        _copy(Matrix2Property);
        break;
    case TYPE_MAT3:
        _copy(Matrix3Property);
        break;
    case TYPE_MAT4:
        _copy(Matrix4Property);
        break;
    case TYPE_BOOL:
        _copy(BoolProperty);
        break;
    case TYPE_BVEC2:
        _copy(BoolVector2Property);
        break;
    case TYPE_BVEC3:
        _copy(BoolVector3Property);
        break;
    case TYPE_BVEC4:
        _copy(BoolVector4Property);
        break;
    case TYPE_MATRIX_STACK:
        ((MatrixStackProperty*)this)->set((const MatrixStackProperty&)src);
        break;
    case TYPE_MV_MATRIX:
        ((MVMatrixProperty*)this)->set((const MVMatrixProperty&)src);
        break;
    case TYPE_TEXTURE:
        ((TextureProperty*)this)->set((const TextureProperty&)src);
        break;
    default:
        break;
    }
    return true;
}

/* Macro starts */
#define DEFINE_PROPERTY_PRIM(name, type, id) \
name##Property::name##Property() : Property(id){}\
name##Property::name##Property(const name##Property& rhv) : Property(id) { \
    if (rhv.getCount() == 1) { \
        set(rhv);\
    } else {\
        set(rhv.get(), rhv.getCount());\
    }\
}\
name##Property::name##Property(type value) : Property(id) { \
    set(value);\
}\
name##Property::~name##Property(){\
    if (mCount > 1) {\
        sysFree(mValue.pointer);\
    }\
}\
\
const void* name##Property::getPointer() const {\
    return get(); \
}\
\
void name##Property::operator=(type value) {\
    set(value);\
}\
\
void name##Property::set(type value) {\
    auto* p = mValue.pointer; \
    if (mCount == 1) { \
        p = &mValue.value; \
    } else { \
        mCount = 1; \
    }\
    \
    if (*p != value) {\
        *p = value; \
        dirty(); \
    }\
}\
\
void name##Property::set(const type* values, uint32_t count) {\
    if (count == 0) { \
        /* todo: print error */ \
        return;\
    }\
    \
    if (count == 1) {\
        if (mCount != 1) {\
            sysFree(mValue.pointer);\
        }\
        if (mValue.value != values[0]) {\
            mValue.value = values[0];\
            dirty();\
        }\
    } else {\
        if (mCount < count) {\
            sysFree(mValue.pointer);\
            mValue.pointer = (type*)sysAlloc(sizeof(type) * count);\
        }\
        memcpy(mValue.pointer, values, sizeof(type) * count);\
        dirty();\
    }\
    mCount = count;\
}\
\
name##Property::operator type() const{\
    return get(0);\
}\
\
type name##Property::get(uint32_t index) const{\
    if (mCount == 1) {\
        return mValue.value;\
    } else {\
        return mValue.pointer[(index >= mCount) ? (mCount - 1) : index];\
    }\
}\
\
const type* name##Property::get() const {\
    if (mCount == 1) { \
        return &mValue.value; \
    } else { \
        return mValue.pointer;\
    }\
}\
\
type* name##Property::getPointerForWrite(){\
    dirty(); \
    if (mCount == 1) { \
        return &mValue.value; \
    } else { \
        return mValue.pointer;\
    }\
}

template<class T>
static bool equals(const T& a, const T& b) {
    return 0 == memcmp(&a, &b, sizeof(T));
}

#define DEFINE_PROPERTY(name, type, id) \
name##Property::name##Property() : Property(id){}\
name##Property::name##Property(const type& value) : Property(id) { \
    set(value);\
}\
name##Property::name##Property(const name##Property& rhv) : Property(id) { \
    if (rhv.getCount() == 1) { \
        set(rhv);\
    } else {\
        set(rhv.get(), rhv.getCount());\
    }\
}\
name##Property::~name##Property(){\
    if (mCount > 1) {\
        sysFree(mValue.pointer);\
    }\
}\
\
const void* name##Property::getPointer() const {\
    return get(); \
}\
\
void name##Property::operator=(const type& value) {\
    set(value);\
}\
\
void name##Property::set(const type& value) {\
    auto* p = mValue.pointer; \
    if (mCount == 1) { \
        p = &mValue.value; \
        } else { \
        mCount = 1; \
        }\
    \
    if (!equals(*p, value)) {\
        *p = value; \
        dirty(); \
        }\
}\
\
void name##Property::set(const type* values, uint32_t count) {\
    if (count == 0) { \
        /* todo: print error */ \
        return;\
        }\
    \
    if (count == 1) {\
        if (mCount != 1) {\
            sysFree(mValue.pointer);\
                }\
        if (!equals(mValue.value, values[0])) {\
            mValue.value = values[0];\
            dirty();\
                }\
        } else {\
        if (mCount < count) {\
            sysFree(mValue.pointer);\
            mValue.pointer = (type*)sysAlloc(sizeof(type) * count);\
                }\
        memcpy(mValue.pointer, values, sizeof(type) * count);\
        dirty();\
        }\
    mCount = count;\
}\
\
name##Property::operator const type() const{\
    return get(0);\
}\
\
const type name##Property::get(uint32_t index) const{\
    if (mCount == 1) {\
        return mValue.value;\
        } else {\
        return mValue.pointer[(index >= mCount) ? (mCount - 1) : index];\
        }\
}\
\
const type* name##Property::get() const {\
    if (mCount == 1) { \
        return &mValue.value; \
        } else { \
        return mValue.pointer;\
        }\
}\
\
type* name##Property::getPointerForWrite(){\
    dirty(); \
    if (mCount == 1) { \
        return &mValue.value; \
    } else { \
        return mValue.pointer;\
    }\
}

/* Macro ends */


DEFINE_PROPERTY_PRIM(Bool, bool, TYPE_BOOL);
DEFINE_PROPERTY(BoolVector2, BoolVector2, TYPE_BVEC2);
DEFINE_PROPERTY(BoolVector3, BoolVector3, TYPE_BVEC3);
DEFINE_PROPERTY(BoolVector4, BoolVector4, TYPE_BVEC4);
DEFINE_PROPERTY_PRIM(Int, int32_t, TYPE_INT);
DEFINE_PROPERTY(IntVector2, IntVector2, TYPE_IVEC2);
DEFINE_PROPERTY(IntVector3, IntVector3, TYPE_IVEC3);
DEFINE_PROPERTY(IntVector4, IntVector4, TYPE_IVEC4);
DEFINE_PROPERTY_PRIM(Float, float, TYPE_FLOAT);
DEFINE_PROPERTY(Vector2, Vector2, TYPE_VEC2);
DEFINE_PROPERTY(Vector3, Vector3, TYPE_VEC3);
DEFINE_PROPERTY(Vector4, Vector4, TYPE_VEC4);
DEFINE_PROPERTY(Matrix2, Matrix2, TYPE_MAT2);
DEFINE_PROPERTY(Matrix3, Matrix3, TYPE_MAT3);
DEFINE_PROPERTY(Matrix4, Matrix4, TYPE_MAT4);


MatrixStackProperty::MatrixStackProperty()
    : MatrixStackProperty(TYPE_MATRIX_STACK)
{
}

MatrixStackProperty::MatrixStackProperty(MatrixStack* stack)
    : MatrixStackProperty(TYPE_MATRIX_STACK, stack)
{
}

MatrixStackProperty::MatrixStackProperty(const MatrixStackProperty& rhv)
    : MatrixStackProperty(TYPE_MATRIX_STACK, rhv)
{
}

MatrixStackProperty::MatrixStackProperty(DataType type)
    : MatrixStackProperty(type, nullptr)
{
}

MatrixStackProperty::MatrixStackProperty(DataType type, MatrixStack* stack)
    : Property(type)
    , mValue(stack)
{
}

MatrixStackProperty::MatrixStackProperty(DataType type, const MatrixStackProperty& rhv)
    : Property(type)
{
    set(rhv.mValue);
}

MatrixStackProperty::~MatrixStackProperty()
{
    if (mCount > 1) {
        sysFree(mValue);
    }
}

const void* MatrixStackProperty::getPointer() const
{
    return mValue;
}

void MatrixStackProperty::operator= (MatrixStack* value)
{
    set(value);
}

void MatrixStackProperty::set(MatrixStack* value)
{
    mValue = value;
}

MatrixStackProperty::operator MatrixStack*() const
{
    return get();
}

MatrixStack* MatrixStackProperty::get() const
{
    return mValue;
}


MVMatrixProperty::MVMatrixProperty()
    : MatrixStackProperty(TYPE_MV_MATRIX)
{
}

MVMatrixProperty::MVMatrixProperty(MatrixStack* stack)
    : MatrixStackProperty(TYPE_MV_MATRIX, stack)
{
}

MVMatrixProperty::MVMatrixProperty(const MVMatrixProperty& rhv)
    : MatrixStackProperty(TYPE_MV_MATRIX, rhv)
{
}

MVMatrixProperty::~MVMatrixProperty()
{
}


MMatrixProperty::MMatrixProperty()
    : MatrixStackProperty(TYPE_M_MATRIX)
{
}

MMatrixProperty::MMatrixProperty(MatrixStack* stack)
    : MatrixStackProperty(TYPE_M_MATRIX, stack)
{
}

MMatrixProperty::MMatrixProperty(const MMatrixProperty& rhv)
    : MatrixStackProperty(TYPE_M_MATRIX, rhv)
{
}

MMatrixProperty::~MMatrixProperty()
{
}


TextureProperty::TextureProperty()
    : Property(TYPE_TEXTURE)
    , mValue(nullptr)
{
}

TextureProperty::TextureProperty(Texture* texture)
    : Property(TYPE_TEXTURE)
{
    set(texture);
}

TextureProperty::TextureProperty(const TextureProperty& rhv)
    : Property(TYPE_TEXTURE)
{
    set(rhv.mValue);
}

TextureProperty::~TextureProperty()
{
    if (mCount > 1) {
        sysFree(mValue);
    }
}

const void* TextureProperty::getPointer() const
{
    return mValue;
}

void TextureProperty::operator=(Texture* value)
{
    set(value);
}

void TextureProperty::set(Texture* value)
{
    mValue = value;
}

TextureProperty::operator Texture*() const
{
    return get();
}

Texture* TextureProperty::get() const
{
    return mValue;
}

}
