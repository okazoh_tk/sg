/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/SharedObject.h"

namespace sg {

class SG_LOCAL_API SharedObjectImpl {
public:
    SharedObjectImpl() : mObservers(nullptr) {}

    SharedObject::Observer* mObservers;
};

SharedObject::SharedObject()
    : mImpl(new SharedObjectImpl())
{
}

SharedObject::~SharedObject()
{
    delete mImpl;
}

void SharedObject::addObserver(Observer* observer)
{
    if (observer) {
        observer->mNext = mImpl->mObservers;
        mImpl->mObservers = observer;
    }
}

void SharedObject::removeObserver(Observer* observer)
{
    auto iter = mImpl->mObservers;
    Observer* prev = nullptr;
    while (iter) {
        if (iter == observer) {
            if (prev) {
                prev->mNext = iter->mNext;
            } else {
                mImpl->mObservers = iter->mNext;
            }
            iter->mNext = nullptr;
            break;
        }
        prev = iter;
        iter = iter->mNext;
    }
}

bool SharedObject::hasMultiObservers() const
{
    return mImpl->mObservers && mImpl->mObservers->mNext;
}

bool SharedObject::hasNoObservers() const
{
    return nullptr == mImpl->mObservers;
}

void SharedObject::clearObserver()
{
    while (mImpl->mObservers) {
        auto iter = mImpl->mObservers;
        mImpl->mObservers = mImpl->mObservers->mNext;
        iter->mNext = nullptr;
        iter->onReleased(this);
    }
}


SharedObject::Observer::Observer()
    : mNext(nullptr)
{
}

SharedObject::Observer::~Observer()
{
}

}