/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/RenderTarget.h"
#include "OpenGL.h"

namespace sg {

class SG_LOCAL_API RenderBuffer {
public:
    RenderBuffer() : mNext(nullptr) {}
    virtual ~RenderBuffer() {}

    virtual void prepare(const Size& size) = 0;
    virtual void activate(GLenum attachment) = 0;

    virtual Texture* getTexture() { return nullptr; }

    void setNext(RenderBuffer* next) {
        mNext = next;
    }

    RenderBuffer* getNext() {
        return mNext;
    }

private:
    RenderBuffer* mNext;
};

class SG_LOCAL_API RenderBufferImpl : public RenderBuffer {
public:
    RenderBufferImpl(GLenum internalformat)
        : mName(0)
        , mFormat(internalformat)
    {
    }

    virtual ~RenderBufferImpl()
    {
        if (mName) {
            invokeGL(glDeleteRenderbuffers(1, &mName));
            mName = 0;
        }
    }

    virtual void prepare(const Size& size) {

        invokeGL(glGenRenderbuffers(1, &mName));

        invokeGL(glBindRenderbuffer(GL_RENDERBUFFER, mName));
        invokeGL(glRenderbufferStorage(GL_RENDERBUFFER, mFormat, size.width, size.height));
    }

    virtual void activate(GLenum attachment) {
        invokeGL(glFramebufferRenderbuffer(
            GL_FRAMEBUFFER,
            attachment,
            GL_RENDERBUFFER,
            mName));
    }
private:
    GLuint mName;
    GLenum mFormat;
};

class SG_LOCAL_API TextureRenderBuffer : public RenderBuffer {
public:
    TextureRenderBuffer(Texture::Format format) :mFormat(format) {}
    virtual ~TextureRenderBuffer() {}

    virtual void prepare(const Size& size) {
        mTexture.set(size.width, size.height, mFormat);
        mTexture.bind();
    }

    virtual void activate(GLenum attachment) {
        invokeGL(glFramebufferTexture2D(
            GL_FRAMEBUFFER,
            attachment,
            GL_TEXTURE_2D,
            mTexture.getName(),
            0));
    }

    virtual Texture* getTexture() { return &mTexture; }

private:
    Texture::Format mFormat;
    Texture mTexture;
};

RenderTarget::RenderTarget(const Parameter& param)
    : mParam(param)
    , mName(0)
    , mColorBuffers(nullptr)
    , mDepthBuffer(nullptr)
    , mStencilBuffer(nullptr)
{
}

RenderTarget::~RenderTarget()
{
    clear();
}

void RenderTarget::clear()
{
    if (mColorBuffers) {
        auto iter = mColorBuffers;
        while (iter) {
            auto next = iter->getNext();
            delete iter;
            iter = next;
        }
        mColorBuffers = nullptr;
    }
    if (mDepthBuffer) {
        delete mDepthBuffer;
        mDepthBuffer = nullptr;
    }
    if (mStencilBuffer) {
        delete mStencilBuffer;
        mStencilBuffer = nullptr;
    }

    if (mName) {
        invokeGL(glDeleteFramebuffers(1, &mName));
        mName = 0;
    }
}

void RenderTarget::bind()
{
    if (mName == 0) {
        prepare();
    }

    invokeGL(glBindFramebuffer(GL_FRAMEBUFFER, mName));
}

void RenderTarget::unbind()
{
    invokeGL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

void RenderTarget::prepare()
{
    TextureRenderBuffer* tex;
    TextureRenderBuffer* last = nullptr;
    for (uint16_t i = 0; i < mParam.color.num; ++i) {
        tex = new TextureRenderBuffer(mParam.color.buffers[i].format);
        tex->prepare(mParam.size);
        if (last == nullptr) {
            mColorBuffers = tex;
        } else {
            last->setNext(tex);
        }
        last = tex;
    }

    if (mParam.depth.enable) {
        if (mParam.depth.as_texture) {
            mDepthBuffer = new TextureRenderBuffer(Texture::DEPTH);
        } else {
            mDepthBuffer = new RenderBufferImpl(GL_DEPTH_COMPONENT16);
        }
        mDepthBuffer->prepare(mParam.size);
    }

    if (mParam.stencil.enable) {
        mDepthBuffer = new RenderBufferImpl(GL_STENCIL_INDEX8);
        mDepthBuffer->prepare(mParam.size);
    }

    // init framebuffers
    invokeGL(glGenFramebuffers(1, &mName));
    invokeGL(glBindFramebuffer(GL_FRAMEBUFFER, mName));

    auto cb = mColorBuffers;
    for (uint16_t i = 0; i < mParam.color.num; ++i) {
        cb->activate(GL_COLOR_ATTACHMENT0 + i);
        cb = cb->getNext();
    }

    if (mDepthBuffer) {
        mDepthBuffer->activate(GL_DEPTH_ATTACHMENT);
    }

    if (mStencilBuffer) {
        mStencilBuffer->activate(GL_STENCIL_ATTACHMENT);
    }

    // check status
    auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        LOGE("Framebuffer is not completed.");
    }

    invokeGL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

const RenderTarget::Parameter& RenderTarget::getParameter() const
{
    return mParam;
}

Texture* RenderTarget::getColorBuffer(uint8_t index)
{
    if (index > mParam.color.num || nullptr == mColorBuffers) {
        return nullptr;
    }

    // find
    auto iter = mColorBuffers;
    uint8_t i = 0;
    while (i != index) {
        iter = iter->getNext();
        ++i;
    }

    return iter ? iter->getTexture() : nullptr;
}

Texture* RenderTarget::getDepthBuffer()
{
    return mDepthBuffer ? mDepthBuffer->getTexture() : nullptr;
}


RenderTarget::Parameter::Parameter()
    : Parameter({1,1})
{
}

RenderTarget::Parameter::Parameter(const Size& s)
    : Parameter(s, Texture::RGB)
{
}

RenderTarget::Parameter::Parameter(
    const Size& s,
    Texture::Format color_format)
    : Parameter(s, color_format, false)
{
}

RenderTarget::Parameter::Parameter(
    const Size& s,
    Texture::Format color_format,
    bool depth_as_texture)
    : size(s)
    , color({ 1, { { color_format } } })
    , depth({ depth_as_texture, depth_as_texture })
    , stencil({ false })
{
}

RenderTarget::Parameter::Parameter(const Parameter& rhv)
    : size(rhv.size)
    , color(rhv.color)
    , depth(rhv.depth)
    , stencil(rhv.stencil)
{
}


RenderTarget::WeakRef::WeakRef(RenderTarget* target)
    : mTarget(target)
{
    if (mTarget) {
        mTarget->addObserver(this);
    }
}

RenderTarget::WeakRef::WeakRef(const WeakRef& rhv)
    : WeakRef(rhv.mTarget)
{
}

RenderTarget::WeakRef::~WeakRef()
{
    if (mTarget) {
        mTarget->removeObserver(this);
        mTarget = nullptr;
    }
}

RenderTarget* RenderTarget::WeakRef::operator->()
{
    return mTarget;
}

void RenderTarget::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mTarget);
}

void RenderTarget::WeakRef::operator=(RenderTarget* target)
{
    if (mTarget == target) {
        return;
    }

    if (mTarget) {
        mTarget->removeObserver(this);
    }

    mTarget = target;

    if (mTarget) {
        mTarget->addObserver(this);
    }
}

RenderTarget::WeakRef::operator RenderTarget*() const
{
    return mTarget;
}

void RenderTarget::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mTarget = nullptr;
}

}
