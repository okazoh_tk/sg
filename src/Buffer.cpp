/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Buffer.h"
#include "sg/MemoryAllocator.h"
#include "OpenGL.h"
#include <string.h>

namespace sg {

static const GLenum types[] = {
    GL_ARRAY_BUFFER,
    GL_ELEMENT_ARRAY_BUFFER,
#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    GL_COPY_READ_BUFFER,
    GL_COPY_WRITE_BUFFER,
    GL_PIXEL_PACK_BUFFER,
    GL_PIXEL_UNPACK_BUFFER,
    GL_TRANSFORM_FEEDBACK_BUFFER,
    GL_UNIFORM_BUFFER,
#endif
};

static const GLenum usages[] = {
    GL_STREAM_DRAW,
    GL_STATIC_DRAW,
    GL_DYNAMIC_DRAW,
#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    GL_STREAM_READ,
    GL_STREAM_COPY,
    GL_STATIC_READ,
    GL_STATIC_COPY,
    GL_DYNAMIC_READ,
    GL_DYNAMIC_COPY,
#endif
};

Buffer::Buffer(Type type, Usage usage)
    : mName(0)
    , mType(type)
    , mUsage(usage)
{
    VERIFY(type < COUNT_OF(types));
    VERIFY(usage < COUNT_OF(usages));
}

Buffer::~Buffer()
{
    if (0 != mName) {
        invokeGL(glDeleteBuffers(1, &mName));
    }
}

uint32_t Buffer::getName() const
{
    return mName;
}

Buffer::Type Buffer::getType() const
{
    return mType;
}

Buffer::Usage Buffer::getUsage() const
{
    return mUsage;
}

void Buffer::setUsage(Usage usage)
{
    mUsage = usage;
}

bool Buffer::transfer(uint32_t len, const void* data)
{
    if (mName == 0) {
        invokeGL(glGenBuffers(1, &mName));
    }

    if (mName != 0) {
        GLenum type = types[mType];
        invokeGL(glBindBuffer(type, mName));
        invokeGL(glBufferData(type, len, data, usages[mUsage]));
        return true;
    } else {
        return false;
    }
}

bool Buffer::transfer(uint32_t offset, uint32_t len, const void* data)
{
    if (mName != 0) {
        GLenum type = types[mType];
        invokeGL(glBindBuffer(type, mName));
        invokeGL(glBufferSubData(type, offset, len, data));
        return true;
    } else {
        return false;
    }
}

}
