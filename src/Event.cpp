/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Event.h"
#include "Utility.h"

namespace sg {

KeyEvent::KeyEvent(Type type, const Device::Key::State& state)
    : mType(type)
    , mState(state)
{
}

KeyEvent::~KeyEvent()
{
}

KeyEvent::Type KeyEvent::getType() const
{
    return mType;
}

int32_t KeyEvent::getKey() const
{
    return mState.getChar();
}

bool KeyEvent::isPressed() const
{
    return mState.isPressed();
}

ButtonEvent::ButtonEvent(Type type, const Device::Button::State& state)
    : mType(type)
    , mState(state)
{
}

ButtonEvent::~ButtonEvent()
{
}

ButtonEvent::Type ButtonEvent::getType() const
{
    return mType;
}

int32_t ButtonEvent::getID() const
{
    return mState.getID();
}

bool ButtonEvent::isPressed() const
{
    return mState.isPressed();
}


PointerEvent::PointerEvent(Type type, const Device::Pointer::State& state)
    : mType(type)
    , mState(state)
{
}

PointerEvent::~PointerEvent()
{
}

PointerEvent::Type PointerEvent::getType() const
{
    return mType;
}

int32_t PointerEvent::getID() const
{
    return mState.getID();
}

int32_t PointerEvent::getX() const
{
    return mState.getX();
}

int32_t PointerEvent::getY() const
{
    return mState.getY();
}


ScrollEvent::ScrollEvent(int32_t x_offset, int32_t y_offset)
    : mX(x_offset)
    , mY(y_offset)
{
}

ScrollEvent::~ScrollEvent()
{
}

int32_t ScrollEvent::getX() const
{
    return mX;
}

int32_t ScrollEvent::getY() const
{
    return mY;
}

IEventListener::IEventListener()
    : mOwner(nullptr)
{
}


IEventListener::~IEventListener()
{
}

void IEventListener::removeChain()
{
    if (mOwner) {
        mOwner->removeListener();
        mOwner = nullptr;
    }
}

EventListenerChain* IEventListener::getOwner()
{
    return mOwner;
}

void IEventListener::resetOwner(EventListenerChain* owner)
{
    mOwner = owner;
}


KeyEventListener::KeyEventListener()
{
}

KeyEventListener::~KeyEventListener()
{
    LOGV("Removed %s(%p).", getName(), this);
    removeChain();
}

const char* KeyEventListener::getName()
{
    static const char* name = "KeyEventListener";
    return name;
}


ButtonEventListener::ButtonEventListener()
{
}

ButtonEventListener::~ButtonEventListener()
{
    removeChain();
}

const char* ButtonEventListener::getName()
{
    static const char* name = "ButtonEventListener";
    return name;
}


PointerEventListener::PointerEventListener()
{
}

PointerEventListener::~PointerEventListener()
{
    LOGV("Removed %s(%p).", getName(), this);
    removeChain();
}

const char* PointerEventListener::getName()
{
    static const char* name = "PointerEventListener";
    return name;
}


UIEventListener::UIEventListener()
{
}

UIEventListener::~UIEventListener()
{
    LOGV("Removed %s(%p).", getName(), this);
    removeChain();
}

const char* UIEventListener::getName()
{
    static const char* name = "UIEventListener";
    return name;
}

void UIEventListener::onScroll(EventDriver& /*caller*/, const ScrollEvent& /*e*/)
{
}


RendererEventListener::RendererEventListener()
{
}

RendererEventListener::~RendererEventListener()
{
    LOGV("Removed %s(%p).", getName(), this);
    removeChain();
}

const char* RendererEventListener::getName()
{
    static const char* name = "RendererEventListener";
    return name;
}


SystemEventListener::SystemEventListener()
{
}

SystemEventListener::~SystemEventListener()
{
    LOGV("Removed %s(%p).", getName(), this);
    removeChain();
}

const char* SystemEventListener::getName()
{
    static const char* name = "SystemEventListener";
    return name;
}

void SystemEventListener::onLaunching(EventDriver& /*caller*/)
{
}

void SystemEventListener::onSuspend(EventDriver& /*caller*/)
{
}

void SystemEventListener::onResume(EventDriver& /*caller*/)
{
}

void SystemEventListener::onDestroy(EventDriver& /*caller*/)
{
}

}