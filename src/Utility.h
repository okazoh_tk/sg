/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_UTILITY_H_INCLUDED_
#define SG_UTILITY_H_INCLUDED_

#include "sg/Config.h"
#include "sg/Event.h"
#include "sg/MathUtil.h"

namespace sg {

#ifndef SG_ELEMENT_NEW
#define SG_ELEMENT_NEW(type)    new type()
#define SG_ELEMENT_DELETE(ptr) delete ptr
#endif

template<class T>
struct Stack {
    Stack()
        : first_(nullptr)
        , last_(nullptr)
        , reserve_(nullptr)
    {
        push();
    }

    ~Stack() {
        auto release = [](Element* iter) {
            Element* next;
            while (iter) {
                next = iter->next;
                SG_ELEMENT_DELETE(iter);
                iter = next;
            }
        };

        release(reserve_);
        release(first_);
    }

    T* push() {
        Element* element;
        if (reserve_) {
            element = reserve_;
            reserve_ = reserve_->next;
        }
        else {
            element = SG_ELEMENT_NEW(Element);
        }

        element->next = nullptr;
        if (last_) {
            last_->next = element;
            last_ = element;
        }
        else {
            first_ = element;
            last_ = element;
        }

        return &element->data;
    }

    void pop() {
        VERIFY(first_ != last_);

        Element* prev = nullptr;
        auto iter = first_;
        while (iter->next) {
            prev = iter;
            iter = iter->next;
        }

        last_ = prev;
        prev->next = nullptr;

        iter->next = reserve_;
        reserve_ = iter;
    }

    T* last() {
        return &last_->data;
    }

    struct Element {
        T data;
        Element* next;
    };

private:
    Element* first_;
    Element* last_;
    Element* reserve_;
};


class SG_LOCAL_API EventListenerChain {
public:
    EventListenerChain()
    {
    }

    virtual ~EventListenerChain()
    {
    }

    virtual void removeListener() = 0;

protected:
    void resetOwner(IEventListener* listener, EventListenerChain* owner) {
        listener->resetOwner(owner);
    }

    static EventListenerChain* getOwner(IEventListener* listener)
    {
        return listener->getOwner();
    }
};

template<class Listener>
class SG_LOCAL_API TEventListenerChain : public EventListenerChain {
public:
    TEventListenerChain(Listener* listener, int32_t priority)
        : mListener(listener)
        , mPriority(priority)
        , mNext(nullptr)
    {
        resetOwner(mListener, this);
    }

    virtual ~TEventListenerChain()
    {
        if (mListener) {
            // notify that this chain has been removed.
            resetOwner(mListener, nullptr);
            mListener = nullptr;
        }
    }

    virtual void removeListener() {
        mListener = nullptr;
    }

    Listener* getListener() {
        return static_cast<Listener*>(mListener);
    }

    TEventListenerChain* getNext() {
        return mNext;
    }

    class SG_LOCAL_API Head {
    public:
        Head()
            : mHead(nullptr)
            , mCurrent(nullptr)
        {
        }

        ~Head()
        {
            auto iter = mHead;
            while (iter) {
                auto next = iter->getNext();
                delete iter;
                iter = next;
            }
        }

        void add(Listener* listener, int32_t priority) {

            // todo: First, delete chain if listener has a owner!!
            remove(listener);

            auto chain = new TEventListenerChain(listener, priority);
            if (!mHead) {
                mHead = chain;
            } else {
                auto iter = mHead;
                decltype(iter) prev = nullptr;
                do {
                    if (iter->mPriority > priority) {
                        chain->mNext = iter;
                        if (!prev) {
                            mHead = chain;
                        } else {
                            prev->mNext = chain;
                        }
                        break;
                    }
                    prev = iter;
                    iter = iter->mNext;
                    if (!iter) {
                        prev->mNext = chain;
                        break;
                    }
                } while (iter);
            }
        }

        void remove(Listener* listener) {
            auto owner = (TEventListenerChain*)EventListenerChain::getOwner(listener);

            if (!owner) {
                return;
            } else if (owner == mCurrent) {
                // if removing current chain, only do reset listener pointer.
                owner->resetOwner(listener, nullptr);
                owner->removeListener();
                return;
            }
            decltype(owner) iter = owner;
            decltype(iter) prev = nullptr;
            while (iter) {
                if (iter == owner) {
                    if (prev) {
                        prev->mNext = iter->mNext;
                    } else {
                        mHead = iter->mNext;
                    }
                    delete iter;
                    break;
                }
                iter = iter->mNext;
            }
        }

        bool validate(TEventListenerChain* iter) {
            return iter && iter->mListener;
        }

        TEventListenerChain* compressChain(TEventListenerChain* iter) {
            while (iter && nullptr == iter->mListener) {
                auto next = iter->mNext;
                delete iter;
                iter = next;
            }
            return iter;
        }

        TEventListenerChain* head() {
            mHead = compressChain(mHead);
            mCurrent = mHead;
            return mCurrent;
        }

        TEventListenerChain* next(TEventListenerChain* iter) {
            if (iter) {
                iter->mNext = compressChain(iter->mNext);
                mCurrent = iter->mNext;
            } else {
                mCurrent = nullptr;
            }
            return mCurrent;
        }

        TEventListenerChain* mHead;
        TEventListenerChain* mCurrent;
    };

private:
    Listener* mListener;
    int32_t mPriority;
    TEventListenerChain* mNext;
};

#define fire(listeners, exp)            \
    {                                   \
        auto iter = listeners.head();   \
        while (iter) {                  \
            auto listener = iter->getListener();\
            exp;                        \
            iter = listeners.next(iter);\
        }                               \
    }


class SG_LOCAL_API Projection {
public:

    Projection()
    {
    }

    void set(const Perspective& perspctive)
    {
        value.perspective = perspctive;
        type = SG_PERSPECTIVE;
    }

    void set(const Frustum& frustum)
    {
        value.frustum = frustum;
        type = SG_FRUSTUM;
    }

    void set(const Ortho& ortho)
    {
        value.ortho = ortho;
        type = SG_ORTHO;
    }

    bool isPerspective() const {
        return type == SG_PERSPECTIVE;
    }

    bool isFrustum() const {
        return type == SG_FRUSTUM;
    }

    bool isOrtho() const {
        return type == SG_ORTHO;
    }

    bool operator==(const Perspective& rhv) const {
        return
            isPerspective() &&
            value.perspective.fovy == rhv.fovy &&
            value.perspective.aspect == rhv.aspect &&
            value.perspective.znear == rhv.znear &&
            value.perspective.zfar == rhv.zfar;
    }

    bool operator!=(const Perspective& rhv) const {
        return !operator==(rhv);
    }

    bool operator==(const Frustum& rhv) const {
        return
            isPerspective() &&
            value.frustum.left == rhv.left &&
            value.frustum.right == rhv.right &&
            value.frustum.bottom == rhv.bottom &&
            value.frustum.top == rhv.top &&
            value.frustum.znear == rhv.znear &&
            value.frustum.zfar == rhv.zfar;
    }

    bool operator!=(const Frustum& rhv) const {
        return !operator==(rhv);
    }

    bool operator==(const Ortho& rhv) const {
        return
            isPerspective() &&
            value.ortho.left == rhv.left &&
            value.ortho.right == rhv.right &&
            value.ortho.bottom == rhv.bottom &&
            value.ortho.top == rhv.top &&
            value.ortho.znear == rhv.znear &&
            value.ortho.zfar == rhv.zfar;

    }

    bool operator!=(const Ortho& rhv) const {
        return !operator==(rhv);
    }

    ProjectionType type;
    union {
        Perspective perspective;
        Frustum frustum;
        Ortho ortho;
    } value;
};

}

#endif
