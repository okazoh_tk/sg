/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "AssimpModelAsset.h"
#include "sg/Context.h"
#include "sg/MemoryAllocator.h"
#include "OpenGL.h"
#include "GLSLDefaultShaders.h"
#include <assimp/postprocess.h>
#include <vector>
#include <functional>

namespace sg {

/*
 * http://assimp.sourceforge.net/lib_html/materials.html
 */

AssimpModelAsset::AssimpModelAsset(Model& owner)
    : IModelAsset(owner)
    , mRootNode(nullptr)
{
}

AssimpModelAsset:: ~AssimpModelAsset()
{
    if (mRootNode) {
        NodeDestroyer::destroyAfterRemove(*mRootNode);
    }

    for (auto model : mModels) {
        if (model) {
            delete model;
        }
    }

    for (auto mesh : mMeshes) {
        if (mesh) {
            delete mesh;
        }
    }

    for (auto material : mMaterials) {
        if (material) {
            delete material;
        }
    }
}

bool AssimpModelAsset::load(const char* filepath)
{
    Assimp::Importer importer;

    // read asset file as triangle mesh
    const aiScene* scene = importer.ReadFile(
        filepath,
//        aiProcessPreset_TargetRealtime_Fast);
        aiProcessPreset_TargetRealtime_Quality);

    if (scene == nullptr) {
        LOGE("%s", importer.GetErrorString());
        return false;
    }

    // setup materials
    if (scene->HasMaterials()) {
        for (uint32_t i = 0; i < scene->mNumMaterials; ++i) {
            mMaterials.push_back(new AssimpMaterial(scene->mMaterials[i]));
        }
    }

    // setup meshes
    if (scene->HasMeshes()) {
        for (uint32_t i = 0; i < scene->mNumMeshes; ++i) {

            auto mesh = new AssimpMesh(scene->mMeshes[i]);

            if (!mesh->isComplete()) {
                delete mesh;
                mesh = nullptr;
            }

            mMeshes.push_back(mesh);

            AssimpModel* model = nullptr;
            if (mesh) {
                model = new AssimpModel(
                    mesh,
                    mMaterials[scene->mMeshes[i]->mMaterialIndex]);
            }
            mModels.push_back(model);
        }
    }

    // create tree

    std::function<AssimpNode*(aiNode* n)> createNode = [&](aiNode* n) -> AssimpNode* {
        auto node = new AssimpNode(n->mNumMeshes);

        for (uint32_t i = 0; i < n->mNumMeshes; ++i) {
            node->set(i, mModels[i]);
        }

        if (n->mTransformation.IsIdentity()) {
            node->set(n->mTransformation);
        }

        for (uint32_t i = 0; i < n->mNumChildren; ++i) {
            node->addChild(*createNode(n->mChildren[i]));
        }

        return node;
    };

    mRootNode = createNode(scene->mRootNode);

    return true;
}

void AssimpModelAsset::update()
{
    if (mRootNode) {
        NodeUpdater::updateTree(*mRootNode);
    }
}

void AssimpModelAsset::draw(Context& gl)
{
    if (mRootNode) {
        NodeRenderer::drawTree(gl, *mRootNode);
    }
}


AssimpMesh::AssimpMesh(aiMesh* mesh)
    : mVertexBuffer(Buffer::STATIC_DRAW)
    , mIndexBuffer(Buffer::STATIC_DRAW)
{
    if (makeVertexBuffer(mesh)) {
        if (makeIndexBuffer(mesh)) {
            setVertices(&mVertexBuffer);
            setFaces(&mIndexBuffer);
            setPrimitiveType(SG_TRIANGLES);
        } else {
            mVertexBuffer.clear();
        }
    }
}

AssimpMesh::~AssimpMesh()
{
}

bool AssimpMesh::isComplete() const
{
    return 0 != mIndexBuffer.getIndicesNum();
}

bool AssimpMesh::makeVertexBuffer(aiMesh* mesh)
{
    const static char* color_names[AI_MAX_NUMBER_OF_COLOR_SETS] = {
        SG_COLOR0_NAME, SG_COLOR1_NAME, SG_COLOR2_NAME, SG_COLOR3_NAME,
        SG_COLOR4_NAME, SG_COLOR5_NAME, SG_COLOR6_NAME, SG_COLOR7_NAME
    };

    const static char* texcoord_names[AI_MAX_NUMBER_OF_TEXTURECOORDS] = {
        SG_TEXCOORD0_NAME, SG_TEXCOORD1_NAME, SG_TEXCOORD2_NAME, SG_TEXCOORD3_NAME,
        SG_TEXCOORD4_NAME, SG_TEXCOORD5_NAME, SG_TEXCOORD6_NAME, SG_TEXCOORD7_NAME,
    };

    // 1. make layout
    if (mesh->HasPositions()) {
        VertexAttrib attrib;
        attrib.type = &VertexBuffer::POSITION_TYPE;
        attrib.name = SG_POSITION_NAME;
        mLayout.push_back(attrib);
    }

    for (uint32_t c = 0; c < mesh->GetNumColorChannels(); ++c) {
        VertexAttrib attrib;
        attrib.type = &VertexBuffer::COLORF_TYPE;
        attrib.name = color_names[c];
        mLayout.push_back(attrib);
    }

    if (mesh->HasNormals()) {
        VertexAttrib attrib;
        attrib.type = &VertexBuffer::NORMAL_TYPE;
        attrib.name = SG_NORMAL_NAME;
        mLayout.push_back(attrib);
    }

    for (uint32_t t = 0; t < mesh->GetNumUVChannels(); ++t) {
        VertexAttrib attrib;
        attrib.type = &VertexBuffer::TEXCOORD_TYPE;
        attrib.name = texcoord_names[t];
        mLayout.push_back(attrib);
    }

    {
        VertexAttrib attrib;
        attrib.type = nullptr;
        attrib.name = nullptr;
        mLayout.push_back(attrib);
    }

    // 2. create vertex buffer
    auto num = mesh->mNumVertices;
    auto layout = mLayout.data();

    // set vertex layout and keep enough buffer
    if (!mVertexBuffer.reserve(layout, num)) {
        mLayout.clear();
        return false;
    }

    auto stride = VertexBuffer::computeVertexSize(layout);
    uint8_t* base_ptr = (uint8_t*)mVertexBuffer.getBuffer();

    if (mesh->HasPositions()) {
        Vector3* p = (Vector3*)base_ptr;
        for (uint32_t i = 0; i < num; ++i) {
            p->x = mesh->mVertices[i].x;
            p->y = mesh->mVertices[i].y;
            p->z = mesh->mVertices[i].z;
            p = (Vector3*)((uint8_t*)p + stride);
        }

        base_ptr += VertexBuffer::computeAttribSize(layout);
    }

    for (uint32_t c = 0; c < mesh->GetNumColorChannels(); ++c) {
        Colorf* p = (Colorf*)base_ptr;
        for (uint32_t i = 0; i < num; ++i) {
            p->r = mesh->mColors[c][i].r;
            p->g = mesh->mColors[c][i].g;
            p->b = mesh->mColors[c][i].b;
            p->a = mesh->mColors[c][i].a;
            p = (Colorf*)((uint8_t*)p + stride);
        }

        base_ptr += VertexBuffer::computeAttribSize(layout);
    }

    if (mesh->HasNormals()) {
        Vector3* p = (Vector3*)base_ptr;
        for (uint32_t i = 0; i < num; ++i) {
            p->x = mesh->mNormals[i].x;
            p->y = mesh->mNormals[i].y;
            p->z = mesh->mNormals[i].z;
            p = (Vector3*)((uint8_t*)p + stride);
        }

        base_ptr += VertexBuffer::computeAttribSize(layout);
    }

    for (uint32_t t = 0; t < mesh->GetNumUVChannels(); ++t) {
        Vector2* p = (Vector2*)base_ptr;
        for (uint32_t i = 0; i < num; ++i) {
            p->x = mesh->mTextureCoords[t][i].x;
            p->y = mesh->mTextureCoords[t][i].y;
            p = (Vector2*)((uint8_t*)p + stride);
        }

        base_ptr += VertexBuffer::computeAttribSize(layout);
    }

    return true;
}

bool AssimpMesh::makeIndexBuffer(aiMesh* mesh)
{
    // copy indices as short if possible
    bool is_short = (mVertexBuffer.getVertexNum() <= 0xffff);

    bool success = mIndexBuffer.reserve(mesh->mNumFaces * 3, is_short);
    if (success) {
        for (uint32_t i = 0; i < mesh->mNumFaces; ++i) {
            mIndexBuffer.copy(i * 3, mesh->mFaces[i].mIndices, mesh->mFaces[i].mNumIndices);
        }
    }

    return success;
}

AssimpMaterial::AssimpMaterial(aiMaterial* material)
    : mProgram(nullptr)
    , mCulling(true)
{
    make(material);
}

AssimpMaterial::~AssimpMaterial()
{
    delete mProgram;
}

#define setColorProperty(matkey, name) \
    [&]{ \
        aiColor3D color; \
        if (aiReturn_SUCCESS == material->Get(matkey, color)) { \
            this->set(name, Vector3Property({color.r, color.g, color.b}));\
        }\
    }()

bool AssimpMaterial::make(aiMaterial* material)
{
    aiColor3D color;
    if (aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
        this->set(SG_UNIFORM_COLOR_DIFFUSE, Vector3Property({ color.r, color.g, color.b }));
    }
    if (aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
        //color.r = 0.5;
        //color.g = 0.0;
        //color.b = 0.5;
        this->set(SG_UNIFORM_COLOR_SPECULAR, Vector3Property({ color.r, color.g, color.b }));
    }
    if (aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_AMBIENT, color)) {
        //color.r = 1;
        //color.g = 1;
        //color.b = 1;
        this->set(SG_UNIFORM_COLOR_AMBIENT, Vector3Property({ color.r, color.g, color.b }));
    }
    if (aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_EMISSIVE, color)) {
        this->set(SG_UNIFORM_COLOR_EMISSIVE, Vector3Property({ color.r, color.g, color.b }));
    }
    //    setColorProperty(AI_MATKEY_COLOR_TRANSPARENT, "transparent");

    float opacity;
    if (aiReturn_SUCCESS == material->Get(AI_MATKEY_OPACITY, opacity)) {
        this->set(SG_UNIFORM_OPACITY, FloatProperty(opacity));
    }
    float shininess;
    if (aiReturn_SUCCESS == material->Get(AI_MATKEY_SHININESS, shininess)) {
        //shininess = 1.0f;
        this->set(SG_UNIFORM_SHININESS, FloatProperty(shininess));
    }

    this->set(SG_UNIFORM_GLOBAL_AMBIENT, Vector3Property({ 1, 1, 1 }));

    {
        int twosided;
        if (aiReturn_SUCCESS == material->Get(AI_MATKEY_TWOSIDED, twosided)) {
            mCulling = (0 == twosided);
        }
    }


    if (mProgram) {
        delete mProgram;
    }
    
    return true;
}

const char* LIGHT_HEADER_0 = "\n#define MAX_LIGHTS 0\n";
const char* LIGHT_HEADER_1 = "\n#define MAX_LIGHTS 1\n";
const char* LIGHT_HEADER_2 = "\n#define MAX_LIGHTS 2\n";
const char* LIGHT_HEADER_3 = "\n#define MAX_LIGHTS 3\n";

bool AssimpMaterial::update(Context& gl)
{
    if (!mProgram) {
        // setup program
        mProgram = new Program();
        uint32_t max_lights = gl.getLightNum();
        const char* light_shader_header = max_lights ? LIGHT_HEADER_1 : LIGHT_HEADER_0;

#if 1
        const char* vs[] = { Shader::HEADER_PRECISION, light_shader_header, default_vert };
        const char* fs[] = { Shader::HEADER_PRECISION, light_shader_header, default_frag };
        mProgram->setShader(vs, COUNT_OF(vs), fs, COUNT_OF(fs));
#else
        mProgram->setShaderFromFile(
#if 1
            "default2.vert",
            "default2.frag");
#else
            "openFrameworks.vert",
            "openFrameworks.frag");
#endif
#endif
        setProgram(mProgram);
    }

    Matrix4 mat;
    auto& stack = gl.getCurrentMatrix();

    stack.getModelMatrix(mat);
    set(SG_UNIFORM_M_MATRIX, Matrix4Property(mat));

    mat.inverse();
    mat.transpose();
    set(SG_UNIFORM_NORMAL_MATRIX, Matrix4Property(mat));

    set(SG_UNIFORM_CAMERA_POSITION, Vector3Property(stack.getCameraPosition()));

    gl.pushSetting();

    gl.culling.enable(mCulling);

    return true;
}

void AssimpMaterial::restore(Context& gl)
{
    gl.popSetting();
}



AssimpModel::AssimpModel(AssimpMesh* mesh, AssimpMaterial* material)
    : mMesh(mesh)
    , mMaterial(material)
{
}

AssimpModel::~AssimpModel()
{
}

void AssimpModel::update()
{
}

void AssimpModel::draw(Context& gl)
{
    gl.draw(*mMesh, *mMaterial);
}


AssimpNode::AssimpNode(uint32_t num_models)
    : mNum(num_models)
    , mNeedTransform(false)
{
    if (mNum > 1) {
        mList.models = (AssimpModel**)sysAlloc(mNum * sizeof(AssimpModel*));
    } else {
        mList.model = nullptr;
    }
}

AssimpNode::~AssimpNode()
{
    if (mNum > 1) {
        sysFree(mList.models);
    }
}

void AssimpNode::set(uint32_t index, AssimpModel* model)
{
    if (mNum) {
        if (mNum > 1) {
            mList.models[index] = model;
        } else {
            mList.model = model;
        }
    }
}

void AssimpNode::set(const aiMatrix4x4& mat)
{
    memcpy(mTransformation.m, &mat.a1, sizeof(mTransformation));
    mNeedTransform = true;
}

bool AssimpNode::onUpdate()
{
    if (mNum) {
        if (mNum > 1) {
            for (uint32_t i = 0; i < mNum; ++i) {
                mList.models[i]->update();
            }
        } else {
            mList.model->update();
        }
    }
    return true;
}

bool AssimpNode::onDraw(Context& gl)
{
    if (mNeedTransform) {
        auto& matrixStack = gl.getCurrentMatrix();

        // save matrix stack
        matrixStack.push();
        matrixStack.multiply(mTransformation);
    }

    if (mNum) {
        if (mNum > 1) {
            for (uint32_t i = 0; i < mNum; ++i) {
                mList.models[i]->draw(gl);
            }
        } else {
            mList.model->draw(gl);
        }
    }

    return true;
}

void AssimpNode::onDrawPost(Context& gl)
{
    if (mNeedTransform) {
        // restore matrix stack
        gl.getCurrentMatrix().pop();
    }
}

}
