/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Light.h"
#include "sg/Program.h"
#include "OpenGL.h"
#include <string>

namespace sg {

Light::Light(Type type)
    : mType(type)
    , mDiffuse({ 0, 0, 0, 1 })
    , mAmbient({ 0, 0, 0, 1 })
    , mSpecular({ 0, 0, 0, 1 })
{
}

Light::~Light()
{
}

void Light::setDiffuseColor(const Colorf& color)
{
    mDiffuse = color;
    mDiffuse.a = 1.0f;
}

Colorf Light::getDiffuseColor() const
{
    return mDiffuse;
}

void Light::setAmbientColor(const Colorf& color)
{
    mAmbient = color;
    mAmbient.a = 1.0f;
}

Colorf Light::getAmbientColor() const
{
    return mAmbient;
}

void Light::setSpecularColor(const Colorf& color)
{
    mSpecular = color;
    mSpecular.a = 1.0f;
}

Colorf Light::getSpecularColor() const
{
    return mSpecular;
}

void Light::copyUniforms(Program& program, uint32_t index)
{
    auto name = program.getName();
    std::string prefix = "gLights[" + std::to_string(index) + "]";
    ProgramData data = {
        name,
        prefix.c_str()
    };

    copy(data, "type", (int32_t)mType);
    copy(data, "diffuse", Vector3{ mDiffuse.r, mDiffuse.g, mDiffuse.b });
    copy(data, "ambient", Vector3{ mAmbient.r, mAmbient.g, mAmbient.b });
    copy(data, "specular", Vector3{ mSpecular.r, mSpecular.g, mSpecular.b });

    onCopyValues(data);

    (void)prefix;
}

void Light::copy(const ProgramData& program, const char* name, int32_t value)
{
    auto loc = glGetUniformLocation(
        program.name,
        (std::string(program.prefix) + "." + name).c_str());
    if (loc < 0) {
        return;
    }
    invokeGL(glUniform1iv(loc, 1, &value));
}

void Light::copy(const ProgramData& program, const char* name, float value)
{
    auto loc = glGetUniformLocation(
        program.name,
        (std::string(program.prefix) + "." + name).c_str());
    if (loc < 0) {
        return;
    }
    invokeGL(glUniform1fv(loc, 1, &value));
}

void Light::copy(const ProgramData& program, const char* name, const Colorf& color)
{
    auto loc = glGetUniformLocation(
        program.name,
        (std::string(program.prefix) + "." + name).c_str());
    if (loc < 0) {
        return;
    }
    invokeGL(glUniform4fv(loc, 1, (const GLfloat*)&color));
}

void Light::copy(const ProgramData& program, const char* name, const Vector3& vec)
{
    auto loc = glGetUniformLocation(
        program.name,
        (std::string(program.prefix) + "." + name).c_str());
    if (loc < 0) {
        return;
    }
    invokeGL(glUniform3fv(loc, 1, (const GLfloat*)&vec));
}


Light::WeakRef::WeakRef(Light* light)
    : mLight(light)
{
    if (mLight) {
        mLight->addObserver(this);
    }
}

Light::WeakRef::WeakRef(const WeakRef& rhv)
    : WeakRef(rhv.mLight)
{
}

Light::WeakRef::~WeakRef()
{
    if (mLight) {
        mLight->removeObserver(this);
        mLight = nullptr;
    }
}

Light* Light::WeakRef::operator->()
{
    return mLight;
}

void Light::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mLight);
}

void Light::WeakRef::operator=(Light* light)
{
    if (mLight == light) {
        return;
    }

    if (mLight) {
        mLight->removeObserver(this);
    }

    mLight = light;

    if (mLight) {
        mLight->addObserver(this);
    }
}

Light::WeakRef::operator Light*() const
{
    return mLight;
}

void Light::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mLight = nullptr;
}


SpotLight::SpotLight()
    : Light(SPOT)
    , mCone({ 10, 20 })
    , mAttenuation({ 1, 0, 0 })
    , mFallOffExponent(1.0f)
{
}

SpotLight::~SpotLight()
{
}

void SpotLight::setDirection(const Vector3& direction)
{
    mDirection = direction;
}

Vector3 SpotLight::getDirection() const
{
    return mDirection;
}

void SpotLight::setPosition(const Position3& position)
{
    mPosition = position;
}

Position3 SpotLight::getPosition() const
{
    return mPosition;
}

void SpotLight::setAttenuation(const Attenuation& attenuation)
{
    mAttenuation = attenuation;
}

Light::Attenuation SpotLight::getAttenuation() const
{
    return mAttenuation;
}

void SpotLight::setCone(const Cone& cone)
{
    mCone = cone;
}

Light::Cone SpotLight::getCone() const
{
    return mCone;
}

void SpotLight::setFallOffExponent(float exponent)
{
    mFallOffExponent = exponent;
}

float SpotLight::getFallOffExponent() const
{
    return mFallOffExponent;
}


#ifndef PI
#define PI 3.14159265f
#endif

void SpotLight::onCopyValues(const ProgramData& program)
{
    copy(program, "position", mPosition);
    copy(program, "direction", mDirection);
    copy(program, "attenConstant", mAttenuation.constant);
    copy(program, "attenLinear", mAttenuation.linear);
    copy(program, "attenQuadratic", mAttenuation.quadratic);
    copy(program, "innerCos", cos(mCone.inner_angle * PI / 90)); // (angle * PI / 180) /2
    copy(program, "outerCos", cos(mCone.outer_angle * PI / 90));
    copy(program, "fallOffExp", mFallOffExponent);
}


PointLight::PointLight()
    : Light(POINT)
    , mAttenuation({ 1, 0, 0 })
{
}

PointLight::~PointLight()
{
}

void PointLight::setPosition(const Position3& position)
{
    mPosition = position;
}

Position3 PointLight::getPosition() const
{
    return mPosition;
}

void PointLight::setAttenuation(const Attenuation& attenuation)
{
    mAttenuation = attenuation;
}

Light::Attenuation PointLight::getAttenuation() const
{
    return mAttenuation;
}

void PointLight::onCopyValues(const ProgramData& program)
{
    copy(program, "position", mPosition);
    copy(program, "attenConstant", mAttenuation.constant);
    copy(program, "attenLinear", mAttenuation.linear);
    copy(program, "attenQuadratic", mAttenuation.quadratic);
}


DirectionalLight::DirectionalLight()
    : Light(DIRECTIONAL)
{
}

DirectionalLight::~DirectionalLight()
{
}

void DirectionalLight::setDirection(const Vector3& direction)
{
    mDirection = direction;
}

Vector3 DirectionalLight::getDirection() const
{
    return mDirection;
}

void DirectionalLight::onCopyValues(const ProgramData& program)
{
    copy(program, "direction", mDirection);
}

}