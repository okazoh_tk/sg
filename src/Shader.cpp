/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Shader.h"
#include "OpenGL.h"

namespace sg {

#if defined SG_OPENGL_ES
const char* Shader::HEADER_PRECISION_HIGH = "precision highp float;\n";
const char* Shader::HEADER_PRECISION_MEDIAM = "precision mediump float;\n";
const char* Shader::HEADER_PRECISION_LOW = "precision lowp float;\n";
#else
const char* Shader::HEADER_PRECISION_HIGH = "";
const char* Shader::HEADER_PRECISION_MEDIAM = "";
const char* Shader::HEADER_PRECISION_LOW = "";
#endif
const char* Shader::HEADER_PRECISION = HEADER_PRECISION_MEDIAM;

Shader::Shader(
    ShaderType type,
    const char* source)
    : Shader(type, &source, source ? 1 : 0)
{
}

Shader::Shader(
    ShaderType type,
    const char* sources[],
    uint32_t num)
    : mName(0)
    , mType(type)
    , mSource(nullptr)
{
    if (!sources || num == 0) {
        return;
    }

    mSource = getDefaultMemoryAllocator()->concat(sources, num);
}

Shader::Shader(ShaderType type, const FilePath& filepath)
    : mName(0)
    , mType(type)
    , mSource(nullptr)
{
    mSource = File::read(filepath, getDefaultMemoryAllocator());
}

Shader::~Shader()
{
    if (mSource) {
        sysFree((void*)mSource);
        mSource = nullptr;
    }
    destroy(mName);
    mName = 0;
}

void Shader::setAllocatedSource(const char* source)
{
    if (mSource) {
        sysFree((void*)mSource);
    }
    mSource = source;
}

uint32_t Shader::getName() const
{
    return mName;
}

bool Shader::prepare()
{
    if (mName == 0) {
        mName = compile(mType, mSource, (uint32_t)strlen(mSource));
    }

    return mName != 0;
}

uint32_t Shader::compile(ShaderType type, const char* source, uint32_t source_len)
{
    if (source == 0 || source_len <= 0) {
        return 0;
    }

    auto name = glCreateShader(type == VERTEX_SHADER ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER);

    if (0 == name) {
        return 0;
    }

    GLint src_len = (GLint)source_len;
    glShaderSource(name, 1, &source, &src_len);

    glCompileShader(name);

    // check compilation status.
    GLint is_ready;
    glGetShaderiv(name, GL_COMPILE_STATUS, &is_ready);
    if (is_ready == GL_FALSE) {
        GLint log_len;
        glGetShaderiv(name, GL_INFO_LOG_LENGTH, &log_len);
        log_len += 1;
        GLchar* log = (GLchar*)sysAlloc(log_len);
        GLsizei real_len;
        glGetShaderInfoLog(name, log_len, &real_len, log);
        log[real_len] = '\0';
        LOGE("Shader compilation error: %s", log);
        sysFree(log);

        destroy(name);
        name = 0;
    }
    
    return name;
}

void Shader::destroy(uint32_t name)
{
    if (name != 0) {
        glDeleteShader(name);
    }
}


VertexShader::VertexShader(const char* source)
    : Shader(VERTEX_SHADER, source)
{
}

VertexShader::VertexShader(const char* sources[], uint32_t num)
    : Shader(VERTEX_SHADER, sources, num)
{
}

VertexShader::VertexShader(const FilePath& filepath)
    : Shader(VERTEX_SHADER, filepath)
{
}


FragmentShader::FragmentShader(const char* source)
    : Shader(FRAGMENT_SHADER, source)
{
}

FragmentShader::FragmentShader(const char* sources[], uint32_t num)
    : Shader(FRAGMENT_SHADER, sources, num)
{
}

FragmentShader::FragmentShader(const FilePath& filepath)
    : Shader(FRAGMENT_SHADER, filepath)
{
}

}
