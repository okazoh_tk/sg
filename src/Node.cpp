/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Node.h"
#include "sg/MemoryAllocator.h"
#include <vector>

namespace sg {

Node::Node()
    : mParent(nullptr)
    , mPrevSibling(nullptr)
    , mNextSibling(nullptr)
    , mFirstChild(nullptr)
    , mLastChild(nullptr)
{
}

Node::~Node()
{
}

Node* Node::getParent() const
{
    return mParent;
}

Node* Node::getPrevSibling() const
{
    return mPrevSibling;
}

Node* Node::getNextSibling() const
{
    return mNextSibling;
}

Node* Node::getFirstChild() const
{
    return mFirstChild;
}

Node* Node::getLastChild() const
{
    return mLastChild;
}

void Node::addChild(Node& child)
{
    if (child.mParent) {
        child.mParent->removeChild(child);
    }

    if (mLastChild) {
        mLastChild->mNextSibling = &child;
        child.mPrevSibling = mLastChild;
    } else {
        mFirstChild = &child;
    }
    child.mParent = this;
    mLastChild = &child;
}

void Node::removeChild(Node& child)
{
    if (child.mParent != this) {
        return;
    }

    if (child.mPrevSibling) {
        child.mPrevSibling->mNextSibling = child.mNextSibling;
    } else {
        mFirstChild = child.mNextSibling;
    }
    if (child.mNextSibling) {
        child.mNextSibling->mPrevSibling = child.mPrevSibling;
    } else {
        mLastChild = child.mPrevSibling;
    }
    child.mParent = child.mNextSibling = child.mPrevSibling = nullptr;
}

bool Node::update()
{
    return onUpdate();
}

void Node::updatePost()
{
    onUpdatePost();
}

bool Node::draw(Context& gl)
{
    return onDraw(gl);
}

void Node::drawPost(Context& gl)
{
    onDrawPost(gl);
}


bool Node::onUpdate()
{
    return true;
}

void Node::onUpdatePost()
{
}

bool Node::onDraw(Context& /*gl*/)
{
    return true;
}

void Node::onDrawPost(Context& /*gl*/)
{
}

NodeVisitor::NodeVisitor()
{
}

NodeVisitor::~NodeVisitor()
{
}

void NodeVisitor::traverse(Node& root)
{
    Node* iter = &root;
    Node* next_iter;
    Node* parent;
    bool traverse_children;

    while (iter) {

        /* If traverse_children is true, the visitor traverses iter's children recursively.
         * Otherwise, the visitor ignores "iter's children" and "iter's visitPost process".
         */
        traverse_children = visit(*iter);

        if (traverse_children) {
            next_iter = iter->getFirstChild();
        } else {
            if (iter == &root) {
                break;
            }
            next_iter = iter->getNextSibling();
        }

        if (!next_iter) {

            // Call visitPost in this section.

            if (traverse_children) {
                parent = iter; // call visitPost(iter)
            } else {
                // Don't call visitPost(iter) due to visit() result,
                // so starts from iter's parent.
                parent = iter->getParent();
            }

            while (parent && !next_iter) {

                auto temp_next = parent->getNextSibling();
                auto temp_parent = parent->getParent();

                visitPost(*parent);

                if (parent == &root) {
                    break;
                }

                next_iter = temp_next;
                if (!next_iter) {
                    parent = temp_parent;
                }
            }
        }

        iter = next_iter;
    }
}


NodeUpdater::NodeUpdater()
{
}

NodeUpdater::~NodeUpdater()
{
}

void NodeUpdater::updateTree(Node& root)
{
    NodeUpdater updater;
    updater.traverse(root);
}

bool NodeUpdater::visit(Node& node)
{
    return node.update();
}

void NodeUpdater::visitPost(Node& node)
{
    node.updatePost();
}


NodeRenderer::NodeRenderer(Context& gl)
    : mContext(&gl)
{
}

NodeRenderer::~NodeRenderer()
{
}

void NodeRenderer::drawTree(Context& gl, Node& root)
{
    NodeRenderer renderer(gl);
    renderer.traverse(root);
}

bool NodeRenderer::visit(Node& node)
{
    return node.draw(*mContext);
}

void NodeRenderer::visitPost(Node& node)
{
    node.drawPost(*mContext);
}


NodeDestroyer::NodeDestroyer()
{
}

NodeDestroyer::~NodeDestroyer()
{
}

void NodeDestroyer::destroyAfterRemove(Node& root)
{
    NodeDestroyer destroyer;

    auto parent = root.getParent();
    if (parent) {
        parent->removeChild(root);
    }

    destroyer.traverse(root);
}

bool NodeDestroyer::visit(Node& /*node*/)
{
    return true;
}

void NodeDestroyer::visitPost(Node& node)
{
    delete &node;
}

}
