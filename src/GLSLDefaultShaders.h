/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_GLSL_DEFAULT_SHADERS_H_INCLUDED_
#define SG_GLSL_DEFAULT_SHADERS_H_INCLUDED_

#include "OpenGL.h"

#ifndef STRINGIFY
#if defined(SG_OPENGL_ES)
#define STRINGIFY(exp) "precision mediump float;\n" #exp
#else
#define STRINGIFY(exp) #exp
#endif
#endif

#ifndef STRINGIFY_MIN
#define STRINGIFY_MIN(exp) #exp
#endif


static const char* gradient_fill_vs = STRINGIFY(
attribute vec3 inPosition; \n
attribute vec4 inColor; \n
uniform mat4 gMVPMatrix; \n
varying vec4 outColor; \n
\n
void main(void)\n
{ \n
    outColor = inColor; \n
    gl_Position = gMVPMatrix * vec4(inPosition, 1); \n
}\n
);

static const char* gradient_fill_fs = STRINGIFY(
varying vec4 outColor; \n
\n
void main(void)\n
{ \n
    gl_FragColor = outColor; \n
}\n
);

static const char* solid_fill_vs = STRINGIFY(
attribute vec3 inPosition;\n
uniform mat4 gMVPMatrix; \n
\n
void main(void)\n
{\n
    gl_Position = gMVPMatrix * vec4(inPosition, 1); \n
}\n
);

static const char* solid_fill_fs = STRINGIFY(
uniform vec4 gColor; \n
\n
void main(void)\n
{\n
    gl_FragColor = gColor;\n
}\n
);


static const char* mono_texture_vs = STRINGIFY(
attribute vec3 inPosition;\n
attribute vec2 inTexcoord;\n
uniform mat4 gMVPMatrix; \n
varying vec2 outUV;\n
\n
void main(void)\n
{\n
    gl_Position = gMVPMatrix * vec4(inPosition, 1); \n
    outUV = inTexcoord; \n
}\n
);

static const char* mono_texture_fs = STRINGIFY(
varying vec2 outUV;\n
uniform sampler2D gTexture;\n
uniform vec4 gColor;\n
\n
void main(void)\n
{\n
    gl_FragColor = vec4(gColor.xyz, gColor.w * texture2D(gTexture, outUV).w); \n
}\n
);


static const char* texture_diffuse_vs = STRINGIFY(
attribute vec3 inPosition;\n
attribute vec2 inTexcoord;\n
uniform mat4 gMVPMatrix; \n
varying vec2 outUV;\n
\n
void main(void)\n
{\n
    gl_Position = gMVPMatrix * vec4(inPosition, 1); \n
    outUV = inTexcoord; \n
}\n
);

static const char* texture_diffuse_fs = STRINGIFY(
varying vec2 outUV; \n
uniform sampler2D gTexture;\n
uniform vec4 gColor;\n
\n
void main(void)\n
{\n
    gl_FragColor = texture2D(gTexture, outUV) * gColor; \n
}\n
);


static const char* default_vert = STRINGIFY_MIN(

uniform mat4 gMVPMatrix;
attribute vec3 inPosition;

\n#if MAX_LIGHTS > 0\n

uniform mat4 gMMatrix;
uniform mat4 gNormalMatrix;
uniform vec3 gCameraPos;

attribute vec3 inNormal;

varying vec3 vViewVec;
varying vec3 vNormal;
varying vec3 vPosition;

\n#endif\n

void main()
{
    gl_Position = gMVPMatrix * vec4(inPosition, 1.0);

\n#if MAX_LIGHTS > 0\n
    vPosition = (gMMatrix * vec4(inPosition, 1.0)).xyz;
    vViewVec = gCameraPos - vPosition;
    vNormal = (gNormalMatrix * vec4(inNormal, 0)).xyz;
\n#endif\n
}

);

static const char* default_frag = STRINGIFY_MIN(

// material
uniform vec3 gDiffuse;
uniform vec3 gSpecular;
uniform vec3 gAmbient;
uniform vec3 gEmissive;
uniform vec3 gGlobalAmbient;
uniform float gOpacity;

\n#if MAX_LIGHTS > 0\n

varying vec3 vViewVec;
varying vec3 vNormal;
varying vec3 vPosition;

uniform float gShininess;

struct Light {
    int type;
    vec3 diffuse;
    vec3 ambient;
    vec3 specular;
    vec3 position;
    vec3 direction;
    float innerCos;
    float outerCos;
    float fallOffExp;
    float attenConstant;
    float attenLinear;
    float attenQuadratic;
};

uniform Light gLights[MAX_LIGHTS];

void directionalLight(in Light light, in vec3 normalVec, in vec3 viewVec, inout vec3 diffuse, inout vec3 ambient, inout vec3 specular)
{
    vec3 lightVec = normalize(-light.direction);

    // Diffuse phase
    diffuse += light.diffuse * max(0.0, dot(normalVec, lightVec));

    // Ambient phase
    ambient += light.ambient;

    // Specular phase
    vec3 halfVec = normalize(lightVec + viewVec);
    float sfactor = dot(normalVec, halfVec);
    if (sfactor > 0.0) {
        specular += light.specular * pow(sfactor, gShininess);
    }
}

void pointLight(in Light light, in vec3 normalVec, in vec3 viewVec, inout vec3 diffuse, inout vec3 ambient, inout vec3 specular)
{
    vec3 l = light.position - vPosition; // light vector
    float d = length(l); // light vector length
    vec3 lightVec = normalize(l); // normalized light vector
    float attenuation = 1.0 / (light.attenConstant + light.attenLinear * d + light.attenQuadratic * d * d);

    // Diffuse phase
    diffuse += light.diffuse * max(0.0, dot(normalVec, lightVec)) * attenuation;

    // Ambient phase
    ambient += light.ambient * attenuation;

    // Specular phase
    vec3 halfVec = normalize(lightVec + viewVec);
    float sfactor = dot(normalVec, halfVec);
    if (sfactor > 0.0) {
        specular += light.specular * pow(sfactor, gShininess) * attenuation;
    }
}

void spotLight(in Light light, in vec3 normalVec, in vec3 viewVec, inout vec3 diffuse, inout vec3 ambient, inout vec3 specular)
{
    vec3 l = light.position - vPosition;
    float d = length(l);
    vec3 lightVec = normalize(l);
    float attenuation = 1.0 / (light.attenConstant + light.attenLinear * d + light.attenQuadratic * d * d);
    float cosine = dot(-lightVec, normalize(light.direction));

    if (cosine > light.outerCos) {
        if (cosine < light.innerCos) {
            float diff = light.innerCos - light.outerCos;
            //attenuation *= (cosine - light.outerCos) / diff; // same as DirectX
            attenuation *= pow((cosine - light.outerCos) / diff, light.fallOffExp);
        }

        // Diffuse phase
        diffuse += light.diffuse * max(0.0, dot(normalVec, lightVec)) * attenuation;

        // Specular phase
        vec3 halfVec = normalize(lightVec + viewVec);
        float sfactor = dot(normalVec, halfVec);
        if (sfactor > 0.0) {
            specular += light.specular * pow(sfactor, gShininess) * attenuation;
        }
    }

    // Ambient phase
    ambient += light.ambient * attenuation;
}

vec3 lighting()
{
    vec3 normal = normalize(vNormal);
    vec3 view = normalize(vViewVec);
    vec3 diffuse = vec3(0);
    vec3 ambient = gGlobalAmbient;
    vec3 specular = vec3(0);

    for (int i = 0; i < MAX_LIGHTS; ++i) {
        if (gLights[i].type == 1) {
            // Directional Light
            directionalLight(gLights[i], normal, view, diffuse, ambient, specular);
        }
        else if (gLights[i].type == 2) {
            // Spot Light
            spotLight(gLights[i], normal, view, diffuse, ambient, specular);
        }
        else if (gLights[i].type == 3) {
            // Point Light
            pointLight(gLights[i], normal, view, diffuse, ambient, specular);
        }

    }
    return gDiffuse * diffuse + gSpecular * specular + gAmbient * ambient + gEmissive;
}
\n#else\n
vec3 lighting()
{
    // no lighting
    return gDiffuse + gSpecular + gGlobalAmbient * gAmbient + gEmissive;
}
\n#endif\n

void main()
{
    vec4 finalColor = vec4(lighting(), gOpacity);
    gl_FragColor = clamp(finalColor, vec4(0), vec4(1));
}

);

#undef STRINGIFY
#undef STRINGIFY_MIN

#endif
