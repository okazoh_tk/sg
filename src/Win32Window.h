/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_WIN32_WINDOW_H_INCLUDED_
#define SG_WIN32_WINDOW_H_INCLUDED_

#include <Windows.h>
#include "sg/Window.h"
#include "OpenGL.h"
#include "SimpleDevice.h"

namespace sg {

#if ENABLE_WIN32WINDOW != 0

#if defined(SG_OPENGL_ES)
class EGLObject;
#endif

class SG_LOCAL_API Win32Window : public Window {
public:

    Win32Window(const Size& window_size);
    virtual ~Win32Window();

    HWND initWindow();
    virtual bool initContext() = 0;
    virtual void destroyContext() = 0;
    void destroyWindow();
    HWND getWindowHandle();
    IDevice* getDevice();

    virtual bool handleEvents(EventDelegate& eventDelegate);
    virtual void exitLoop();

    virtual void swap();

    virtual LRESULT onMessage(UINT msg, WPARAM wparam, LPARAM lparam);

    void scroll(int32_t x, int32_t y);

    static uint32_t convertKey(WPARAM keycode, bool& is_button);

private:
    static LRESULT CALLBACK windowProc(HWND wnd, UINT msg, WPARAM wparm, LPARAM lparam);

    void fireEvent(bool pressed, WPARAM wparam);
    void checkSideKeyEvent(bool pressed, const WPARAM wkeycode[2], const int32_t keycode[2]);

private:
    ATOM mAtom;
    HWND mWindowHandle;
    EventDelegate* mEventDelegate;
    SimpleDevice mDevice;
};


#if defined(SG_OPENGL_ES)

class SG_LOCAL_API EGLWindow : public Win32Window {
public:
    EGLWindow(const Size& window_size);
    virtual ~EGLWindow();

    virtual bool initContext();
    virtual void destroyContext();
    virtual void swap();

private:
    EGLObject* mEGL;
};

#else

class SG_LOCAL_API WGLWindow : public Win32Window {
public:
    WGLWindow(const Size& window_size);
    virtual ~WGLWindow();

    virtual bool initContext();
    virtual void destroyContext();

private:
    HGLRC mGLContext;
};

#endif

#endif

}

#endif
