/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define LOG_TAG "VertexBuffer"
#include "sg/VertexBuffer.h"
#include "OpenGL.h"
#include "sg/MemoryAllocator.h"

namespace sg {

const VertexType VertexBuffer::POSITION_TYPE = {
    3, VertexType::FLOAT, false
};

const VertexType VertexBuffer::TEXCOORD_TYPE = {
    2, VertexType::FLOAT, false
};

const VertexType VertexBuffer::TEXCOORD_CUBE_TYPE = {
    3, VertexType::FLOAT, false
};

const VertexType VertexBuffer::COLORF_TYPE = {
    4, VertexType::FLOAT, false
};

const VertexType VertexBuffer::NORMAL_TYPE = {
    3, VertexType::FLOAT, false
};


static const VertexAttrib _P_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PC_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_COLOR_NAME, &VertexBuffer::COLORF_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PT_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_TEXCOORD_NAME, &VertexBuffer::TEXCOORD_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PN_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_NORMAL_NAME, &VertexBuffer::NORMAL_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PCT_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_COLOR_NAME, &VertexBuffer::COLORF_TYPE },
    { SG_TEXCOORD_NAME, &VertexBuffer::TEXCOORD_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PCN_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_COLOR_NAME, &VertexBuffer::COLORF_TYPE },
    { SG_NORMAL_NAME, &VertexBuffer::NORMAL_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PTN_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_TEXCOORD_NAME, &VertexBuffer::TEXCOORD_TYPE },
    { SG_NORMAL_NAME, &VertexBuffer::NORMAL_TYPE },
    { 0, 0 }
};

static const VertexAttrib _PCTN_LAYOUT[] = {
    { SG_POSITION_NAME, &VertexBuffer::POSITION_TYPE },
    { SG_COLOR_NAME, &VertexBuffer::COLORF_TYPE },
    { SG_TEXCOORD_NAME, &VertexBuffer::TEXCOORD_TYPE },
    { SG_NORMAL_NAME, &VertexBuffer::NORMAL_TYPE },
    { 0, 0 }
};

const VertexAttrib* PosVertex::LAYOUT = _P_LAYOUT;
const VertexAttrib* PosColVertex::LAYOUT = _PC_LAYOUT;
const VertexAttrib* PosTexVertex::LAYOUT = _PT_LAYOUT;
const VertexAttrib* PosNormVertex::LAYOUT = _PN_LAYOUT;
const VertexAttrib* PosColTexVertex::LAYOUT = _PCT_LAYOUT;
const VertexAttrib* PosColNormVertex::LAYOUT = _PCN_LAYOUT;
const VertexAttrib* PosTexNormVertex::LAYOUT = _PTN_LAYOUT;
const VertexAttrib* PosColTexNormVertex::LAYOUT = _PCTN_LAYOUT;


VertexBuffer::VertexBuffer(Usage usage)
    : Buffer(ARRAY_BUFFER, usage)
    , mVertexNum(0)
    , mVertexSize(0)
    , mBuffer(nullptr)
    , mVertexLayout(nullptr)
    , mReleaseDelegate(nullptr)
{
}

VertexBuffer::~VertexBuffer()
{
    clearObserver();
    clear();
}

const VertexAttrib* VertexBuffer::getVertexLayout() const
{
    return mVertexLayout;
}

uint32_t VertexBuffer::getVertexNum() const
{
    return mVertexNum;
}

uint32_t VertexBuffer::getVertexSize() const
{
    return mVertexSize;
}

void* VertexBuffer::getBuffer()
{
    return mBuffer;
}

const void* VertexBuffer::getBuffer() const
{
    return mBuffer;
}

uint32_t VertexBuffer::getBufferSize() const
{
    return mBufferSize;
}

bool VertexBuffer::reserve(const VertexAttrib* layout, uint32_t vertex_num)
{
    if (!layout || vertex_num == 0) {
        return false;
    }

    // compute vertex size
    uint32_t vertex_size = computeVertexSize(layout);
    auto allocator = getDefaultMemoryAllocator();
    auto buffer_size = vertex_size * vertex_num;
    if (allocator && allocator == mReleaseDelegate && buffer_size < mBufferSize) {
        // re-use buffer

        // update members
        mVertexLayout = layout;
        mVertexNum = vertex_num;
        mVertexSize = vertex_size;
    } else {
        // create new buffer
        auto buffer = allocator->allocate(buffer_size);

        if (!buffer) {
            return false;
        }

        // reset parameters
        reset(buffer, vertex_size, vertex_num, layout, allocator);
    }

    return true;
}

void VertexBuffer::clear()
{
    if (mBuffer) {
        if (mReleaseDelegate) {
            mReleaseDelegate->deallocate(mBuffer);
            mReleaseDelegate = nullptr;
        }
        mVertexLayout = nullptr;
        mVertexNum = 0;
        mBufferSize = 0;
        mBuffer = nullptr;
    }
}

void VertexBuffer::reset(void* vertices, uint32_t vertex_size, uint32_t vertex_num, const VertexAttrib* vertex_layout, MemoryAllocator* release_delegate)
{
    clear();
    mVertexLayout = vertex_layout;
    mVertexNum = vertex_num;
    mVertexSize = vertex_size;
    mBuffer = vertices;
    mReleaseDelegate = release_delegate;
    mBufferSize = vertex_size * vertex_num;
}

void VertexBuffer::set(const Vector3* position, uint32_t num)
{
    if (reserve(PosVertex::LAYOUT, num)) {
        memcpy(mBuffer, position, mBufferSize);
    }
}

void VertexBuffer::set(const Vector3* position, const Colorf* colors, uint32_t num)
{
    if (reserve(PosColVertex::LAYOUT, num)) {
        auto ptr = (PosColVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].color= colors[i];
        }
    }
}

void VertexBuffer::set(const Vector3* position, const Vector2* texcoords, uint32_t num)
{
    if (reserve(PosTexVertex::LAYOUT, num)) {
        auto ptr = (PosTexVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].uv = texcoords[i];
        }
    }
}

void VertexBuffer::set(const Vector3* position, const Vector3* normals, uint32_t num)
{
    if (reserve(PosNormVertex::LAYOUT, num)) {
        auto ptr = (PosNormVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].normal = normals[i];
        }
    }
}

void VertexBuffer::set(
    const Vector3* position,
    const Colorf* colors,
    const Vector2* texcoords,
    uint32_t num)
{
    if (reserve(PosColTexVertex::LAYOUT, num)) {
        auto ptr = (PosColTexVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].color = colors[i];
            ptr[i].uv = texcoords[i];
        }
    }
}

void VertexBuffer::set(
    const Vector3* position,
    const Colorf* colors,
    const Vector3* normals,
    uint32_t num)
{
    if (reserve(PosColNormVertex::LAYOUT, num)) {
        auto ptr = (PosColNormVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].color = colors[i];
            ptr[i].normal = normals[i];
        }
    }
}

void VertexBuffer::set(
    const Vector3* position,
    const Vector2* texcoords,
    const Vector3* normals,
    uint32_t num)
{
    if (reserve(PosTexNormVertex::LAYOUT, num)) {
        auto ptr = (PosTexNormVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].uv = texcoords[i];
            ptr[i].normal = normals[i];
        }
    }
}

void VertexBuffer::set(
    const Vector3* position,
    const Colorf* colors,
    const Vector2* texcoords,
    const Vector3* normals,
    uint32_t num)
{
    if (reserve(PosColTexNormVertex::LAYOUT, num)) {
        auto ptr = (PosColTexNormVertex*)mBuffer;
        for (uint32_t i = 0; i < num; ++i) {
            ptr[i].position = position[i];
            ptr[i].color = colors[i];
            ptr[i].uv = texcoords[i];
            ptr[i].normal = normals[i];
        }
    }
}


void VertexBuffer::copy(uint32_t index, const void* vertices, uint32_t copy_num)
{
    if (index >= mVertexNum || nullptr == vertices || copy_num == 0) {
        return;
    }

    if (index + copy_num > mVertexNum) {
        copy_num = mVertexNum - index;
        LOGW("Truncate copy size.");
    }

    memcpy(
        (uint8_t*)mBuffer + (index * mVertexSize),
        vertices,
        copy_num * mVertexSize);
}

static const uint32_t sizes[] = {
    1,  // BYTE
    1,  // UNSIGNED_BYTE
    2,  // SHORT
    2,  // UNSIGNED_SHORT
    4,  // INT
    4,  // UNSIGNED_INT
    4,  // FLOAT
    4,  // FIXED
    2,  // HALF_FLOAT
    4,  // INT_2_10_10_10_REV
    4,  // UNSIGNED_INT_2_10_10_10_REV
};

uint32_t VertexBuffer::computeAttribSize(const VertexAttrib* attrib)
{
    return attrib ? attrib->type->num * sizes[attrib->type->type] : 0;
}

uint32_t VertexBuffer::computeVertexSize(const VertexAttrib* layout)
{
    uint32_t vertex_size = 0;
    while (layout && layout->type) {
        vertex_size += computeAttribSize(layout);
        ++layout;
    }
    return vertex_size;
}

void VertexBuffer::transferData()
{
    // transfer valid buffer size
    transfer(mVertexSize * mVertexNum, mBuffer);
}

VertexBuffer::WeakRef::WeakRef(VertexBuffer* vbuffer)
    : mBuffer(vbuffer)
{
    if (mBuffer) {
        mBuffer->addObserver(this);
    }
}

VertexBuffer::WeakRef::WeakRef(const WeakRef& rhv)
    : WeakRef(rhv.mBuffer)
{
}

VertexBuffer::WeakRef::~WeakRef()
{
    if (mBuffer) {
        mBuffer->removeObserver(this);
        mBuffer = nullptr;
    }
}

VertexBuffer* VertexBuffer::WeakRef::operator->()
{
    return mBuffer;
}

void VertexBuffer::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mBuffer);
}

void VertexBuffer::WeakRef::operator=(VertexBuffer* vbuffer)
{
    if (mBuffer == vbuffer) {
        return;
    }

    if (mBuffer) {
        mBuffer->removeObserver(this);
    }

    mBuffer = vbuffer;

    if (mBuffer) {
        mBuffer->addObserver(this);
    }
}

VertexBuffer::WeakRef::operator VertexBuffer*() const
{
    return mBuffer;
}

void VertexBuffer::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mBuffer = nullptr;
}

}
