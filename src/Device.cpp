/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Device.h"
#include "sg/IDevice.h"

namespace sg {

Device::Device(IDevice* ifc)
    : pointer(ifc ? ifc->getPointerInterface() : nullptr)
    , button(ifc ? ifc->getButtonInterface() : nullptr)
    , key(ifc ? ifc->getKeyInterface() : nullptr)
{
}

Device::~Device()
{
}


/*------------------------------------------------------------
 | Device.Pointer
 -------------------------------------------------------------*/

Device::Pointer::Pointer(IPointer* ifc)
    : mInterface(ifc)
{
}

Device::Pointer::~Pointer()
{
}

bool Device::Pointer::isSupported() const
{
    return mInterface != nullptr;
}


Device::Pointer::State Device::Pointer::operator()(int32_t id) const
{
    return State(mInterface ? mInterface->getState(id) : nullptr);
}

Device::Pointer::State::State(const PointerState* state)
    : mState(state)
{
}

Device::Pointer::State::State(const State& rhv)
    : mState(rhv.mState)
{
}

Device::Pointer::State::~State()
{
}

bool Device::Pointer::State::isSupported() const
{
    return mState != nullptr;
}

int32_t Device::Pointer::State::getID() const
{
    return mState ? mState->id : -1;
}

int32_t Device::Pointer::State::getX() const
{
    return mState ? mState->x : 0;
}

int32_t Device::Pointer::State::getY() const
{
    return mState ? mState->y : 0;
}

Device::Pointer::State::Position Device::Pointer::State::getPosition() const
{
    return{getX(), getY()};
}



/*------------------------------------------------------------
 | Device.Button
 -------------------------------------------------------------*/

Device::Button::Button(IButton* ifc)
    : mInterface(ifc)
{
}

Device::Button::~Button()
{
}

bool Device::Button::isSupported() const
{
    return mInterface != nullptr;
}

Device::Button::State Device::Button::operator()(int32_t id) const
{
    return State(mInterface ? mInterface->getState(id) : nullptr);
}

Device::Button::State::State(const ButtonState* state)
    : mState(state)
{
}

Device::Button::State::State(const State& rhv)
    : mState(rhv.mState)
{
}

Device::Button::State::~State()
{
}

bool Device::Button::State::isSupported() const
{
    return mState != nullptr;
}

int32_t Device::Button::State::getID() const
{
    return mState ? mState->id : -1;
}

bool Device::Button::State::isPressed() const
{
    return mState ? mState->pressed : false;
}

bool Device::Button::State::isReleased() const
{
    return mState ? !mState->pressed : true;
}


/*------------------------------------------------------------
 | Device.Key
 -------------------------------------------------------------*/

Device::Key::Key(IKey* ifc)
    : mInterface(ifc)
{
}

Device::Key::~Key()
{
}

bool Device::Key::isSupported() const
{
    return mInterface != nullptr;
}

Device::Key::State Device::Key::operator()(int32_t ch) const
{
    return State(mInterface ? mInterface->getState(ch) : nullptr);
}

Device::Key::State::State(const KeyState* state)
    : mState(state)
{
}

Device::Key::State::State(const State& rhv)
    : mState(rhv.mState)
{
}

Device::Key::State::~State()
{
}

bool Device::Key::State::isSupported() const
{
    return mState != nullptr;
}

int32_t Device::Key::State::getChar() const
{
    return mState ? mState->ch : 0;
}

bool Device::Key::State::isPressed() const
{
    return mState ? mState->pressed : false;
}

bool Device::Key::State::isReleased() const
{
    return mState ? !mState->pressed : true;
}

}
