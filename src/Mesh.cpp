/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define LOG_TAG "Mesh"

#include "sg/Mesh.h"
#include "sg/Program.h"
#include "sg/Material.h"
#include "OpenGL.h"

namespace sg {

static const GLenum modes[] = {
    GL_POINTS, //SG_POINTS = 0,
    GL_LINE_STRIP, //SG_LINE_STRIP,
    GL_LINE_LOOP, //SG_LINE_LOOP,
    GL_LINES, //SG_LINES,
    GL_TRIANGLE_STRIP, //SG_TRIANGLE_STRIP,
    GL_TRIANGLE_FAN, //SG_TRIANGLE_FAN,
    GL_TRIANGLES, //SG_TRIANGLES,
};

Mesh::Mesh()
    : mPrimitiveType(SG_POINTS)
{
}

Mesh::~Mesh()
{
}

PrimitiveType Mesh::getPrimitiveType() const
{
    return (PrimitiveType)mPrimitiveType;
}

void Mesh::setPrimitiveType(PrimitiveType type)
{
    mPrimitiveType = (uint8_t)type;
}

void Mesh::setVertices(VertexBuffer* vbuffer)
{
    mVertices = vbuffer;
}

VertexBuffer* Mesh::getVertices()
{
    return mVertices;
}

void Mesh::setFaces(IndexBuffer* faces)
{
    mFaces = faces;
}

IndexBuffer* Mesh::getFaces()
{
    return mFaces;
}

void Mesh::update(Context& /*gl*/)
{
}

bool Mesh::drawFaces(Context& gl, Material& material)
{
    if (nullptr == mVertices) {
        return false;
    }

    // 1. setup program and uniform variables in material
    auto program = material.getProgram();
    if (!program) {
        LOGE("No program.");
        return false;
    }

    material.copyProperties(gl);

    // 2. setup vertices data and vertex layout
    mVertices->transferData();
    program->setupLayout(mVertices->getVertexLayout());

    // 3. setup indeces and draw

    auto mode = modes[mPrimitiveType];

    if (mFaces && mFaces->getIndicesNum() > 0) {
        auto type = mFaces->isInt() ? GL_UNSIGNED_INT : GL_UNSIGNED_SHORT;
        GLsizei count = mFaces->getIndicesNum();
        auto indices = mFaces->getIndices();

        invokeGL(glDrawElements(mode, count, type, indices));
    } else {
        invokeGL(glDrawArrays(mode, 0, mVertices->getVertexNum()));
    }

    material.cleanupAfterDraw();

    return false;
}

}
