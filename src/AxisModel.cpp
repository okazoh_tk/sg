/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/model/AxisModel.h"

namespace sg {

AxisModel::AxisModel()
    : GradientModel(Buffer::STATIC_DRAW, Buffer::STATIC_DRAW)
{
    // delegate GradientModel(usage, usage)
}

AxisModel::AxisModel(Buffer::Usage vbuffer_usage, Buffer::Usage ibuffer_usage)
    : GradientModel(vbuffer_usage, ibuffer_usage)
{
}

AxisModel::~AxisModel()
{
}

void AxisModel::setLine(float extent)
{
    setLine(-extent, extent);
}

void AxisModel::setLine(float start, float extent)
{
    GradientModel::VertexType vertices[] = {
        { { start, 0, 0 }, { 1, 0, 0, 1 } },
        { { extent, 0, 0 }, { 1, 0, 0, 1 } },
        { { 0, start, 0 }, { 0, 1, 0, 1 } },
        { { 0, extent, 0 }, { 0, 1, 0, 1 } },
        { { 0, 0, start }, { 0, 0, 1, 1 } },
        { { 0, 0, extent }, { 0, 0, 1, 1 } },
    };

    setVertices(vertices, COUNT_OF(vertices));
    setPrimitiveType(SG_LINES);
}

}
