/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/EventDelegate.h"
#include "sg/IDevice.h"
#include "OpenGL.h"
#include "./GLFWWindow.h"
#include "SimpleDevice.h"

namespace sg {

#if ENABLE_GLFW != 0

GLFWWindow::GLFWWindow(const Size& window_size)
    : Window(window_size)
    , mDevice(new SimpleDevice())
    , mDelegate(nullptr)
{
    if (!glfwInit()) {
        return;
    }
}

GLFWWindow::~GLFWWindow()
{
    delete mDevice;
    glfwTerminate();
}

bool GLFWWindow::initWindow()
{
    auto& window_size = getSize();
    auto window = glfwCreateWindow(window_size.width, window_size.height, "Hello World", NULL, NULL);

    if (!window) {
        return false;
    }

    glfwSetWindowUserPointer(window, this);

    // setup event handler
    glfwSetKeyCallback(window, (GLFWkeyfun)keyCallback);
    glfwSetMouseButtonCallback(window, (GLFWmousebuttonfun)mouseCallback);
    glfwSetScrollCallback(window, (GLFWscrollfun)scrollCallback);
    glfwSetCursorPosCallback(window, (GLFWcursorposfun)cursorCallback);

    glfwMakeContextCurrent(window);
    mWindow = window;

    return true;
}

bool GLFWWindow::handleEvents(EventDelegate& eventDelegate)
{
    mDelegate = &eventDelegate;
    bool to_be_continue = !glfwWindowShouldClose((GLFWwindow*)mWindow);

    if (to_be_continue) {

        eventDelegate.draw();

        /* Poll for and process events */
        glfwPollEvents();
    }
    mDelegate = nullptr;

    return to_be_continue;
}

void GLFWWindow::exitLoop()
{
    glfwSetWindowShouldClose((GLFWwindow*)mWindow, 1);
}

void GLFWWindow::swap()
{
    glfwSwapBuffers((GLFWwindow*)mWindow);
}

IDevice* GLFWWindow::getDeviceInterface()
{
    return mDevice;
}

SimpleDevice* GLFWWindow::getDevice()
{
    return mDevice;
}

static GLFWWindow* getSelf(void* w) {
    auto w1 = (GLFWwindow*)w;
    return (GLFWWindow*)glfwGetWindowUserPointer(w1);
}

#define inRange(value, minval, maxval)  ((minval) <= (value) && (value) <= (maxval))

uint32_t GLFWWindow::convertKey(int keycode)
{
    // 0-9, a-z
    if (inRange(keycode, 0x30, 0x39) || inRange(keycode, 0x41, 0x5A)) {
        return keycode;
    }
#define keymap(wkey, vk)    case wkey: return vk

    switch (keycode) {
        keymap(GLFW_KEY_BACKSPACE, SG_VK_BACKSPACE);
        keymap(GLFW_KEY_TAB, SG_VK_TAB);
        keymap(GLFW_KEY_ENTER, SG_VK_ENTER);
        keymap(GLFW_KEY_ESCAPE, SG_VK_ESCAPE);
        keymap(GLFW_KEY_SPACE, SG_VK_SPACE);

        keymap(GLFW_KEY_LEFT, SG_VK_LEFT);
        keymap(GLFW_KEY_UP, SG_VK_UP);
        keymap(GLFW_KEY_RIGHT, SG_VK_RIGHT);
        keymap(GLFW_KEY_DOWN, SG_VK_DOWN);
        keymap(GLFW_KEY_PAGE_UP, SG_VK_PAGE_UP);
        keymap(GLFW_KEY_PAGE_DOWN, SG_VK_PAGE_DOWN);

        keymap(GLFW_KEY_F1, SG_VK_F1);
        keymap(GLFW_KEY_F2, SG_VK_F2);
        keymap(GLFW_KEY_F3, SG_VK_F3);
        keymap(GLFW_KEY_F4, SG_VK_F4);
        keymap(GLFW_KEY_F5, SG_VK_F5);
        keymap(GLFW_KEY_F6, SG_VK_F6);
        keymap(GLFW_KEY_F7, SG_VK_F7);
        keymap(GLFW_KEY_F8, SG_VK_F8);
        keymap(GLFW_KEY_F9, SG_VK_F9);
        keymap(GLFW_KEY_F10, SG_VK_F10);
        keymap(GLFW_KEY_F11, SG_VK_F11);
        keymap(GLFW_KEY_F12, SG_VK_F12);

        keymap(GLFW_KEY_LEFT_SHIFT, SG_VK_LEFT_SHIFT);
        keymap(GLFW_KEY_LEFT_CONTROL, SG_VK_LEFT_CONTROL);
        keymap(GLFW_KEY_LEFT_ALT, SG_VK_LEFT_ALT);
        keymap(GLFW_KEY_RIGHT_SHIFT, SG_VK_RIGHT_SHIFT);
        keymap(GLFW_KEY_RIGHT_CONTROL, SG_VK_RIGHT_CONTROL);
        keymap(GLFW_KEY_RIGHT_ALT, SG_VK_LEFT_ALT);
    }

    return SG_VK_UNKNOWN;
}

void GLFWWindow::keyCallback(
    void* w,
    int glfw_key,
    int scancodde,
    int action,
    int mods)
{
    auto thiz = getSelf(w);

    if (!thiz->mDelegate) {
        return;
    }

    auto& ikey = thiz->mDevice->key();
    auto key = convertKey(glfw_key);

    KeyEvent::Type e = KeyEvent::KEY_DOWN;
    switch (action) {
    case GLFW_PRESS:
        ikey.press(key);
        e = KeyEvent::KEY_DOWN;
        break;
    case GLFW_RELEASE:
        ikey.release(key);
        e = KeyEvent::KEY_UP;
        break;
    }

    thiz->mDelegate->fireKeyEvent(e, key);
}

void GLFWWindow::mouseCallback(void* w, int button, int action, int mods)
{
    auto thiz = getSelf(w);

    if (!thiz->mDelegate) {
        return;
    }

    auto& ibutton = thiz->mDevice->button();

    ButtonEvent::Type e = ButtonEvent::BUTTON_DOWN;
    switch (action) {
    case GLFW_PRESS:
        ibutton.press(button);
        e = ButtonEvent::BUTTON_DOWN;
        break;
    case GLFW_RELEASE:
        ibutton.release(button);
        e = ButtonEvent::BUTTON_UP;
        break;
    }

    thiz->mDelegate->fireButtonEvent(e, button);
}

void GLFWWindow::scrollCallback(void* w, double xoffset, double yoffset)
{
    auto thiz = getSelf(w);

    if (thiz->mDelegate) {
        thiz->mDelegate->fireScrollEvent((int32_t)xoffset, (int32_t)yoffset);
    }
}

void GLFWWindow::cursorCallback(void* w, double x, double y)
{
    auto thiz = getSelf(w);

    if (!thiz->mDelegate) {
        return;
    }

    auto& ipointer = thiz->mDevice->pointer();
    int id = 0;

    ipointer.move(id, (int32_t)x, (int32_t)y);
    thiz->mDelegate->firePointerEvent(PointerEvent::POINTER_MOVE, id);
}

#endif

}
