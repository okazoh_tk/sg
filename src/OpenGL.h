/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_OPENGL_H_INCLUDED_
#define SG_OPENGL_H_INCLUDED_

#include "sg/Config.h"


/* Select OpenGL for Android.
*/
#if defined(TARGET_OS_ANDROID)

#include <android/api-level.h>
#if __ANDROID_API__ >= 21
#define SG_OPENGL_ES 31
#elif __ANDROID_API__ >= 18
#define SG_OPENGL_ES 3
#elif __ANDROID_API__ >= 8
#define SG_OPENGL_ES 2
#else
// invalid platform
#endif

/* iOS version.
*/
#elif defined(TARGET_OS_IOS)

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000 // 7.0 or over
#define SG_OPENGL_ES 3
#else
#define SG_OPENGL_ES 2
#endif

/* Windows version.
*/
#elif defined(TARGET_OS_WIN)

#include <Windows.h>
#if defined SG_OPENGL_ES
#define ENABLE_GLFW 0
#define ENABLE_WIN32WINDOW 1
#endif

#ifndef ENABLE_GLFW
#define ENABLE_GLFW 1
#endif

#if defined(_MSC_VER) && (_MSC_VER >= 1700)
#define GLM_FORCE_CXX03
#endif

#elif defined(TARGET_OS_MAC)

#ifndef ENABLE_GLFW
#define ENABLE_GLFW 1
#endif

/* Other platform version
*/
#else

#endif


#ifndef ENABLE_GLFW
#define ENABLE_GLFW 0
#endif
#ifndef ENABLE_WIN32WINDOW
#define ENABLE_WIN32WINDOW 0
#endif


#if defined(SG_OPENGL_ES)
#define ON_GLES(exp) [&](){exp;}()
#define ON_GL(exp)   ((void)0)
#else
#define ON_GLES(exp) ((void)0)
#define ON_GL(exp)   [&](){exp;}()
#endif


/* Include OpenGL ES headers
*/
#if defined(SG_OPENGL_ES)

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <EGL/eglplatform.h>

#if SG_OPENGL_ES == 2
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2platform.h>
#elif SG_OPENGL_ES == 3
#include <GLES3/gl3.h>
#include <GLES2/gl2ext.h>
#include <GLES3/gl3platform.h>
#elif SG_OPENGL_ES == 31
#include <GLES3/gl31.h>
#include <GLES2/gl2ext.h>
#include <GLES3/gl3platform.h>
#endif

#elif defined(ENABLE_GLFW) && (ENABLE_GLFW != 0)

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#if 0
/* Include OpenGL headers */
#if defined(TARGET_OS_MAC)
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#else

#if defined(TARGET_OS_WIN)
#include <Windows.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <GL/gl.h>
#else
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/glew.h>
#include <GL/glxew.h>
#include <GL/glx.h>
#include <GL/glxext.h>
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#endif

#endif

#endif


#if !defined(NDEBUG)
#define invokeGL(exp) [&](){ \
        exp; \
        auto err = glGetError(); \
        LOGE_IF(GL_NO_ERROR != err, #exp "(err=%04x)", err);\
    }()

#define invokeGL_ret(exp) [&](){ \
        auto ret = exp; \
        auto err = glGetError(); \
        LOGE_IF(GL_NO_ERROR != err, #exp "(err=%04x)", err);\
        return ret; \
    }()
#else
#define invokeGL(exp) exp
#endif


#endif
