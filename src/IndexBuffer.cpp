/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define LOG_TAG "IndexBuffer"

#include "sg/IndexBuffer.h"
#include "sg/MemoryAllocator.h"
#include "OpenGL.h"
#include <string.h>

namespace sg {

#define computeBufferSize(is_int, num) \
    (num * (is_int ? sizeof(uint32_t) : sizeof(uint16_t)))

#define setMember(is_int, indices_num, buffer_size, indices, release_delegate) \
    mIsInt = (is_int) ? 1 : 0; \
    mIndicesNum = indices_num; \
    mBufferSize = buffer_size; \
    mIndices = indices; \
    mReleaseDelegate = release_delegate

IndexBuffer::IndexBuffer(Usage usage)
    : Buffer(ELEMENT_ARRAY_BUFFER, usage)
    , mIsInt(0)
    , mIndicesNum(0)
    , mBufferSize(0)
    , mIndices(nullptr)
    , mReleaseDelegate(nullptr)
{
}

IndexBuffer::~IndexBuffer()
{
    clearObserver();
    clear();
}

bool IndexBuffer::isInt() const
{
    return mIsInt == 1;
}

bool IndexBuffer::isShort() const
{
    return !isInt();
}

const void* IndexBuffer::getIndices() const
{
    return mIndices;
}

uint32_t IndexBuffer::getIndicesNum() const
{
    return mIndicesNum;
}

uint32_t IndexBuffer::getBufferSize() const
{
    return mBufferSize;
}

bool IndexBuffer::reserve(uint32_t indices_num, bool is_short)
{
    if (indices_num == 0) {
        return false;
    }

    auto allocator = getDefaultMemoryAllocator();
    uint32_t buffer_size = computeBufferSize(!is_short, indices_num);

    if (allocator && allocator == mReleaseDelegate && buffer_size < mBufferSize) {
        // re-use buffer

        // update members
        mIsInt = is_short ? 0 : 1;
        mIndicesNum = indices_num;
    } else {
        // replace buffer
        auto buffer = allocator->allocate(buffer_size);
        if (nullptr == buffer) {
            return false;
        }
        reset(buffer, indices_num, is_short, allocator);
    }

    return true;
}

void IndexBuffer::clear()
{
    if (mIndices) {
        if (mReleaseDelegate) {
            mReleaseDelegate->deallocate(mIndices);
            mReleaseDelegate = nullptr;
        }

        mIndices = nullptr;
        mBufferSize = 0;
    }
}

void IndexBuffer::reset(
    void* indices,
    uint32_t indices_num,
    bool is_short,
    MemoryAllocator* release_delegate)
{
    clear();
    setMember(
        !is_short,
        indices_num,
        computeBufferSize(!is_short, indices_num),
        indices,
        release_delegate);
}

void IndexBuffer::set(const uint16_t* indices, uint32_t indices_num)
{
    if (reserve(indices_num, true)) {
        memcpy(mIndices, indices, indices_num * sizeof(uint16_t));
    }
}

void IndexBuffer::set(const uint32_t* indices, uint32_t indices_num)
{
    ON_GLES(LOGW("OpenGL ES does NOT support uint32_t type IndexBuffer."));
    if (reserve(indices_num, false)) {
        memcpy(mIndices, indices, indices_num * sizeof(uint16_t));
    }
}

void IndexBuffer::copy(uint32_t index, const uint16_t* indices, uint32_t copy_num)
{
    if (index >= mIndicesNum || nullptr == indices || copy_num == 0) {
        return;
    }

    if (index + copy_num > mIndicesNum) {
        copy_num = mIndicesNum - index;
        LOGW("Truncate copy size.");
    }

    if (isShort()) {
        memcpy(
            (uint16_t*)mIndices + index,
            indices,
            computeBufferSize(false, copy_num));
    } else {
        uint32_t* ptr = (uint32_t*)mIndices + index;
        for (uint32_t i = 0; i < copy_num; ++i) {
            ptr[i] = indices[i];
        }
    }
}

void IndexBuffer::copy(uint32_t index, const uint32_t* indices, uint32_t copy_num)
{
    if (index >= mIndicesNum || nullptr == indices || copy_num == 0) {
        return;
    }

    if (index + copy_num > mIndicesNum) {
        copy_num = mIndicesNum - index;
        LOGW("Truncate copy size.");
    }

    if (isShort()) {
        //LOGD("Copy uint32_t to uint16_t, possible loss of data.");
        uint16_t* ptr = (uint16_t*)mIndices + index;
        for (uint32_t i = 0; i < copy_num; ++i) {
            ptr[i] = (uint16_t)indices[i];
        }
    } else {
        memcpy(
            (uint32_t*)mIndices + index,
            indices,
            computeBufferSize(false, copy_num));
    }
}

void IndexBuffer::transferData()
{
    // transfer valid buffer size
    transfer(computeBufferSize(mIsInt, mIndicesNum), mIndices);
}

IndexBuffer::WeakRef::WeakRef(IndexBuffer* ibuffer)
    : mBuffer(ibuffer)
{
    if (mBuffer) {
        mBuffer->addObserver(this);
    }
}

IndexBuffer::WeakRef::WeakRef(const WeakRef& rhv)
    : WeakRef(rhv.mBuffer)
{
}

IndexBuffer::WeakRef::~WeakRef()
{
    if (mBuffer) {
        mBuffer->removeObserver(this);
        mBuffer = nullptr;
    }
}

IndexBuffer* IndexBuffer::WeakRef::operator->()
{
    return mBuffer;
}

void IndexBuffer::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mBuffer);
}

void IndexBuffer::WeakRef::operator=(IndexBuffer* ibuffer)
{
    if (mBuffer == ibuffer) {
        return;
    }

    if (mBuffer) {
        mBuffer->removeObserver(this);
    }

    mBuffer = ibuffer;

    if (mBuffer) {
        mBuffer->addObserver(this);
    }
}

IndexBuffer::WeakRef::operator IndexBuffer*() const
{
    return mBuffer;
}

void IndexBuffer::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mBuffer = nullptr;
}

}
