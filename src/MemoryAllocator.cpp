/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/MemoryAllocator.h"
#include <stdlib.h>
#include <string.h>

namespace sg {

static MemoryAllocator* defaultMemoryAllocator = nullptr;

SG_PUBLIC_API void setDefaultMemoryAllocator(MemoryAllocator* new_allocator)
{
    VERIFY(nullptr == defaultMemoryAllocator);
    defaultMemoryAllocator = new_allocator;
}

SG_PUBLIC_API MemoryAllocator* getDefaultMemoryAllocator()
{
    return defaultMemoryAllocator;
}

MemoryAllocator::MemoryAllocator()
{
}

MemoryAllocator::~MemoryAllocator()
{
}

char* MemoryAllocator::duplicate(const char* src)
{
    auto len = (uint32_t)strlen(src) + 1;
    char* ret = (char*)allocate(len);
    strcpy_s(ret, len, src);
    return ret;
}

char* MemoryAllocator::concat(const char* srcs[], uint32_t num)
{
    uint32_t len = 1;
    for (decltype(num) i = 0; i < num; ++i) {
        len += (uint32_t)strlen(srcs[i]);
    }

    char* ret = (char*)allocate(len);
    char* dest = ret;

    for (decltype(num) i = 0; i < num; ++i) {
        auto copied_size = (uint32_t)strlen(srcs[i]);
        strcpy_s(dest, len, srcs[i]);
        len -= copied_size;
        dest += copied_size;
    }
    return ret;
}

Heap::Heap()
{
}

Heap::~Heap()
{
}

void* Heap::allocate(uint32_t size)
{
    return malloc(size);
}

void Heap::deallocate(void* ptr)
{
    free(ptr);
}

}
