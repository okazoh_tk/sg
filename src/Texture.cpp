/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Texture.h"
#include "sg/MemoryAllocator.h"
#include "OpenGL.h"

namespace sg {

/*

+--------------------+---+---+---+
|format              |2.0|DPT|3.0|
+====================+===+===+===+
|GL_RGB              | v |   | v |
|GL_RGBA             | v |   | v |
|GL_LUMINANCE        | v |   | v |
|GL_LUMINANCE_ALPHA  | v |   | v |
|GL_ALPHA            | v |   | v |
|GL_DEPTH_COMPONENT  |   | v | v |
|GL_RED              |   |   | v |
|GL_RED_INTEGER      |   |   | v |
|GL_RG               |   |   | v |
|GL_RG_INTEGER       |   |   | v |
|GL_RGB_INTEGER      |   |   | v |
|GL_RGBA_INTEGER     |   |   | v |
|GL_DEPTH_STENCIL    |   |   | v |
+--------------------+---+---+---+

+------------------------------------+---+---+---+
|type                                |2.0|DPT|3.0|
+====================================+===+===+===+
|GL_UNSIGNED_BYTE                    | v |   | v |
|GL_UNSIGNED_SHORT_5_6_5             | v |   | v |
|GL_UNSIGNED_SHORT_4_4_4_4           | v |   | v |
|GL_UNSIGNED_SHORT_5_5_5_1           | v |   | v |
|GL_UNSIGNED_SHORT                   |   | v | v |
|GL_UNSIGNED_INT                     |   | v | v |
|GL_BYTE                             |   |   | v |
|GL_SHORT                            |   |   | v |
|GL_INT                              |   |   | v |
|GL_HALF_FLOAT                       |   |   | v |
|GL_FLOAT                            |   |   | v |
|GL_UNSIGNED_INT_2_10_10_10_REV      |   |   | v |
|GL_UNSIGNED_INT_10F_11F_11F_REV     |   |   | v |
|GL_UNSIGNED_INT_5_9_9_9_REV         |   |   | v |
|GL_UNSIGNED_INT_24_8                |   |   | v |
|GL_FLOAT_32_UNSIGNED_INT_24_8_REV   |   |   | v |
+------------------------------------+---+---+---+
*/

static const GLenum components[] = {
    GL_RGB,
    GL_RGBA,
    GL_LUMINANCE,
    GL_LUMINANCE_ALPHA,
    GL_ALPHA,
    GL_DEPTH_COMPONENT,

#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    GL_RED,
    GL_RED_INTEGER,
    GL_RG,
    GL_RG_INTEGER,
    GL_RGB_INTEGER,
    GL_RGBA_INTEGER,
    GL_DEPTH_STENCIL,
#endif
};

static const GLenum types[] = {
    GL_UNSIGNED_BYTE,
    GL_UNSIGNED_SHORT_5_6_5,
    GL_UNSIGNED_SHORT_4_4_4_4,
    GL_UNSIGNED_SHORT_5_5_5_1,
    GL_UNSIGNED_SHORT,
    GL_UNSIGNED_INT,

#if !defined(SG_OPENGL_ES) || (SG_OPENGL_ES != 2)
    GL_BYTE,
    GL_SHORT,
    GL_INT,
    GL_HALF_FLOAT,
    GL_FLOAT,
    GL_UNSIGNED_INT_2_10_10_10_REV,
    GL_UNSIGNED_INT_10F_11F_11F_REV,
    GL_UNSIGNED_INT_5_9_9_9_REV,
    GL_UNSIGNED_INT_24_8,
    GL_FLOAT_32_UNSIGNED_INT_24_8_REV,
#endif
};

static const GLenum internal_formats[] = {
    GL_RGB,
    GL_RGBA,
    GL_ALPHA,
    GL_LUMINANCE,
    GL_LUMINANCE_ALPHA,
    GL_DEPTH_COMPONENT,
};

#define getCompIndex(f) ((f&0xff00) >> 8)
#define getTypeIndex(f) (f&0x00ff)

Texture::Texture()
    : mName(0)
    , mWidth(0)
    , mHeight(0)
    , mFormat(RGB)
    , mPixels(nullptr)
    , mReleaseDelegate(nullptr)
    , mNeedSync(false)
{
}

Texture::~Texture()
{
    clearObserver();
    if (mReleaseDelegate && mPixels) {
        mReleaseDelegate->deallocate(mPixels);
    }
    mPixels = nullptr;
    mReleaseDelegate = nullptr;

    if (mName) {
        glDeleteTextures(1, &mName);
    }
}

void Texture::set(uint16_t width, uint16_t height, Format internal_format)
{
    static const PixelFormat tables[] = {
#if !defined(SG_OPENGL_ES)
        (PixelFormat)(sg::Format::RGB | sg::Format::BYTE),
        (PixelFormat)(sg::Format::RGBA | sg::Format::BYTE),
#else
        SG_RGB_565,//RGB
        SG_RGBA_4444,//RGBA
#endif
        SG_A8,//ALPHA
        (PixelFormat)(sg::Format::LUMINANCE | sg::Format::BYTE),//LUMINANCE
        (PixelFormat)(sg::Format::LUMINANCE_ALPHA | sg::Format::BYTE),//LUMINANCE_ALPHA
        SG_DEPTH,//DEPTH
    };

    set(width, height, internal_format, tables[internal_format]);
}

void Texture::set(
    uint16_t width,
    uint16_t height,
    Format internal_format,
    PixelFormat pixel_format)
{
    set(width, height, internal_format, nullptr, pixel_format);
}


void Texture::set(
    uint16_t width,
    uint16_t height,
    Format internalformat,
    void* pixels,
    PixelFormat pixel_format,
    MemoryAllocator* release_delegate)
{
    ASSERT_RET(getCompIndex(pixel_format) < COUNT_OF(components), );
    ASSERT_RET(getTypeIndex(pixel_format) < COUNT_OF(types), );

    // clear old buffer
    if (mPixels && mReleaseDelegate) {
        mReleaseDelegate->deallocate(mPixels);
    }

    mWidth = width;
    mHeight = height;
    mFormat = internalformat;
    mPixelFormat = pixel_format;
    mPixels = pixels;
    mReleaseDelegate = release_delegate;

    mNeedSync = true;
}

uint16_t Texture::getWidth() const
{
    return mWidth;
}

uint16_t Texture::getHeight() const
{
    return mHeight;
}

Texture::Format Texture::getFormat() const
{
    return mFormat;
}

uint32_t Texture::getName() const
{
    return mName;
}

void Texture::bind()
{
    if (mName == 0) {
        glGenTextures(1, &mName);
        if (mName == 0) {
            return;
        }
    }

    invokeGL(glBindTexture(GL_TEXTURE_2D, mName));

    if (mNeedSync) {

        // copy pixels if need
        if (mPixels) {
            invokeGL(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));
        }

        invokeGL(glTexImage2D(
            GL_TEXTURE_2D, 0,
            internal_formats[mFormat],
            mWidth, mHeight,
            0,
            components[getCompIndex(mPixelFormat)],
            types[getTypeIndex(mPixelFormat)],
            mPixels));

        if (mReleaseDelegate) {
            mReleaseDelegate->deallocate(mPixels);
            mReleaseDelegate = nullptr;
            mPixels = nullptr;
        }

        invokeGL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        invokeGL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
        invokeGL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
        invokeGL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

        mNeedSync = false;
    }
}

void Texture::unbind()
{
    invokeGL(glBindTexture(GL_TEXTURE_2D, 0));
}


Texture::WeakRef::WeakRef(Texture* texture)
    : mTexture(texture)
{
    if (mTexture) {
        mTexture->addObserver(this);
    }
}

Texture::WeakRef::WeakRef(const WeakRef& rhv)
    : WeakRef(rhv.mTexture)
{
}

Texture::WeakRef::~WeakRef()
{
    if (mTexture) {
        mTexture->removeObserver(this);
        mTexture = nullptr;
    }
}

Texture* Texture::WeakRef::operator->()
{
    return mTexture;
}

void Texture::WeakRef::operator=(const WeakRef& rhv)
{
    operator=(rhv.mTexture);
}

void Texture::WeakRef::operator=(Texture* texture)
{
    if (mTexture == texture) {
        return;
    }

    if (mTexture) {
        mTexture->removeObserver(this);
    }

    mTexture = texture;

    if (mTexture) {
        mTexture->addObserver(this);
    }
}

Texture::WeakRef::operator Texture*() const
{
    return mTexture;
}

void Texture::WeakRef::onReleased(SharedObject* /*caller*/)
{
    mTexture = nullptr;
}

}
