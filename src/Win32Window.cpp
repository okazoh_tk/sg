/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "./Win32Window.h"
#include "sg/EventDelegate.h"
#include "EGLObject.h"
#include <Windowsx.h>

namespace sg {

#if ENABLE_WIN32WINDOW != 0

Win32Window::Win32Window(const Size& window_size)
    : Window(window_size)
    , mAtom(0)
    , mWindowHandle(nullptr)
    , mEventDelegate(nullptr)
{
}

Win32Window::~Win32Window()
{
    destroyWindow();
}

HWND Win32Window::initWindow()
{
    if (mWindowHandle) {
        // already created
        return NULL;
    }

    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = windowProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = GetModuleHandleA(NULL);
    wcex.hIcon          = NULL;
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = "Default Window";
    wcex.hIconSm        = NULL;

    mAtom = RegisterClassExA(&wcex);
    ASSERT_RET(mAtom != 0, NULL);

    auto& size = getSize();
    RECT rect = {0, 0, size.width, size.height};
    DWORD style = WS_OVERLAPPEDWINDOW & (~WS_THICKFRAME);
    DWORD ex_style = 0/*WS_EX_LAYERED*/;
    auto adjusted = AdjustWindowRectEx(&rect, style, FALSE, ex_style);
    ASSERT_RET(adjusted, NULL);

    mWindowHandle = CreateWindowExA(
        ex_style,
        (LPCSTR)mAtom,
        "sg's Application",
        style,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        rect.right - rect.left,
        rect.bottom - rect.top,
        NULL,
        NULL,
        GetModuleHandle(NULL),
        NULL);

    LOGV_IF(mWindowHandle, "Success to create window(width=%d, height=%d)", size.width, size.height);

    if (mWindowHandle) {
        if (initContext()) {
            SetWindowLongPtr(mWindowHandle, GWLP_USERDATA, (LONG_PTR)this);
            ShowWindow(mWindowHandle, SW_SHOW);
        } else {
            destroyWindow();
        }
    }

    return mWindowHandle;
}

void Win32Window::destroyWindow()
{
    if (mWindowHandle)
    {
        DestroyWindow(mWindowHandle);
        mWindowHandle = NULL;
        LOGV("Destroy window.");
    }

    if (mAtom)
    {
        UnregisterClassA((LPCSTR)mAtom, GetModuleHandleA(NULL));
        mAtom = 0;
        LOGV("Unregistered class.");
    }
}

HWND Win32Window::getWindowHandle()
{
    return mWindowHandle;
}

IDevice* Win32Window::getDevice()
{
    return &mDevice;
}

LRESULT Win32Window::windowProc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    auto self = (Win32Window*)GetWindowLongPtr(wnd, GWLP_USERDATA);
    if (self) {
        return self->onMessage(msg, wparam, lparam);
    } else {
        return DefWindowProcA(wnd, msg, wparam, lparam);
    }
}

bool Win32Window::handleEvents(EventDelegate& eventDelegate)
{
    bool to_be_continued = true;
    mEventDelegate = &eventDelegate;

    MSG msg;
    do {
        if (PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessageA(&msg);

            if (msg.message == WM_QUIT) {
                to_be_continued = false;
            }
        } else {
            eventDelegate.draw();
            break;
        }
    } while (to_be_continued);

    return to_be_continued;
}

LRESULT Win32Window::onMessage(UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch (msg) {
    case WM_CLOSE:
        // exit loop and destroy window
        PostQuitMessage(0);
        return 0;

    case WM_KEYDOWN:
        fireEvent(true, wparam);
        return 0;

    case WM_KEYUP:
        fireEvent(false, wparam);
        return 0;

    case WM_LBUTTONDOWN:
        fireEvent(true, VK_LBUTTON);
        return 0;

    case WM_LBUTTONUP:
        fireEvent(false, VK_LBUTTON);
        return 0;

    case WM_RBUTTONDOWN:
        fireEvent(true, VK_RBUTTON);
        return 0;

    case WM_RBUTTONUP:
        fireEvent(false, VK_RBUTTON);
        return 0;

    case WM_MBUTTONDOWN:
        fireEvent(true, VK_MBUTTON);
        return 0;

    case WM_MBUTTONUP:
        fireEvent(false, VK_RBUTTON);
        return 0;

    case WM_MOUSEMOVE:
        {
            auto x = GET_X_LPARAM(lparam);
            auto y = GET_Y_LPARAM(lparam);
            int32_t id = 0;

            mDevice.pointer().move(id, x, y);
            mEventDelegate->firePointerEvent(PointerEvent::POINTER_MOVE, id);
        }
        return 0;

    case WM_MOUSEWHEEL:
        scroll(0, (short)HIWORD(wparam) / WHEEL_DELTA);
        return 0;

    case WM_VSCROLL:
        // todo: fire vertical scroll event
        break;

    case WM_HSCROLL:
        // todo: fire horizontal scroll event
        break;


    case WM_PAINT:
        {
            RECT rect;
            if(mEventDelegate && GetUpdateRect(mWindowHandle, &rect, FALSE)) {
                Rect r;
                r.point.x = (int16_t)rect.left;
                r.point.y = (int16_t)rect.top;
                r.size.width = (uint16_t)(rect.right - rect.left);
                r.size.height = (uint16_t)(rect.bottom - rect.top);

                mEventDelegate->draw(r);
            }
        }
        return 0;
    }

    return DefWindowProc(this->mWindowHandle, msg, wparam, lparam);
}

static const WPARAM shift_keys[] = { VK_LSHIFT, VK_RSHIFT };
static const int32_t vk_shift_keys[] = { SG_VK_LEFT_SHIFT, SG_VK_RIGHT_SHIFT };
static const WPARAM ctrl_keys[] = { VK_LCONTROL, VK_RCONTROL };
static const int32_t vk_ctrl_keys[] = { SG_VK_LEFT_CONTROL, SG_VK_RIGHT_CONTROL };
static const WPARAM alt_keys[] = { VK_LMENU, VK_RMENU };
static const int32_t vk_alt_keys[] = { SG_VK_LEFT_ALT, SG_VK_RIGHT_ALT };

void Win32Window::fireEvent(bool pressed, WPARAM wparam)
{
    switch (wparam) {
    case VK_SHIFT:
        checkSideKeyEvent(pressed, shift_keys, vk_shift_keys);
        break;
    case VK_CONTROL:
        checkSideKeyEvent(pressed, ctrl_keys, vk_ctrl_keys);
        break;
    case VK_MENU:
        checkSideKeyEvent(pressed, alt_keys, vk_alt_keys);
        break;
    default:
        {
            bool is_button;
            uint32_t id = convertKey(wparam, is_button);
            if (is_button) {
                if (pressed) {
                    mDevice.button().press(id);
                    mEventDelegate->fireButtonEvent(ButtonEvent::BUTTON_DOWN, id);
                } else {
                    mDevice.button().release(id);
                    mEventDelegate->fireButtonEvent(ButtonEvent::BUTTON_UP, id);
                }
            } else {
                if (pressed) {
                    mDevice.key().press(id);
                    mEventDelegate->fireKeyEvent(KeyEvent::KEY_DOWN, id);
                } else {
                    mDevice.key().release(id);
                    mEventDelegate->fireKeyEvent(KeyEvent::KEY_UP, id);
                }
            }
        }
    }
}

void Win32Window::checkSideKeyEvent(bool pressed, const WPARAM wkeycode[2], const int32_t keycode[2])
{
    for (int i = 0; i < 2; ++i) {
        if (mDevice.key().getState(keycode[i])->pressed != pressed) {
            bool key_pressed = (0 != (GetAsyncKeyState(wkeycode[i]) & 0x8000));
            if (key_pressed && pressed) {
                // fire press event
                mDevice.key().press(keycode[i]);
                mEventDelegate->fireKeyEvent(KeyEvent::KEY_DOWN, keycode[i]);
            }

            if (!key_pressed && !pressed) {
                // fire release event
                mDevice.key().release(keycode[i]);
                mEventDelegate->fireKeyEvent(KeyEvent::KEY_UP, keycode[i]);
            }
        }
    }
}

void Win32Window::scroll(int32_t x, int32_t y)
{
    mEventDelegate->fireScrollEvent(x, y);
}

void Win32Window::swap()
{
    SwapBuffers(::GetDC(mWindowHandle));
    ValidateRect(mWindowHandle, NULL);
}

void Win32Window::exitLoop()
{
    PostQuitMessage(0);
}

#define inRange(value, minval, maxval)  ((minval) <= (value) && (value) <= (maxval))

uint32_t Win32Window::convertKey(WPARAM keycode, bool& is_button)
{
    is_button = false;

    // 0-9, a-z
    if (inRange(keycode, 0x30, 0x39) || inRange(keycode, 0x41, 0x5A)) {
        return keycode;
    }

#define keymap(wkey, vk)    case wkey: return vk
#define btnmap(wkey, btn)   case wkey: is_button = true; return btn

    switch (keycode) {
        btnmap(VK_LBUTTON, SG_MOUSE_BUTTON_LEFT);
        btnmap(VK_MBUTTON, SG_MOUSE_BUTTON_MIDDLE);
        btnmap(VK_RBUTTON, SG_MOUSE_BUTTON_RIGHT);

        keymap(VK_BACK, SG_VK_BACKSPACE);
        keymap(VK_TAB, SG_VK_TAB);
        keymap(VK_RETURN, SG_VK_ENTER);
        keymap(VK_ESCAPE, SG_VK_ESCAPE);
        keymap(VK_SPACE, SG_VK_SPACE);

        keymap(VK_LEFT, SG_VK_LEFT);
        keymap(VK_UP, SG_VK_UP);
        keymap(VK_RIGHT, SG_VK_RIGHT);
        keymap(VK_DOWN, SG_VK_DOWN);
        keymap(VK_PRIOR, SG_VK_PAGE_UP);
        keymap(VK_NEXT, SG_VK_PAGE_DOWN);

        keymap(VK_F1, SG_VK_F1);
        keymap(VK_F2, SG_VK_F2);
        keymap(VK_F3, SG_VK_F3);
        keymap(VK_F4, SG_VK_F4);
        keymap(VK_F5, SG_VK_F5);
        keymap(VK_F6, SG_VK_F6);
        keymap(VK_F7, SG_VK_F7);
        keymap(VK_F8, SG_VK_F8);
        keymap(VK_F9, SG_VK_F9);
        keymap(VK_F10, SG_VK_F10);
        keymap(VK_F11, SG_VK_F11);
        keymap(VK_F12, SG_VK_F12);
    }

    return SG_VK_UNKNOWN;
}


#if defined(SG_OPENGL_ES)

EGLWindow::EGLWindow(const Size& window_size)
    : Win32Window(window_size)
    , mEGL(nullptr)
{
}

EGLWindow::~EGLWindow()
{
    destroyContext();
}

bool EGLWindow::initContext()
{
    mEGL = new EGLObject(getWindowHandle());
    return mEGL != nullptr;
}

void EGLWindow::destroyContext()
{
    delete mEGL;
    mEGL = nullptr;
}

void EGLWindow::swap()
{
    mEGL->swap();
    ValidateRect(getWindowHandle(), NULL);
}


#else

WGLWindow::WGLWindow(const Size& window_size)
    : Win32Window(window_size)
    , mGLContext(0)
{
}

WGLWindow::~WGLWindow()
{
    destroyContext();
}

static HGLRC initGL(HWND window)
{
    auto dc = GetDC(window);
    if(!dc) {
        return NULL;
    }

    PIXELFORMATDESCRIPTOR descriptor = {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER,
        PFD_TYPE_RGBA,
        24,
        0, 0, 0, 0, 0, 0,
        0,
        0,
        0,
        0, 0, 0, 0,
        32,
        0,
        0,
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };

    int format = ChoosePixelFormat(dc, &descriptor);
    if(format == 0) {
        return NULL;
    }

    if(!SetPixelFormat(dc, format, &descriptor)) {
        return false;
    }

    return wglCreateContext(dc);
}

bool WGLWindow::initContext()
{
    // init gl context
    auto window = getWindowHandle();
    mGLContext = initGL(window);
    if (mGLContext) {
        if (!wglMakeCurrent(GetDC(window), mGLContext)) {
            destroyContext();
        }
    }

    return NULL != mGLContext;
}

void WGLWindow::destroyContext()
{
    if (mGLContext) {
        wglDeleteContext(mGLContext);
        mGLContext = NULL;
    }
}

#endif

#endif

}
