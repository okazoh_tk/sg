/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "SGString.h"
#include "sg/MemoryAllocator.h"
#include <string.h>

namespace sg {

static const uint32_t FNV_OFFSET_BASIS_32 = 2166136261U;
static const uint32_t FNV_PRIME_32 = 16777619U;
#define MAX_HASH_LENGTH     (32)

static uint32_t fnv_1_hash_32(const uint8_t *bytes, uint32_t& length)
{
    uint32_t hash;
    uint32_t i = 0;
    const uint8_t* p = bytes;

    hash = FNV_OFFSET_BASIS_32;
    while (*p && i < MAX_HASH_LENGTH) {
        hash = (FNV_PRIME_32 * hash) ^ (*p);
        ++i;
        ++p;
    }

    if (*p) {
        i += (uint32_t)strlen((const char*)p);
    }

    length = i;

    return hash;
}

namespace internal {

#define BUFFER_LENGTH   (sizeof(mText.chars)/sizeof(mText.chars[0]))
#define isAllocated()   (!mIsTempBuffer && mBufferLength > BUFFER_LENGTH)
#define isUsingCharsBuffer() (mBufferLength <= BUFFER_LENGTH)
#define MAX_BUFFER_LENGTH   (0x0FFFFFFF)
#define freeBuffer()    if (isAllocated()) { sysFree(mText.ptr); }

void StringData::construct(const char* str, bool to_copy)
{
    mIsTempBuffer = 0;
    mBufferLength = 0;
    mText.chars[0] = '\0';
    mHash = 0;

    if (str) {
        copy(str, to_copy);
    }
}

void StringData::destruct()
{
    freeBuffer();
}

StringData::operator char*()
{
    return isUsingCharsBuffer() ? mText.chars : mText.ptr;
}

StringData::operator const char*() const
{
    return isUsingCharsBuffer() ? mText.chars : mText.ptr;
}

bool StringData::equals(const StringData& rhv) const
{
    return
        mHash == rhv.mHash &&
        0 == strcmp(*this, rhv);
}

bool StringData::copy(const char* str, bool to_copy)
{
    if (str == nullptr) {
        return false;
    }

    uint32_t length;
    auto hash = fnv_1_hash_32((const uint8_t*)(const char*)*this, length);
    uint32_t length_with_null = length + 1;

    if (length_with_null > MAX_BUFFER_LENGTH) {
        length_with_null = MAX_BUFFER_LENGTH;
    }
    
    if (to_copy) {
        if (length_with_null <= BUFFER_LENGTH) {

            freeBuffer();
            strcpy_s(mText.chars, BUFFER_LENGTH, str);
            this->mBufferLength = (uint16_t)length_with_null;
        } else {
            if (length_with_null > this->mBufferLength) {

                // replace buffer with larger one

                auto dest = (char*)sysAlloc(length_with_null);
                if (nullptr == dest) {
                    return false;
                }
                mBufferLength = (uint16_t)length_with_null;

                // free old data if exist
                freeBuffer();

                mText.ptr = dest;
            }
            strcpy_s(mText.ptr, this->mBufferLength, str);
        }
        mIsTempBuffer = 0;
    } else {
        freeBuffer();

        mText.ptr = (char*)str;
        mBufferLength = MAX_BUFFER_LENGTH;
        mIsTempBuffer = 1;
    }

    mHash = hash;

    return true;
}

void StringData::moveFrom(StringData& rhv)
{
    *this = rhv;
    rhv.mIsTempBuffer = 0;
    rhv.mBufferLength = 0;
}

void StringData::clear()
{
    freeBuffer();

    mIsTempBuffer = 0;
    mBufferLength = 0;
    mText.chars[0] = '\0';
}

}

String::String(const char* str)
{
    mData.construct(str);
}

String::String(const char* str, const NoCopy&)
{
    mData.construct(str, false);
}

String::String(const String& rhv)
{
    mData.construct(rhv);
}

String::String(String&& rhv)
{
    mData.moveFrom(rhv.mData);
}

String::String()
{
    mData.destruct();
}

void String::operator=(const char* str)
{
    mData.copy(str);
}

void String::operator=(const String& rhv)
{
    mData.copy(rhv);
}

void String::operator=(String&& rhv)
{
    // destruct first and then move
    mData.destruct();
    mData.moveFrom(rhv.mData);
}

bool String::operator==(const String& rhv) const
{
    return mData.equals(rhv.mData);
}

String::operator const char*() const
{
    return mData;
}

const char* String::c_str() const
{
    return mData;
}

}
