/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Setting.h"
#include "SettingData.h"

namespace sg {

Setting::Setting()
{
}

Setting::~Setting()
{
}

void Setting::init(SettingData** data)
{
    (*data)->init();
    blend.init(data);
    culling.init(data);
    depthTest.init(data);
    dither.init(data);
    polygonOffset.init(data);
    primitiveRestartFixedIndex.init(data);
    sampleAlphaToCoverage.init(data);
    sampleCoverage.init(data);
    scissor.init(data);
    stencil.init(data);

    wireframe.init(data);
    lighing.init(data);
}

Setting::Blend::Blend()
{
}

Setting::Blend::~Blend()
{
}

void Setting::Blend::init(SettingData** data)
{
    mData = data;
}

#define change(exp, dirty)  \
    if ((*mData)->exp) {\
        (*mData)->dirty = 1;\
    }

void Setting::Blend::enable(bool flag)
{
    change(setBlend(flag), dirty_blend);
}

void Setting::Blend::on()
{
    enable(true);
}

void Setting::Blend::off()
{
    enable(false);
}

void Setting::Blend::color(float r, float g, float b, float a)
{
    change(blend.color(r, g, b, a), dirty_blend);
}

void Setting::Blend::color(const Colorf& col)
{
    change(blend.color(col.r, col.g, col.b, col.a), dirty_blend);
}

void Setting::Blend::func(SGBlendFactor src, SGBlendFactor dst)
{
    change(blend.function(src, dst), dirty_blend);
}

void Setting::Blend::func(
    SGBlendFactor srcRGB, SGBlendFactor dstRGB,
    SGBlendFactor srcAlpha, SGBlendFactor dstAlpha)
{
    change(blend.function(srcRGB, dstRGB, srcAlpha, dstAlpha), dirty_blend);
}

void Setting::Blend::equation(SGBlendEquation modeRGB, SGBlendEquation modeAlpha)
{
    change(blend.equation(modeRGB, modeAlpha), dirty_blend);
}


Setting::Culling::Culling()
{
}

Setting::Culling::~Culling()
{
}

void Setting::Culling::init(SettingData** data)
{
    mData = data;
}

void Setting::Culling::enable(bool flag)
{
    change(setCulling(flag), dirty_culling);
}

void Setting::Culling::on()
{
    enable(true);
}

void Setting::Culling::off()
{
    enable(false);
}

void Setting::Culling::face(SGFace f)
{
    change(culling.face(f), dirty_culling);
}

void Setting::Culling::front(SGFrontFace f)
{
    change(culling.front(f), dirty_culling);
}


Setting::DepthTest::DepthTest()
{
}

Setting::DepthTest::~DepthTest()
{
}

void Setting::DepthTest::init(SettingData** data)
{
    mData = data;
}

void Setting::DepthTest::enable(bool flag)
{
    change(setDepthTest(flag), dirty_depth_test);
}

void Setting::DepthTest::on()
{
    enable(true);
}

void Setting::DepthTest::off()
{
    enable(false);
}

void Setting::DepthTest::depthFunc(SGFunction f)
{
    change(depth_test.function(f), dirty_depth_test);
}

void Setting::DepthTest::mask(bool m)
{
    change(depth_test.mask(m), dirty_depth_test);
}

void Setting::DepthTest::range(float n, float f)
{
    change(depth_test.range(n, f), dirty_depth_test);
}


Setting::Dither::Dither()
{
}

Setting::Dither::~Dither()
{
}

void Setting::Dither::init(SettingData** data)
{
    mData = data;
}

void Setting::Dither::enable(bool flag)
{
    change(setDither(flag), dirty_dither);
}

void Setting::Dither::on()
{
    enable(true);
}

void Setting::Dither::off()
{
    enable(false);
}


Setting::PolygonOffset::PolygonOffset()
{
}

Setting::PolygonOffset::~PolygonOffset()
{
}

void Setting::PolygonOffset::init(SettingData** data)
{
    mData = data;
}

void Setting::PolygonOffset::enable(bool flag)
{
    change(setPolygonOffset(flag), dirty_polygon_offset);
}

void Setting::PolygonOffset::on()
{
    enable(true);
}

void Setting::PolygonOffset::off()
{
    enable(false);
}

void Setting::PolygonOffset::set(float factor, float units)
{
    change(polygon_offset.set(factor, units), dirty_polygon_offset);
}


Setting::PrimitiveRestartFixedIndex::PrimitiveRestartFixedIndex()
{
}

Setting::PrimitiveRestartFixedIndex::~PrimitiveRestartFixedIndex()
{
}

void Setting::PrimitiveRestartFixedIndex::init(SettingData** data)
{
    mData = data;
}

void Setting::PrimitiveRestartFixedIndex::enable(bool flag)
{
    change(setPrimitiveRestartFixedIndex(flag), dirty_primitive_restart_fixed_index);
}

void Setting::PrimitiveRestartFixedIndex::on()
{
    enable(true);
}

void Setting::PrimitiveRestartFixedIndex::off()
{
    enable(false);
}


Setting::SampleAlphaToCoverage::SampleAlphaToCoverage()
{
}

Setting::SampleAlphaToCoverage::~SampleAlphaToCoverage()
{
}

void Setting::SampleAlphaToCoverage::init(SettingData** data)
{
    mData = data;
}

void Setting::SampleAlphaToCoverage::enable(bool flag)
{
    change(setSampleAlphaToCoverage(flag), dirty_sample_alpha_to_coverage);
}

void Setting::SampleAlphaToCoverage::on()
{
    enable(true);
}

void Setting::SampleAlphaToCoverage::off()
{
    enable(false);
}


Setting::SampleCoverage::SampleCoverage()
{
}

Setting::SampleCoverage::~SampleCoverage()
{
}

void Setting::SampleCoverage::init(SettingData** data)
{
    mData = data;
}

void Setting::SampleCoverage::enable(bool flag)
{
    change(setSampleCoverage(flag), dirty_sample_coverage);
}

void Setting::SampleCoverage::on()
{
    enable(true);
}

void Setting::SampleCoverage::off()
{
    enable(false);
}


Setting::Scissor::Scissor()
{
}

Setting::Scissor::~Scissor()
{
}

void Setting::Scissor::init(SettingData** data)
{
    mData = data;
}

void Setting::Scissor::enable(bool flag)
{
    change(setScissorTest(flag), dirty_scissor_test);
}

void Setting::Scissor::on()
{
    enable(true);
}

void Setting::Scissor::off()
{
    enable(false);
}

void Setting::Scissor::box(int32_t x, int32_t y, uint32_t width, uint32_t height)
{
    change(scissor.box(x, y, width, height), dirty_scissor_test);
}


Setting::Stencil::Stencil()
{
}

Setting::Stencil::~Stencil()
{
}

void Setting::Stencil::init(SettingData** data)
{
    mData = data;
}

void Setting::Stencil::enable(bool flag)
{
    change(setStencilTest(flag), dirty_stencil_test);
}

void Setting::Stencil::on()
{
    enable(true);
}

void Setting::Stencil::off()
{
    enable(false);
}

void Setting::Stencil::func(SGFunction f, int32_t r, uint32_t m)
{
    change(stencil.func(f, r, m), dirty_stencil_test);
}

void Setting::Stencil::func(SGFace face, SGFunction f, int32_t r, uint32_t m)
{
    change(stencil.func(face, f, r, m), dirty_stencil_test);
}

void Setting::Stencil::op(SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass)
{
    change(stencil.op(sfail, dfail, dppass), dirty_stencil_test);
}

void Setting::Stencil::op(SGFace face, SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass)
{
    change(stencil.op(face, sfail, dfail, dppass), dirty_stencil_test);
}


Setting::WireFrame::WireFrame()
{
}

Setting::WireFrame::~WireFrame()
{
}

void Setting::WireFrame::init(SettingData** data)
{
    mData = data;
}

bool Setting::WireFrame::isOn() const
{
    return (*mData)->renderer.enable_wireframe == 1;
}

void Setting::WireFrame::enable(bool flag)
{
    (*mData)->renderer.enable_wireframe = flag ? 1 : 0;
}

void Setting::WireFrame::on()
{
    enable(true);
}

void Setting::WireFrame::off()
{
    enable(false);
}

void Setting::WireFrame::color(const Colorf& c)
{
    (*mData)->renderer.wire_color = c;
}
const Colorf& Setting::WireFrame::color() const
{
    return (*mData)->renderer.wire_color;
}

Setting::Lighting::Lighting()
{
}

Setting::Lighting::~Lighting()
{
}

void Setting::Lighting::init(SettingData** data)
{
    mData = data;
}

void Setting::Lighting::enable(bool flag)
{
    (*mData)->renderer.enable_lighting = flag ? 1 : 0;
}

bool Setting::Lighting::isOn() const
{
    return (*mData)->renderer.enable_lighting == 1;
}

void Setting::Lighting::on()
{
    enable(true);
}

void Setting::Lighting::off()
{
    enable(false);
}


}
