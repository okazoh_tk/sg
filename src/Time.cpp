/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Time.h"

#if defined(TARGET_OS_WIN)
#include <Windows.h>

static LONGLONG freq = 0;

static sg::Time::time_type getTime()
{
    if (freq == 0) {
        QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
        if (freq == 0) {
            // todo: print error
            freq = 1;
        }
    }

    LONGLONG value;
    QueryPerformanceCounter((LARGE_INTEGER*)&value);

    // return value as microsecond.
    return ((uint64_t)value * 1000000L) / (uint64_t)freq;
}

static void getThreadTimes(sg::ThreadTimes::time_type& kernel_usec, sg::ThreadTimes::time_type& user_usec)
{
    FILETIME cre, ext, kernel, user;
    GetThreadTimes(GetCurrentThread(), &cre, &ext, &kernel, &user);

    kernel_usec = (uint64_t)kernel.dwHighDateTime << 32 | (uint64_t)kernel.dwLowDateTime;
    user_usec = (uint64_t)user.dwHighDateTime << 32 | (uint64_t)user.dwLowDateTime;
}

#else

#if defined(TARGET_OS_MAC)

#include <mach/mach_time.h>

static uint64_t denom = 0;
static uint64_t numer = 0;
static sg::Time::time_type getTime()
{
    if (denom == 0) {
        mach_timebase_info_data_t tb;
        mach_timebase_info(&tb);
        denom = tb.denom * 1000;
        numer = tb.numer;
    }

    uint64_t tick = mach_absolute_time();
    return tick * numer / denom;
}

#else

#include <time.h>

static sg::Time::time_type getTime()
{
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);

    // return value as microsecond.
    return (uint64_t)ts.tv_sec * 1000000L + (uint64_t)ts.tv_nsec / 1000;
}

#endif

#include <sys/resource.h>
#include <sys/time.h>
static void getThreadTimes(sg::ThreadTimes::time_type& kernel_usec, sg::ThreadTimes::time_type& user_usec)
{
    rusage ru;
    getrusage(RUSAGE_SELF, &ru);

    kernel_usec = (uint64_t)ru.ru_stime.tv_sec * 1000000L + ru.ru_stime.tv_usec;
    user_usec = (uint64_t)ru.ru_utime.tv_sec * 1000000L + ru.ru_utime.tv_usec;
}

#endif

namespace sg {

Time::Time()
    : mUsec(0)
{
}

Time::Time(time_type value)
    : mUsec(value)
{
}

Time::Time(const Time& rh)
    : mUsec(rh.mUsec)
{
}

const Time Time::now()
{
    return Time(getTime());
}

void Time::operator=(const Time& rh)
{
    mUsec = rh.mUsec;
}

const Time Time::operator-(const Time& rh) const
{
    return Time(mUsec - rh.mUsec);
}

Time::operator time_type() const
{
    return mUsec;
}



ThreadTimes::ThreadTimes()
    : mKernelUsec(0)
    , mUserUsec(0)
{
}

ThreadTimes::ThreadTimes(time_type kernel_usec, time_type user_usec)
    : mKernelUsec(kernel_usec)
    , mUserUsec(user_usec)
{
}

ThreadTimes::ThreadTimes(const ThreadTimes& rh)
    : mKernelUsec(rh.mKernelUsec)
    , mUserUsec(rh.mUserUsec)
{
}

const ThreadTimes ThreadTimes::now()
{
    time_type kernel_usec, user_usec;
    getThreadTimes(kernel_usec, user_usec);
    return ThreadTimes(kernel_usec, user_usec);
}

void ThreadTimes::operator=(const ThreadTimes& rh)
{
    mKernelUsec = rh.mKernelUsec;
    mUserUsec = rh.mUserUsec;
}

const ThreadTimes ThreadTimes::operator-(const ThreadTimes& rh) const
{
    return ThreadTimes(mKernelUsec - rh.mKernelUsec, mUserUsec - rh.mUserUsec);
}

}
