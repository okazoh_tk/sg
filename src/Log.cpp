/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Log.h"
#include <stdarg.h>

#ifdef TARGET_OS_WIN
#define snprintf _snprintf_s
#include <Windows.h>
#define getpid()    ::GetThreadId(::GetCurrentThread())
#else
#include <sys/types.h> 
#include <unistd.h>
#endif

extern "C" {

static int32_t s_log_level =
#ifdef NDEBUG
    LOG_INFO;
#else
    LOG_DEBUG;
#endif

SG_PUBLIC_API void sg_set_log_level(int32_t level)
{
    s_log_level = level < LOG_ERROR ? LOG_ERROR : level;
}

SG_PUBLIC_API void sg_dprintf(int32_t level, const char* label, const char* fmt, ...)
{
    if (level > s_log_level) {
        return;
    }

    if (level > LOG_VERBOSE) {
        level = LOG_VERBOSE;
    }
    else if (level < LOG_ERROR) {
        level = LOG_ERROR;
    }

    static const char level_ch[] = {
        'E', 'W', 'I', 'D', 'V'
    };

    char newfmt[512];
    snprintf(newfmt, sizeof(newfmt), "%c/%-8s(%5d): %s\n", level_ch[level], label, (uint32_t)getpid(), fmt);

    va_list ap;
    va_start(ap, fmt);

    vprintf(newfmt, ap);

    va_end(ap);
}

}
