/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define LOG_TAG "GL"

#include "sg/Context.h"
#include "sg/MemoryAllocator.h"
#include "sg/Property.h"
#include "sg/Program.h"
#include "sg/Texture.h"
#include "sg/VertexBuffer.h"
#include "OpenGL.h"
#include "GLSLDefaultShaders.h"
#include "SettingData.h"
#include "Utility.h"
#include "sg/MatrixStack.h"
#include "sg/Model.h"
#include "sg/model/AxisModel.h"
#include "sg/model/GridModel.h"
#include "sg/RenderTarget.h"
#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

#include <type_traits>
static_assert(sizeof(GLint) == sizeof(int32_t), "sizeof(GLint) == sizeof(int32_t)");

#define SYSTEM_PREFIX   "system."

namespace sg {

class SG_LOCAL_API ContextImpl {
public:

    ContextImpl(const Size& window_size)
        : mWindowSize(window_size)
        , mCurrentMatrix(nullptr)
        , mRenderTarget(nullptr)
        , mSystemFaces(IndexBuffer::DYNAMIC_DRAW)
    {
        mDefaultMatrix.project(
            Perspective{ 45, window_size.width / (float)window_size.height, 0.1f, 1000 });

        Vector3 eye = { 0, 0, 1.5f };
        Vector3 at = { 0, 0, 0 };
        Vector3 up = { 0, 1, 0 };
        mDefaultMatrix.lookAt(eye, at, up);

        mCurrentSetting = mSettingData.last();

        mAxes.setLine(0.f, 1.5f);
        mGrid.setPlaneZX(10.f, 1.f);
    }

    ~ContextImpl() {
        for (auto iter : mPrograms) {
            if (iter.second.mReleaseOnLostRef) {
                Program* program = iter.second.mProgram;
                iter.second.mProgram = nullptr; // release reference by iter
                if (program) {
                    // ONLY mPrograms HAS A REFERENCE!
                    //ASSERT(!program->hasMultiObservers());
                    delete program;
                }
            }
        }
        mPrograms.clear();
    }

    void pushSetting() {
        auto new_setting = mSettingData.push();
        // clone and clear dirty
        *new_setting = *mCurrentSetting;
        new_setting->clearDirty();

        mCurrentSetting = new_setting;
    }

    void popSetting() {
        auto old_one = mCurrentSetting;
        mSettingData.pop();
        mCurrentSetting = mSettingData.last();
        mCurrentSetting->rollback(*old_one);
    }

    Size getCurrentTargetSize() const
    {
        if (mRenderTarget) {
            return mRenderTarget->getParameter().size;
        } else {
            return this->mWindowSize;
        }
    }

    void begin(MatrixStack* matrix, RenderTarget* target)
    {
        mCurrentMatrix = matrix;
        mRenderTarget = target;
        if (mRenderTarget) {
            mRenderTarget->bind();
        }
    }

    void end()
    {
        if (mRenderTarget) {
            mRenderTarget->unbind();
        }
        mCurrentMatrix = nullptr;
        mRenderTarget = nullptr;
    }

    MatrixStack& getDefaultMatrix()
    {
        return mDefaultMatrix;
    }

    MatrixStack& getCurrentMatrix()
    {
        return mCurrentMatrix ? *mCurrentMatrix : mDefaultMatrix;
    }

    Material& getWireframeMaterial(const Colorf& color) {

        if (!mWireframeMaterial.hasProgram()) {
            mWireframeMaterial.setProgram(findProgram(SYSTEM_PREFIX "solid_fill"));
        }

        mWireframeMaterial.set(
            SG_UNIFORM_COLOR,
            Vector4Property((const Vector4&)color));

        return mWireframeMaterial;
    }


    class SG_LOCAL_API ProgramContainer {
    public:
        ProgramContainer()
        {
        }

        ProgramContainer(Program* new_program, bool release_on_lost_ref)
            : mProgram(new_program)
            , mReleaseOnLostRef(release_on_lost_ref)
        {
        }

        ~ProgramContainer()
        {
            mProgram = nullptr;
        }

        ProgramContainer(const ProgramContainer& rhv)
            : mProgram(rhv.mProgram)
            , mReleaseOnLostRef(rhv.mReleaseOnLostRef)
        {
        }

        void operator=(const ProgramContainer& rhv)
        {
            mProgram = rhv.mProgram;
            mReleaseOnLostRef = rhv.mReleaseOnLostRef;
        }


        Program::WeakRef mProgram;
        bool mReleaseOnLostRef;
    };

    void registProgram(const char* name, Program* new_program, bool release_on_lost_ref)
    {
        // replace program
        mPrograms[name] = ProgramContainer(new_program, release_on_lost_ref);
    }

    Program* findProgram(const char* name)
    {
        if (!name) {
            return nullptr;
        }

        std::string key(name);
        auto iter = mPrograms.find(key);
        if (iter != mPrograms.end()) {
            return iter->second.mProgram;
        }

        if (0 != key.find_first_of(SYSTEM_PREFIX)) {
            return nullptr;
        }

        Program* program = nullptr;

        if (key == SYSTEM_PREFIX "solid_fill") {
            program = new Program();
            program->setShader(solid_fill_vs, solid_fill_fs);
        }
        else if (key == SYSTEM_PREFIX "gradient_fill") {
            program = new Program();
            program->setShader(gradient_fill_vs, gradient_fill_fs);
        }
        else if (key == SYSTEM_PREFIX "mono_texture") {
            program = new Program();
            program->setShader(mono_texture_vs, mono_texture_fs);
        }
        else if (key == SYSTEM_PREFIX "texture_diffuse") {
            program = new Program();
            program->setShader(texture_diffuse_vs, texture_diffuse_fs);
        }

        if (program) {
            registProgram(key.c_str(), program, true);
        }

        return program;
    }

    void addLight(Light* light)
    {
        mLights.push_back(light);
    }

    void removeLight(Light* light)
    {
        auto found = std::find_if(mLights.begin(), mLights.end(), [light](Light::WeakRef& l){
            return l == light;
        });

        if (found != mLights.end()) {
            mLights.erase(found);
        }
    }

    uint32_t getLightNum()
    {
        return mLights.size();
    }

    Light* getLight(uint32_t index)
    {
        return mLights[index];
    }

public:
    SettingData* mCurrentSetting;
    Stack<SettingData> mSettingData;
    Size mWindowSize;
    MatrixStack* mCurrentMatrix;
    MatrixStack mDefaultMatrix;

    RenderTarget* mRenderTarget;

    Mesh mSystemMesh;
    IndexBuffer mSystemFaces;
    Material mWireframeMaterial;

    AxisModel mAxes;
    GridModel mGrid;

    std::unordered_map<std::string, ProgramContainer> mPrograms;

    std::vector<Light::WeakRef> mLights;
};


Context::Context(const Size& window_size)
    : mImpl(new ContextImpl(window_size))
{
    init(&mImpl->mCurrentSetting);
}

Context::~Context()
{
    delete mImpl;
}

void Context::pushSetting()
{
    mImpl->pushSetting();
}

void Context::popSetting()
{
    mImpl->popSetting();
}

Size Context::getCurrentTargetSize() const
{
    return mImpl->getCurrentTargetSize();
}

void Context::clear(uint8_t mask)
{
    GLbitfield newMask = 
        ((mask&SG_COLOR_BUFFER_BIT)?GL_COLOR_BUFFER_BIT:0) |
        ((mask&SG_DEPTH_BUFFER_BIT)?GL_DEPTH_BUFFER_BIT:0) |
        ((mask&SG_STENCIL_BUFFER_BIT)?GL_STENCIL_BUFFER_BIT:0);
    invokeGL(glClear(newMask));
}

void Context::clear(const Color& color, uint8_t mask)
{
    invokeGL(glClearColor(color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f));
    clear(mask);
}

void Context::clear(float r, float g, float b, float a, uint8_t mask)
{
    invokeGL(glClearColor(r, g, b, a));
    clear(mask);
}

void Context::clearColor(float red, float green, float blue, float alpha)
{
    invokeGL(glClearColor(red, green, blue, alpha));
}

void Context::clearDepth(float d)
{
    ON_GL(invokeGL(glClearDepth(d)));
    ON_GLES(invokeGL(glClearDepthf(d)));
}

void Context::clearStencil(int32_t s)
{
    invokeGL(glClearStencil(s));
}

void Context::viewport(const Viewport& v)
{
    viewport(v.x, v.y, v.width, v.height);
}

void Context::viewport(int32_t x, int32_t y, int32_t width, int32_t height)
{
    invokeGL(glViewport(x, y, width, height));
}

MatrixStack& Context::getDefaultMatrix()
{
    return mImpl->getDefaultMatrix();
}

MatrixStack& Context::getCurrentMatrix()
{
    return mImpl->getCurrentMatrix();
}

void Context::begin()
{
    begin(nullptr, nullptr);
}

void Context::begin(MatrixStack* matrix)
{
    begin(matrix, nullptr);
}

void Context::begin(MatrixStack* matrix, RenderTarget* target)
{
    mImpl->begin(matrix, target);
}

void Context::end()
{
    mImpl->end();
}

void Context::drawAxes()
{
    mImpl->mAxes.draw(*this);
}

void Context::drawGrid()
{
    mImpl->mGrid.draw(*this);
}

void Context::draw(Mesh& mesh, Material& material)
{
    if (wireframe.isOn()) {
        auto prim = mesh.getPrimitiveType();
        mesh.setPrimitiveType(SG_LINES);
        mesh.drawFaces(*this, mImpl->getWireframeMaterial(wireframe.color()));
        mesh.setPrimitiveType(prim);
    } else {

        // update material's properties.
        material.set(SG_UNIFORM_MVP_MATRIX, MatrixStackProperty(&getCurrentMatrix()));

        if (material.update(*this)) {

            // update mesh's values and draw.
            mesh.update(*this);
            mesh.drawFaces(*this, material);

            material.restore(*this);
        }
    }
}

void Context::draw(Model& model)
{
    model.draw(*this);
}

void Context::place(
    Model& model,
    const Vector3& translate,
    const Vector3& scale,
    const Quaternion& rotate)
{
    auto& mat = getCurrentMatrix();
    mat.push();

    mat.translate(translate);
    mat.rotate(rotate);
    mat.scale(scale);

    model.draw(*this);

    mat.pop();
}

void Context::registProgram(const char* name, Program* new_program, bool release_on_lost_ref)
{
    mImpl->registProgram(name, new_program, release_on_lost_ref);
}

Program* Context::findProgram(const char* name)
{
    return mImpl->findProgram(name);
}

void Context::addLight(Light* light)
{
    if (light) {
        mImpl->addLight(light);
    }
}

void Context::removeLight(Light* light)
{
    if (light) {
        mImpl->removeLight(light);
    }
}

uint32_t Context::getLightNum()
{
    return lighing.isOn() ? mImpl->getLightNum() : 0;
}

Light* Context::getLight(uint32_t index)
{
    return lighing.isOn() ? mImpl->getLight(index) : nullptr;
}


#if 0

void Context::getFloatv(GLenum pname, float *data)
{
    _check(glGetFloatv(pname, data));
}

void Context::getIntegerv(GLenum pname, GLint *data)
{
    _check(glGetIntegerv(pname, data));
}

void Context::getBooleanv(GLenum pname, GLboolean *data)
{
    _check(glGetBooleanv(pname, data));
}

const GLubyte* Context::getString(GLenum name)
{
    return glGetString(name);
}

GLenum Context::getError(void)
{
    return glGetError();
}

void Context::colorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)
{
    _check(glColorMask(red, green, blue, alpha));
}

void Context::hint(GLenum target, GLenum mode)
{
    _check(glHint(target, mode));
}

void Context::readPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels)
{
    _check(glReadPixels(x, y, width, height, format, type, pixels));
}

void Context::sampleCoverage(float value, GLboolean invert)
{
    _check(glSampleCoverage(value, invert));
}
#endif

}
