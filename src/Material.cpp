/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Material.h"
#include "OpenGL.h"
#include "sg/Program.h"
#include "sg/Context.h"
#include <string>
#include <unordered_map>
#include <vector>

namespace sg {

class SG_LOCAL_API MaterialImpl {
public:
    MaterialImpl() : program(nullptr), texture_unit_count(0) {
    }

    ~MaterialImpl() {
        for (auto prop : own_properties) {
            delete prop;
        }
    }

    Property* cloneFrom(const Property& src) {
        auto prop = src.clone();
        own_properties.push_back(prop);
        return prop;
    }

    Program::WeakRef program;
    std::vector<Property*> own_properties;
    std::unordered_map<std::string, Property*> properties;
    GLint texture_unit_count;
};

Material::Material()
    : mImpl(new MaterialImpl())
{
}

Material::~Material()
{
    delete mImpl;
}

void Material::setProgram(Program* program)
{
    mImpl->program = program;
}

Program* Material::getProgram()
{
    return mImpl->program;
}

bool Material::hasProgram() const
{
    return mImpl->program != nullptr;
}

void Material::set(const char* key, const Property& prop)
{
    if (mImpl->properties.find(key) != mImpl->properties.end()) {
        // copy value
        mImpl->properties[key]->copyFrom(prop);
    } else {
        mImpl->properties[key] = mImpl->cloneFrom(prop);
    }
}

void Material::remove(const char* key)
{
    auto iter = mImpl->properties.find(key);
    if (iter != mImpl->properties.end()) {
        mImpl->properties.erase(iter);
    }
}

void Material::reserve(const char* key)
{
    if (mImpl->properties.find(key) == mImpl->properties.end()) {
        // reserve key
        mImpl->properties[key] = nullptr;
    }
}

bool Material::hasKey(const char* key) const
{
    return mImpl->properties.find(key) != mImpl->properties.end();
}

void Material::printProperties()
{
    for (auto iter : mImpl->properties) {
        if (iter.second) {
            iter.second->print(iter.first.c_str());
        }
    }
}

Property* Material::findProperty(const char* /*key*/)
{
    return nullptr;
}

bool Material::update(Context& /*gl*/)
{
    return true;
}

void Material::restore(Context& /*gl*/)
{
}

void Material::copyProperties(Context& gl)
{
    mImpl->program->prepare();
    auto program = mImpl->program->getName();
    invokeGL(glUseProgram(program));

    // set uniforms
    for (auto& iter : mImpl->properties) {
        auto prop = iter.second;

        if (nullptr == prop) {
            prop = findProperty(iter.first.c_str());
        }

        if (prop) {
            auto loc = glGetUniformLocation(program, iter.first.c_str());
            if (loc < 0) {
                continue;
            }

            setUniform(loc, *prop);
        }
    }

    // todo: setup light uniforms
    uint32_t MAX_LIGHTS = gl.getLightNum();
    for (uint32_t i = 0; i < MAX_LIGHTS; ++i) {
        auto light = gl.getLight(i);
        light->copyUniforms(*mImpl->program, i);
    }
}

void Material::cleanupAfterDraw()
{
    for (GLint i = 0; i < mImpl->texture_unit_count; ++i) {
        invokeGL(glActiveTexture(GL_TEXTURE0 + i));
        invokeGL(glBindTexture(GL_TEXTURE_2D, 0));
    }
    mImpl->texture_unit_count = 0;
}

void Material::setUniform(GLint location, const Property& prop)
{
    switch (prop.getType()) {
    case TYPE_INT:
        invokeGL(glUniform1iv(location, prop.getCount(), (const GLint*)prop.getPointer()));
        break;
    case TYPE_IVEC2:
        invokeGL(glUniform2iv(location, prop.getCount(), (const GLint*)prop.getPointer()));
        break;
    case TYPE_IVEC3:
        invokeGL(glUniform3iv(location, prop.getCount(), (const GLint*)prop.getPointer()));
        break;
    case TYPE_IVEC4:
        invokeGL(glUniform4iv(location, prop.getCount(), (const GLint*)prop.getPointer()));
        break;
    case TYPE_FLOAT:
        invokeGL(glUniform1fv(location, prop.getCount(), (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_VEC2:
        invokeGL(glUniform2fv(location, prop.getCount(), (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_VEC3:
        invokeGL(glUniform3fv(location, prop.getCount(), (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_VEC4:
        invokeGL(glUniform4fv(location, prop.getCount(), (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_MAT2:
        invokeGL(glUniformMatrix2fv(location, prop.getCount(), GL_FALSE, (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_MAT3:
        invokeGL(glUniformMatrix3fv(location, prop.getCount(), GL_FALSE, (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_MAT4:
        invokeGL(glUniformMatrix4fv(location, prop.getCount(), GL_FALSE, (const GLfloat*)prop.getPointer()));
        break;
    case TYPE_MATRIX_STACK: // TYPE_MVP_MATRIX
        {
            auto stack = ((const MatrixStackProperty&)prop).get();
            if (stack) {
                // copy Model-View-Projection Matrix
                Matrix4 mat;
                stack->getMVPMatrix(mat);
                invokeGL(glUniformMatrix4fv(location, prop.getCount(), GL_FALSE, mat.m));
            }
        }
        break;
    case TYPE_MV_MATRIX:
        {
            auto stack = ((const MatrixStackProperty&)prop).get();
            if (stack) {
                // copy Model-View Matrix
                Matrix4 mat;
                stack->getModelViewMatrix(mat);
                invokeGL(glUniformMatrix4fv(location, prop.getCount(), GL_FALSE, mat.m));
            }
        }
        break;
    case TYPE_M_MATRIX:
        {
            auto stack = ((const MatrixStackProperty&)prop).get();
            if (stack) {
                // copy Model Matrix
                Matrix4 mat;
                stack->getModelMatrix(mat);
                invokeGL(glUniformMatrix4fv(location, prop.getCount(), GL_FALSE, mat.m));
            }
        }
        break;
    case TYPE_TEXTURE:
        {
            auto texture = ((const TextureProperty&)prop).get();
            if (texture) {
                invokeGL(glActiveTexture(GL_TEXTURE0 + mImpl->texture_unit_count));
                texture->bind();
                invokeGL(glUniform1i(location, mImpl->texture_unit_count));
                ++mImpl->texture_unit_count;
            }
        }
        break;

    case TYPE_BOOL:
    case TYPE_BVEC2:
    case TYPE_BVEC3:
    case TYPE_BVEC4:
    default:
        break;
    }
}

}
