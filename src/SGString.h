/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_STRING_H_INCLUDED_
#define SG_STRING_H_INCLUDED_

#include "sg/Config.h"

namespace sg {

namespace internal {

/** POD style string structure.
 *
 * @internal
 */
struct SG_LOCAL_API StringData {

    void construct(const char* str = nullptr, bool to_copy = true);
    void destruct();

    operator char*();
    operator const char*() const;

    bool equals(const StringData& rhv) const;
    bool copy(const char* str, bool to_copy = true);
    void moveFrom(StringData& rhv);
    void clear();

    uint32_t mIsTempBuffer:1;
    uint32_t mBufferLength:31;
    uint32_t mHash;
    union {
        // align StringData as 64byte.
        char chars[64 - (sizeof(uint16_t) + sizeof(uint16_t) + sizeof(uint32_t))];
        char* ptr;
    } mText;
};

}

class SG_LOCAL_API String {
public:
    struct NoCopy{};

    String(const char* str);
    String(const char* str, const NoCopy&);
    String(const String& rhv);
    String(String&& rhv);
    String();

    void operator=(const char* str);
    void operator=(const String& rhv);
    void operator=(String&& rhv);

    bool operator==(const String& rhv) const;
    operator const char*() const;
    const char* c_str() const;

private:
    internal::StringData mData;
};

}

#endif
