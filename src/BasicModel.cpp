/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/model/BasicModel.h"
#include "sg/Context.h"

namespace sg {

BasicModel::BasicModel()
    : BasicModel(Buffer::STATIC_DRAW, Buffer::STATIC_DRAW)
{
}

BasicModel::BasicModel(Buffer::Usage vbuffer_usage, Buffer::Usage ibuffer_usage)
    : mVertices(vbuffer_usage)
    , mIndices(ibuffer_usage)
    , mPrepared(false)
    , mScale(Vector3::make(1, 1, 1))
    , mRotate(Quaternion::make(1, 0, 0, 0))
    , mTranslate(Vector3::make(0, 0, 0))
{
    mMesh.setFaces(&mIndices);
    mMesh.setVertices(&mVertices);
}

BasicModel::~BasicModel()
{
}

Mesh& BasicModel::getMesh()
{
    return mMesh;
}

Material& BasicModel::getMaterial()
{
    return mMaterial;
}

VertexBuffer& BasicModel::getVertexBuffer()
{
    return mVertices;
}

IndexBuffer& BasicModel::getIndexBuffer()
{
    return mIndices;
}

void BasicModel::setTranslation(const Vector3& trans)
{
    mTranslate = trans;
}

const Vector3& BasicModel::getTranslation() const
{
    return mTranslate;
}

void BasicModel::setRotation(const Quaternion& quat)
{
    mRotate = quat;
}

void BasicModel::setRotation(float angle, const Vector3& coord)
{
    mRotate = Quaternion::rotate(angle, coord.x, coord.y, coord.z);
}

const Quaternion& BasicModel::getRotation() const
{
    return mRotate;
}

void BasicModel::setScaling(const Vector3& scale)
{
    mScale = scale;
}

const Vector3& BasicModel::getScaling() const
{
    return mScale;
}

void BasicModel::draw(Context& gl)
{
    if (!mPrepared) {
        mPrepared = prepare(gl);
    }

    if (mPrepared) {

        auto& matrix = gl.getCurrentMatrix();

        matrix.push();

        matrix.translate(mTranslate);
        matrix.rotate(mRotate);
        matrix.scale(mScale);

        gl.draw(mMesh, mMaterial);

        matrix.pop();
    }
}

bool BasicModel::prepare(Context& /*gl*/)
{
    return false;
}

bool BasicModel::setProgramIfEmpty(Context& gl, const char* program_name)
{
    if (!mMaterial.hasProgram()) {
        mMaterial.setProgram(gl.findProgram(program_name));
    }

    return mMaterial.hasProgram();
}

void BasicModel::needPrepare()
{
    mPrepared = false;
}

void BasicModel::setProperty(const char* key, const Property& prop)
{
    mMaterial.set(key, prop);
}

void BasicModel::setProperty(const char* key, const Colorf& color)
{
    setProperty(key, Vector4Property((const Vector4&)color));
}

void BasicModel::setProperty(const char* key, Texture* texture)
{
    if (!texture) {
        mMaterial.remove(SG_UNIFORM_TEXTURE);
    }
    else {
        mMaterial.set(key, TextureProperty(texture));
    }
}

PrimitiveType BasicModel::getPrimitiveType() const
{
    return mMesh.getPrimitiveType();
}

void BasicModel::setPrimitiveType(PrimitiveType type)
{
    mMesh.setPrimitiveType(type);
}

void BasicModel::setIndices(const uint16_t* indices, uint32_t indices_num)
{
    mIndices.set(indices, indices_num);
}

}
