/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Application.h"
#include "sg/EventLooper.h"
#include "sg/DebugView.h"
#include "sg/CameraActor.h"

#if defined(_CRTDBG_MAP_ALLOC)
#define activeMemoryLeak()  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF)
#define reportMemoryLeak()  _CrtDumpMemoryLeaks()
#endif
#ifndef activeMemoryLeak
#define activeMemoryLeak()
#endif
#ifndef reportMemoryLeak
#define reportMemoryLeak()
#endif

#define HIGHEST_PRIORITY    (-1000)
#define DEFAULT_PRIORITY    (0)
#define LOWEST_PRIORITY     (10000)

namespace sg {

class SG_LOCAL_API ApplicationImpl
    : public SystemEventListener
    , public RendererEventListener
    , public UIEventListener
    , public KeyEventListener
    , public ButtonEventListener
    , public PointerEventListener
{
public:

    ApplicationImpl(Application* thiz, const Size& window_size, int /*argc*/, char* /*argv*/[])
        : mApp(thiz)
        , mEventDriver(window_size)
        , mDebugView(nullptr)
    {
        mEventDriver.addEventListener((SystemEventListener*)this, DEFAULT_PRIORITY);
    }

    virtual ~ApplicationImpl()
    {
        // todo: remove all listeners

        if (mDebugView) {
            delete mDebugView;
        }
    }

    virtual void onLaunching(EventDriver& /*caller*/)
    {
        // setup all listeners
        mEventDriver.addEventListener((RendererEventListener*)this, DEFAULT_PRIORITY);
        mEventDriver.addEventListener((UIEventListener*)this, DEFAULT_PRIORITY);
        mEventDriver.addEventListener((KeyEventListener*)this, DEFAULT_PRIORITY);
        mEventDriver.addEventListener((ButtonEventListener*)this, DEFAULT_PRIORITY);
        mEventDriver.addEventListener((PointerEventListener*)this, DEFAULT_PRIORITY);
    }

    virtual void onLaunched(EventDriver& /*caller*/)
    {
        mApp->setup();

        mDebugView = new DebugView();
        mEventDriver.addEventListener(mDebugView, LOWEST_PRIORITY);
    }

    virtual void onUpdate(EventDriver& /*caller*/)
    {
        mApp->update();
    }

    virtual void onDraw(EventDriver& /*caller*/, Context& gl)
    {
        mApp->draw(gl);
    }

    virtual void onKeyEvent(EventDriver& /*caller*/, const KeyEvent& e)
    {
        switch (e.getType()) {
        case KeyEvent::KEY_DOWN:
            mApp->onKeyDown(e);
            break;
        case KeyEvent::KEY_UP:
            mApp->onKeyUp(e);
            break;
        }
    }

    virtual void onButtonEvent(EventDriver& /*caller*/, const ButtonEvent& e)
    {
        switch (e.getType()) {
        case ButtonEvent::BUTTON_DOWN:
            mApp->onButtonDown(e);
            break;
        case ButtonEvent::BUTTON_UP:
            mApp->onButtonUp(e);
            break;
        }
    }

    virtual void onPointerEvent(EventDriver& /*caller*/, const PointerEvent& e)
    {
        switch (e.getType()) {
        case PointerEvent::POINTER_DOWN:
            mApp->onPointerDown(e);
            break;
        case PointerEvent::POINTER_UP:
            mApp->onPointerUp(e);
            break;
        case PointerEvent::POINTER_MOVE:
            mApp->onPointerMove(e);
            break;
        }
    }

    virtual void onScroll(EventDriver& /*caller*/, const ScrollEvent& e)
    {
        mApp->onScroll(e);
    }

    int32_t run()
    {
        if (!mEventDriver.setup()) {
            return -1;
        }

        while (mEventDriver.handleEvents()) {
            ;
        }

        mEventDriver.terminate();

        return 0;
    }

    void exit()
    {
        mEventDriver.exitLoop();
    }

    float getFps() const
    {
        return mEventDriver.getFps();
    }

    void setBuilder(Builder* builder)
    {
        mEventDriver.setBuilder(builder);
    }

    void addActor(CameraActor* actor)
    {
        actor->regist(mEventDriver, HIGHEST_PRIORITY);
    }

    void addEventListener(KeyEventListener* listener, int32_t priority)
    {
        mEventDriver.addEventListener(listener, priority);
    }

    void addEventListener(ButtonEventListener* listener, int32_t priority)
    {
        mEventDriver.addEventListener(listener, priority);
    }

    void addEventListener(PointerEventListener* listener, int32_t priority)
    {
        mEventDriver.addEventListener(listener, priority);
    }

    void addEventListener(UIEventListener* listener, int32_t priority)
    {
        mEventDriver.addEventListener(listener, priority);
    }

    void addEventListener(RendererEventListener* listener, int32_t priority)
    {
        mEventDriver.addEventListener(listener, priority);
    }

    void addEventListener(SystemEventListener* listener, int32_t priority)
    {
        mEventDriver.addEventListener(listener, priority);
    }

private:
    Application* mApp;
    EventLooper mEventDriver;
    DebugView* mDebugView;
};


Application::Application()
    : Application(0, nullptr)
{
}

Application::Application(const Size& window_size)
    : Application(window_size, 0, nullptr)
{
}

Application::Application(int argc, char* argv[])
    : Application(Size{ 640, 480 }, argc, argv)
{
}

Application::Application(const Size& window_size, int argc, char* argv[])
{
#if defined(_CRTDBG_MAP_ALLOC)
    //_CrtSetBreakAlloc(266);
#endif
    activeMemoryLeak();

    mImpl = new ApplicationImpl(this, window_size, argc, argv);
}

Application::~Application()
{
    //int* xxx = new int();
    delete mImpl;
    reportMemoryLeak();
}

void Application::setBuilder(Builder* builder)
{
    mImpl->setBuilder(builder);
}

void Application::addActor(CameraActor* actor)
{
    mImpl->addActor(actor);
}

int32_t Application::run()
{
    return mImpl->run();
}

void Application::exit()
{
    mImpl->exit();
}

float Application::getFps() const
{
    return mImpl->getFps();
}

void Application::addEventListener(KeyEventListener* listener, int32_t priority)
{
    mImpl->addEventListener(listener, priority);
}

void Application::addEventListener(ButtonEventListener* listener, int32_t priority)
{
    mImpl->addEventListener(listener, priority);
}

void Application::addEventListener(PointerEventListener* listener, int32_t priority)
{
    mImpl->addEventListener(listener, priority);
}

void Application::addEventListener(UIEventListener* listener, int32_t priority)
{
    mImpl->addEventListener(listener, priority);
}

void Application::addEventListener(RendererEventListener* listener, int32_t priority)
{
    mImpl->addEventListener(listener, priority);
}

void Application::addEventListener(SystemEventListener* listener, int32_t priority)
{
    mImpl->addEventListener(listener, priority);
}

void Application::onKeyDown(const KeyEvent&)
{
}

void Application::onKeyUp(const KeyEvent&)
{
}

void Application::onButtonDown(const ButtonEvent&)
{
}

void Application::onButtonUp(const ButtonEvent&)
{
}

void Application::onPointerMove(const PointerEvent&)
{
}

void Application::onPointerDown(const PointerEvent&)
{
}

void Application::onPointerUp(const PointerEvent&)
{
}

void Application::onScroll(const ScrollEvent&)
{
}

}
