/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/model/BoxModel.h"
#include "sg/Context.h"

namespace sg {

BoxModel::BoxModel()
    : BoxModel(Buffer::STATIC_DRAW, Buffer::STATIC_DRAW)
{
    // delegate PlaneModel(usage, usage)
}

BoxModel::BoxModel(Buffer::Usage vbuffer_usage, Buffer::Usage ibuffer_usage)
    : BasicModel(vbuffer_usage, ibuffer_usage)
    , mSolidFill(false)
{
    setPrimitiveType(SG_TRIANGLE_STRIP);

    // make index buffer.
    uint16_t indices[] = {
        0, 1, 2, 3, 4, 5, 6, 7,
        7, 2, // degenerate
        2, 4, 0, 6, 1, 7, 3, 5
    };

    getIndexBuffer().set(indices, COUNT_OF(indices));
}

BoxModel::~BoxModel()
{
}

void BoxModel::makeVertices(const Vector3& he, Vector3 positions[8])
{
    positions[0] = { -he.x,  he.y,  he.z };
    positions[1] = { -he.x, -he.y,  he.z };
    positions[2] = {  he.x,  he.y,  he.z };
    positions[3] = {  he.x, -he.y,  he.z };
    positions[4] = {  he.x,  he.y, -he.z };
    positions[5] = {  he.x, -he.y, -he.z };
    positions[6] = { -he.x,  he.y, -he.z };
    positions[7] = { -he.x, -he.y, -he.z };
}

void BoxModel::makeNormals(Vector3 normals[8])
{
    normals[0] = Vector3::makeNormalized(-1,  1,  1);
    normals[1] = Vector3::makeNormalized(-1, -1,  1);
    normals[2] = Vector3::makeNormalized( 1,  1,  1);
    normals[3] = Vector3::makeNormalized( 1, -1,  1);
    normals[4] = Vector3::makeNormalized( 1,  1, -1);
    normals[5] = Vector3::makeNormalized( 1, -1, -1);
    normals[6] = Vector3::makeNormalized(-1,  1, -1);
    normals[7] = Vector3::makeNormalized(-1, -1, -1);
}


void BoxModel::setVertices(const Vector3& he, const Colorf& solid_color)
{
    if (false == mSolidFill) {
        mSolidFill = true;
        needPrepare();
    }

    setProperty(SG_UNIFORM_COLOR, solid_color);
    
    Vector3 vertices[8];
    makeVertices(he, vertices);

    Vector3 normals[8];
    makeNormals(normals);

    getVertexBuffer().set(vertices, normals, COUNT_OF(vertices));
}

void BoxModel::setVertices(
    const Vector3& he,
    const Colorf* vertex_colors_8)
{
    if (false != mSolidFill) {
        mSolidFill = false;
        needPrepare();
    }

    Vector3 vertices[8];
    makeVertices(he, vertices);

    Vector3 normals[8];
    makeNormals(normals);

    getVertexBuffer().set(vertices, vertex_colors_8, normals, COUNT_OF(vertices));
}

bool BoxModel::prepare(Context& gl)
{
    // setup material
    auto& material = getMaterial();
    if (!material.hasProgram()) {
        material.setProgram(
            gl.findProgram(mSolidFill ? "system.solid_fill" : "system.gradient_fill"));
    }

    return true;
}

}
