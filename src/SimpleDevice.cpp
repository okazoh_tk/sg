/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
#include "SimpleDevice.h"

namespace sg {

SimpleDevice::SimpleDevice()
{
}

SimpleDevice::~SimpleDevice() {
}


SimpleDevice::PointerDevice::PointerDevice()
{
    for (int i = 0; i < COUNT_OF(pointers); ++i) {
        pointers[i].id = i;
        pointers[i].x = pointers[i].y = 0;
    }
}

SimpleDevice::PointerDevice::~PointerDevice() {
}

bool SimpleDevice::PointerDevice::validate(int32_t id) const
{
    return id < COUNT_OF(pointers);
}

const PointerState* SimpleDevice::PointerDevice::getState(int32_t id) const
{
    if (validate(id)) {
        return &pointers[id];
    }
    else {
        return nullptr;
    }
}

void SimpleDevice::PointerDevice::move(int32_t id, int32_t x, int32_t y)
{
    if (validate(id)) {
        pointers[id].x = x;
        pointers[id].y = y;
    }
}


SimpleDevice::ButtonDevice::ButtonDevice()
{
    for (int32_t i = 0; i < COUNT_OF(buttons); ++i) {
        buttons[i].id = i;
        buttons[i].pressed = false;
    }
}

SimpleDevice::ButtonDevice::~ButtonDevice()
{
}

bool SimpleDevice::ButtonDevice::validate(int32_t id) const
{
    return id < COUNT_OF(buttons);
}

const ButtonState* SimpleDevice::ButtonDevice::getState(int32_t id) const
{
    if (validate(id)) {
        return &buttons[id];
    }
    else {
        return nullptr;
    }
}

void SimpleDevice::ButtonDevice::press(int32_t ch)
{
    if (validate(ch)) {
        buttons[ch].pressed = true;
    }
}
void SimpleDevice::ButtonDevice::release(int32_t ch)
{
    if (validate(ch)) {
        buttons[ch].pressed = false;
    }
}


SimpleDevice::KeyDevice::KeyDevice()
{
    for (int32_t i = 0; i < COUNT_OF(keys); ++i) {
        keys[i].ch = i;
        keys[i].pressed = false;
    }
}

SimpleDevice::KeyDevice::~KeyDevice()
{
}

bool SimpleDevice::KeyDevice::validate(int32_t ch) const
{
    return ch < COUNT_OF(keys);
}

const KeyState* SimpleDevice::KeyDevice::getState(int32_t ch) const
{
    if (validate(ch)) {
        return &keys[ch];
    }
    else {
        return nullptr;
    }
}

void SimpleDevice::KeyDevice::press(int32_t ch)
{
    if (validate(ch)) {
        keys[ch].pressed = true;
    }
}
void SimpleDevice::KeyDevice::release(int32_t ch)
{
    if (validate(ch)) {
        keys[ch].pressed = false;
    }
}


SimpleDevice::PointerDevice& SimpleDevice::pointer()
{
    return mPointer;
}

SimpleDevice::ButtonDevice& SimpleDevice::button()
{
    return mButton;
}

SimpleDevice::KeyDevice& SimpleDevice::key()
{
    return mKey;
}

IPointer* SimpleDevice::getPointerInterface()
{
    return &mPointer;
}

IButton* SimpleDevice::getButtonInterface()
{
    return &mButton;
}

IKey* SimpleDevice::getKeyInterface()
{
    return &mKey;
}

}
