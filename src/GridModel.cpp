/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/model/GridModel.h"
#include <math.h>

namespace sg {

GridModel::GridModel()
    : GridModel(Buffer::STATIC_DRAW, Buffer::STATIC_DRAW)
{
    // delegate GridModel(usage, usage)
}

GridModel::GridModel(Buffer::Usage vbuffer_usage, Buffer::Usage ibuffer_usage)
    : WireModel(vbuffer_usage, ibuffer_usage)
{
}

GridModel::~GridModel()
{
}

enum Plane {
    // index: x = 0, y = 1, z = 2
    // zero component axis index, axis index 1, axis index 2 
    PlaneYZ = 0x012,
    PlaneZX = 0x120,
    PlaneXY = 0x201,
};

void GridModel::makePlane(float start, float extent, float cell_size, int plane)
{
    if (!(start < extent && cell_size > 0)) {
        LOGW("Grid constrains start < extent && cell_size > 0.");
        return;
    }

    uint32_t lines = (uint32_t)ceil((extent - start) / cell_size) + 1;
    getVertexBuffer().reserve(VertexType::LAYOUT, lines * 4);

    int zero_component = (plane >> 8) & 0xf;
    int a = (plane >> 4) & 0xf;
    int b = (plane)& 0xf;

    VertexType vert;
    float* v = (float*)&vert.position;

    v[zero_component] = 0;
    int32_t i = 0;
    float s;
    while ((s = (start + cell_size * i)) <= extent) {
        v[a] = s;
        v[b] = start;
        setVertex(i * 4, vert);

        v[b] = extent;
        setVertex(i * 4 + 1, vert);

        v[b] = s;
        v[a] = start;
        setVertex(i * 4 + 2, vert);

        v[a] = extent;
        setVertex(i * 4 + 3, vert);

        ++i;
    }
}

void GridModel::setPlaneYZ(float extent, float cell_size)
{
    setPlaneYZ(-extent, extent, cell_size);
}

void GridModel::setPlaneYZ(float start, float extent, float cell_size)
{
    makePlane(start, extent, cell_size, PlaneYZ);

    // Red
    Colorf color = { 1, 0, 0, 1 };
    setColor(color);
}

void GridModel::setPlaneZX(float extent, float cell_size)
{
    setPlaneZX(-extent, extent, cell_size);
}

void GridModel::setPlaneZX(float start, float extent, float cell_size)
{
    makePlane(start, extent, cell_size, PlaneZX);

    // Green
    Colorf color = { 0, 1, 0, 1 };
    setColor(color);
}

void GridModel::setPlaneXY(float extent, float cell_size)
{
    setPlaneXY(-extent, extent, cell_size);
}

void GridModel::setPlaneXY(float start, float extent, float cell_size)
{
    makePlane(start, extent, cell_size, PlaneXY);

    // Blue
    Colorf color = { 0, 0, 1, 1 };
    setColor(color);
}

}
