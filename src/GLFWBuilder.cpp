/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "OpenGL.h"
#include "./GLFWBuilder.h"
#include "./GLFWWindow.h"
#include "./GLEWContext.h"

namespace sg {

#if ENABLE_GLFW != 0

GLFWBuilder* GLFWBuilder::create()
{
    return new GLFWBuilder();
}

GLFWBuilder::GLFWBuilder()
    : mWindow(nullptr)
    , mContext(nullptr)
    , mDevice(nullptr)
{
}

GLFWBuilder::~GLFWBuilder()
{
    if (mContext) {
        delete mContext;
        mContext = nullptr;
    }

    if (mWindow) {
        delete mWindow;
        mWindow = nullptr;
    }
}

bool GLFWBuilder::init(const Size& windowSize)
{
    auto window = new GLFWWindow(windowSize);

    if (!window->initWindow()) {
        delete window;
    } else {
//        mDevice = new Device(window->getDevice());
        mWindow = window;
        mDevice = window->getDeviceInterface();
    }

    return mWindow != nullptr;
}

void GLFWBuilder::release()
{
    delete this;
}

Window* GLFWBuilder::getWindow()
{
    return mWindow;
}

Context* GLFWBuilder::createContext()
{
    if (nullptr == mContext) {
        mContext = new GLEWContext(mWindow->getSize());
    }

    return mContext;
}

void GLFWBuilder::destroyContext(Context* context)
{
    delete context;
    mContext = nullptr;
}

IDevice* GLFWBuilder::getDeviceInterface()
{
    return mDevice;
}

#endif

}
