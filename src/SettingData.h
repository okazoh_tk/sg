/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_SETTING_DATA_H_INCLUDED_
#define SG_SETTING_DATA_H_INCLUDED_

#include "sg/Config.h"
#include "sg/Context.h"

namespace sg {

/*
 * ref: https://www.khronos.org/opengles/sdk/docs/man3/html/glEnable.xhtml
 */
struct SG_LOCAL_API SettingData {

    void init();
    void clearDirty();
    void rollback(const SettingData& dirties);

    uint32_t enable_blend : 1; // initial(false)
    uint32_t dirty_blend : 1;
    uint32_t enable_culling : 1; // initial(false)
    uint32_t dirty_culling : 1;
    uint32_t enable_depth_test : 1; // initial(false)
    uint32_t dirty_depth_test : 1;
    uint32_t enable_dither : 1; // initial(true)
    uint32_t dirty_dither : 1;
    uint32_t enable_polygon_offset : 1; // initial(false)
    uint32_t dirty_polygon_offset : 1;
    uint32_t enable_primitive_restart_fixed_index : 1; // initial(false)
    uint32_t dirty_primitive_restart_fixed_index : 1;
    uint32_t enable_rasterizer_discard : 1; // initial(false)
    uint32_t dirty_rasterizer_discard : 1;
    uint32_t enable_sample_alpha_to_coverage : 1; // initial(false)
    uint32_t dirty_sample_alpha_to_coverage : 1;
    uint32_t enable_sample_coverage : 1; // initial(false)
    uint32_t dirty_sample_coverage : 1;
    uint32_t enable_scissor_test : 1; // initial(false)
    uint32_t dirty_scissor_test : 1;
    uint32_t enable_stencil_test : 1; // initial(false)
    uint32_t dirty_stencil_test : 1;

    bool setBlend(bool enable);
    bool setCulling(bool enable);
    bool setDepthTest(bool enable);
    bool setDither(bool enable);
    bool setPolygonOffset(bool enable);
    bool setPrimitiveRestartFixedIndex(bool enable);
    bool setRasterizerDiscard(bool enable);
    bool setSampleAlphaToCoverage(bool enable);
    bool setSampleCoverage(bool enable);
    bool setScissorTest(bool enable);
    bool setStencilTest(bool enable);

    // blend
    struct SG_LOCAL_API Blend {
        float red, green, blue, alpha;  // initial(0, 0, 0, 0)
        uint32_t sfactor_rgb : 4;   // initial(one)
        uint32_t dfactor_rgb : 4;   // initial(zero)
        uint32_t sfactor_a : 4;     // initial(one)
        uint32_t dfactor_a : 4;     // initial(zero)
        uint32_t equation_rgb : 3;  // initial(add)
        uint32_t equation_a : 3;    // initial(add)

        void init();

        bool equalsColor(const Blend& rhv) const {
            return equalsColor(rhv.red, rhv.green, rhv.blue, rhv.alpha);
        }

        bool equalsColor(float r, float g, float b, float a) const {
            return red == r && green == g && blue == b && alpha == a;
        }

        bool equalsFunc(const Blend& rhv) const {
            return equalsFunc(
                (SGBlendFactor)rhv.sfactor_rgb,
                (SGBlendFactor)rhv.dfactor_rgb,
                (SGBlendFactor)rhv.sfactor_a,
                (SGBlendFactor)rhv.dfactor_a);
        }

        bool equalsFunc(
            SGBlendFactor srcRGB, SGBlendFactor dstRGB,
            SGBlendFactor srcAlpha, SGBlendFactor dstAlpha) const
        {
            return
                sfactor_rgb == (uint32_t)srcRGB &&
                dfactor_rgb == (uint32_t)dstRGB &&
                sfactor_a == (uint32_t)srcAlpha &&
                dfactor_a == (uint32_t)dstAlpha;
        }

        bool equalsEqu(const Blend& rhv) const {
            return equalsEqu(
                (SGBlendEquation)rhv.equation_rgb,
                (SGBlendEquation)rhv.equation_a);
        }

        bool equalsEqu(SGBlendEquation modeRGB, SGBlendEquation modeAlpha) const {
            return equation_rgb == (uint32_t)modeRGB &&
                equation_a == (uint32_t)modeAlpha;
        }

        bool color(float r, float g, float b, float a);
        bool function(
            SGBlendFactor src, SGBlendFactor dst);
        bool function(
            SGBlendFactor srcRGB, SGBlendFactor dstRGB,
            SGBlendFactor srcAlpha, SGBlendFactor dstAlpha);
        bool equation(SGBlendEquation mode);
        bool equation(SGBlendEquation modeRGB, SGBlendEquation modeAlpha);

        void rollbackFrom(const Blend& current);
    } blend;

    // culling
    struct SG_LOCAL_API Culling {
        uint8_t face_; // initial(back)
        uint8_t front_;

        void init();
        bool face(SGFace f);
        bool front(SGFrontFace f);

        void rollbackFrom(const Culling& current);
    } culling;

    // depth test
    struct SG_LOCAL_API DepthTest {
        uint8_t func_;   // initial(less)
        bool mask_;  // initial(true)
        float near_, far_; // initial(0, 1)

        void init();
        bool function(SGFunction f);
        bool mask(bool m);
        bool range(float n, float f);

        void rollbackFrom(const DepthTest& current);
    } depth_test;

    // polygon offset fill
    struct SG_LOCAL_API PolygonOffset {
        float factor_;   // initial(0)
        float units_;    // initial(0)

        void init();
        bool set(float factor, float units);

        void rollbackFrom(const PolygonOffset& current);
    } polygon_offset;

    struct SG_LOCAL_API SampleCoverage {
        float value_;    // initial(1.0)
        bool invert_;    // initial(false)

        void init();
        bool set(float value, bool invert);

        void rollbackFrom(const SampleCoverage& current);
    } sample_coverage;

    // scissor test
    struct SG_LOCAL_API Scissor {
        int32_t x_, y_;   // initial(0, 0)
        uint32_t width_, height_; // initial(window.width, window.height)

        void init();
        bool box(int32_t x, int32_t y, uint32_t width, uint32_t height);
        bool equals(const Scissor& rhv) const {
            return equals(rhv.x_, rhv.y_, rhv.width_, rhv.height_);
        }
        bool equals(int32_t x, int32_t y, uint32_t width, uint32_t height) const {
            return x_ == x && y_ == y && width_ == width && height_ == height;
        }

        void rollbackFrom(const Scissor& current);
    } scissor;

    // stencil
    struct SG_LOCAL_API Stencil {
        struct SG_LOCAL_API Func {
            uint8_t func;   // initial(always)
            int32_t ref;    // initial(0)
            uint32_t mask;  // initial(0xffffffff ??)

            void set(SGFunction f, int32_t r, uint32_t m);
            bool equals(const Func& rhv) const {
                return equals((SGFunction)rhv.func, rhv.ref, rhv.mask);
            }
            bool equals(SGFunction f, int32_t r, uint32_t m) const {
                return f == func && r == ref && m == mask;
            }
        } func_[2]; // 0: front, 1: back

        struct SG_LOCAL_API Op {
            uint16_t sfail_ : 4;  // initial(keep)
            uint16_t dpfail_ : 4; // initial(keep)
            uint16_t dppass_ : 4; // initial(keep)

            void set(SGStencilOp sfail, SGStencilOp dpfail, SGStencilOp dppass);
            bool equals(const Op& rhv) const {
                return equals(
                    (SGStencilOp)rhv.sfail_,
                    (SGStencilOp)rhv.dpfail_,
                    (SGStencilOp)rhv.dppass_);
            }
            bool equals(SGStencilOp sfail, SGStencilOp dpfail, SGStencilOp dppass) const {
                return (uint8_t)sfail == sfail_ &&
                    (uint8_t)dpfail == dpfail_ &&
                    (uint8_t)dppass == dppass_;
            }
        } op_[2];
        uint32_t mask_[2];  // initial(0xffffffff ??)

        void init();
        bool func(SGFunction f, int32_t r, uint32_t m);
        bool func(SGFace face, SGFunction f, int32_t r, uint32_t m);
        bool op(SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass);
        bool op(SGFace face, SGStencilOp sfail, SGStencilOp dfail, SGStencilOp dppass);

        bool mask(uint32_t m);
        bool mask(SGFace face, uint32_t m);

        void rollbackFrom(const Stencil& current);
    } stencil;

    struct SG_LOCAL_API Renderer {
        uint32_t enable_wireframe : 1; // initial(false)
        uint32_t enable_lighting : 1; // initial(true)

        Colorf wire_color; // initial(0,0,0,1)

        void init();
    } renderer;
};

}

#endif
