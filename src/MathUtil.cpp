/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/MathUtil.h"

#ifdef TARGET_OS_WIN
// disable "nonstandard extension used"
#pragma warning(disable:4201)
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/compatibility.hpp>

namespace sg {

static glm::vec2 conv(const Vector2& rhv);
static Vector2 conv(const glm::vec2& rhv);
static glm::vec3 conv(const Vector3& rhv);
static Vector3 conv(const glm::vec3& rhv);
static glm::vec4 conv(const Vector4& rhv);
static Vector4 conv(const glm::vec4& rhv);
static glm::quat conv(const Quaternion& rhv);
static Quaternion conv(const glm::quat& rhv);
static glm::mat4 conv(const Matrix4& rhv);
static Matrix4 conv(const glm::mat4& rhv);


/*----------------------------------------------
 * Vector2
 ----------------------------------------------*/

Vector2 Vector2::make(float x, float y)
{
    return { x, y };
}

Vector2 Vector2::makeNormalized(float x, float y)
{
    return conv(glm::normalize(glm::vec2(x, y)));
}

Vector2 Vector2::lerp(const Vector2& s, const Vector2& e, float t)
{
    return conv(glm::lerp(conv(s), conv(e), t));
}

void Vector2::normalize()
{
    *this = conv(glm::normalize(conv(*this)));
}

Vector2 Vector2::operator+(const Vector2& rhv) const
{
    return conv(conv(*this) + conv(rhv));
}

Vector2 Vector2::operator-(const Vector2& rhv) const
{
    return conv(conv(*this) - conv(rhv));
}

float Vector2::dot(const Vector2& rhv) const
{
    return glm::dot(conv(*this), conv(rhv));
}

/*----------------------------------------------
 * Vector3
 ----------------------------------------------*/

Vector3 Vector3::make(float x, float y, float z)
{
    return { x, y, z };
}

Vector3 Vector3::makeNormalized(float x, float y, float z)
{
    return conv(glm::normalize(glm::vec3(x, y, z)));
}

Vector3 Vector3::lerp(const Vector3& s, const Vector3& e, float t)
{
    return conv(glm::lerp(conv(s), conv(e), t));
}

void Vector3::normalize() {
    *this = conv(glm::normalize(conv(*this)));
}

Vector3 Vector3::operator+(const Vector3& rhv) const
{
    return conv(conv(*this) + conv(rhv));
}

const Vector3& Vector3::operator+=(const Vector3& rhv)
{
    x += rhv.x;
    y += rhv.y;
    z += rhv.z;
    return *this;
}

Vector3 Vector3::operator-(const Vector3& rhv) const
{
    return conv(conv(*this) - conv(rhv));
}

const Vector3& Vector3::operator-=(const Vector3& rhv)
{
    x -= rhv.x;
    y -= rhv.y;
    z -= rhv.z;
    return *this;
}

Vector3 Vector3::operator*(float rhv) const
{
    return {x * rhv, y * rhv, z * rhv};
}

const Vector3& Vector3::operator*=(float rhv)
{
    x *= rhv;
    y *= rhv;
    z *= rhv;
    return *this;
}

Vector3 Vector3::cross(const Vector3& rhv) const
{
    return conv(glm::cross(conv(*this), conv(rhv)));
}

float Vector3::dot(const Vector3& rhv) const
{
    return glm::dot(conv(*this), conv(rhv));
}


/*----------------------------------------------
 * Vector4
 ----------------------------------------------*/

Vector4 Vector4::make(float x, float y, float z, float w)
{
    return { x, y, z, w };
}

Vector4 Vector4::makeNormalized(float x, float y, float z, float w)
{
    return conv(glm::normalize(glm::vec4(x, y, z, w)));
}

Vector4 Vector4::lerp(const Vector4& s, const Vector4& e, float t)
{
    return conv(glm::lerp(conv(s), conv(e), t));
}

void Vector4::normalize() {
    *this = conv(glm::normalize(conv(*this)));
}

Vector4 Vector4::operator+(const Vector4& rhv) const
{
    return conv(conv(*this) + conv(rhv));
}

Vector4 Vector4::operator-(const Vector4& rhv) const
{
    return conv(conv(*this) - conv(rhv));
}

float Vector4::dot(const Vector4& rhv) const
{
    return glm::dot(conv(*this), conv(rhv));
}


/*----------------------------------------------
 * Quaternion
 ----------------------------------------------*/

Quaternion Quaternion::make(float w, float x, float y, float z)
{
    return { x, y, z, w };
}

void Quaternion::normalize()
{
    *this = conv(glm::normalize(conv(*this)));
}

void Quaternion::inverse()
{
    *this = conv(glm::inverse(conv(*this)));
}

Quaternion Quaternion::rotate(float angle, float x, float y, float z)
{
    return conv(glm::angleAxis(angle, glm::vec3(x, y, z)));
}

Quaternion Quaternion::rotate(float pitch, float yaw, float roll)
{
    return conv(glm::quat(glm::radians(glm::vec3(pitch, yaw, roll))));
}

Quaternion Quaternion::lerp(const Quaternion& s, const Quaternion& e, float t)
{
    return conv(glm::lerp(conv(s), conv(e), t));
}

Quaternion Quaternion::slerp(const Quaternion& s, const Quaternion& e, float t)
{
    return conv(glm::slerp(conv(s), conv(e), t));
}

Quaternion Quaternion::operator*(const Quaternion& rhv) const
{
    return conv(conv(*this) * conv(rhv));
}

const Quaternion& Quaternion::operator*=(const Quaternion& rhv)
{
    conv(*this) *= conv(rhv);
    return *this;
}

Vector3 Quaternion::operator*(const Vector3& rhv) const
{
    // return glm::quat * glm::vec3.
    return conv(conv(*this) * conv(rhv));
}

void Quaternion::eulerAngles(float* pitch, float* yaw, float* roll) const
{
    auto angles = glm::degrees(glm::eulerAngles(conv(*this)));

    if (pitch) {
        *pitch = angles.x;
    }

    if (yaw) {
        *yaw = angles.y;
    }

    if (roll) {
        *roll = angles.z;
    }
}


/*----------------------------------------------
 * Matrix4
 ----------------------------------------------*/

Matrix4 Matrix4::make(const float m[4 * 4])
{
    Matrix4 mat;
    memcpy(mat.m, m, sizeof(Matrix4));
    return mat;
}

void Matrix4::identity()
{
    glm::mat4 mat;
    memcpy(m, glm::value_ptr(mat), sizeof(Matrix4));
}

void Matrix4::inverse()
{
    auto mat = conv(*this);
    glm::inverse(mat);
    memcpy(m, glm::value_ptr(mat), sizeof(Matrix4));
}

void Matrix4::transpose()
{
    auto mat = conv(*this);
    glm::transpose(mat);
    memcpy(m, glm::value_ptr(mat), sizeof(Matrix4));
}

/*----------------------------------------------
 * Static functions
 ----------------------------------------------*/

static glm::vec2 conv(const Vector2& rhv)
{
    return glm::vec2(rhv.x, rhv.y);
}

static Vector2 conv(const glm::vec2& rhv)
{
    return { rhv.x, rhv.y };
}

static glm::vec3 conv(const Vector3& rhv)
{
    return glm::vec3(rhv.x, rhv.y, rhv.z);
}

static Vector3 conv(const glm::vec3& rhv)
{
    return { rhv.x, rhv.y, rhv.z };
}

static glm::vec4 conv(const Vector4& rhv)
{
    return glm::vec4(rhv.x, rhv.y, rhv.z, rhv.w);
}

static Vector4 conv(const glm::vec4& rhv)
{
    return { rhv.x, rhv.y, rhv.z, rhv.w };
}

static glm::quat conv(const Quaternion& rhv)
{
    return glm::quat(rhv.w, rhv.x, rhv.y, rhv.z);
}

static Quaternion conv(const glm::quat& rhv)
{
    return { rhv.x, rhv.y, rhv.z, rhv.w };
}

static glm::mat4 conv(const Matrix4& rhv)
{
    return glm::make_mat4(rhv.m);
}

static Matrix4 conv(const glm::mat4& rhv)
{
    Matrix4 mat;
    memcpy(mat.m, glm::value_ptr(rhv), sizeof(Matrix4));
    return mat;
}

}
