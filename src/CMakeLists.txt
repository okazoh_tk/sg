
set(SRCS_sg
    Application.cpp
    ApplicationImpl.cpp
    AssimpModelAsset.cpp
    AxisModel.cpp
    BasicModel.cpp
    BoxModel.cpp
    Buffer.cpp
    Builder.cpp
    Context.cpp
    DebugFont.cpp
    DebugView.cpp
    Device.cpp
    Event.cpp
    EventDelegate.cpp
    GradientModel.cpp
    GridModel.cpp
    IDevice.cpp
    IModelAsset.cpp
    IndexBuffer.cpp
    Log.cpp
    Material.cpp
    MatrixStack.cpp
    MemoryAllocator.cpp
    Mesh.cpp
    Model.cpp
    ModelAsset.cpp
    MonoTextureModel.cpp
    Node.cpp
    Program.cpp
    Property.cpp
    RectangleModel.cpp
    RenderTarget.cpp
    SGString.cpp
    Setting.cpp
    SettingData.cpp
    Shader.cpp
    SharedObject.cpp
    SimpleDevice.cpp
    SimpleModel.cpp
    SolidModel.cpp
    Texture.cpp
    Time.cpp
    VertexBuffer.cpp
    Window.cpp
    WireModel.cpp)

set(SRCS_glfw
    GLEWContext.cpp
    GLFWBuilder.cpp
    GLFWWindow.cpp)

set(SRCS_egl
    EGLObject.cpp)

set(SRCS_win
    Win32Builder.cpp
    Win32Window.cpp)


set(INC_PATH ${PROJECT_SOURCE_DIR}/include)
file(GLOB_RECURSE INCS_internal "*.h")
file(GLOB_RECURSE INCS "${INC_PATH}/*h")

include_directories(${INC_PATH})

include_directories(${PROJECT_SOURCE_DIR}/third_party/glm)

if (USE_OPENGL_ES)
    include_directories(${PROJECT_SOURCE_DIR}/third_party/angle/include)
else()
    include_directories(${PROJECT_SOURCE_DIR}/third_party/glfw/include)
    include_directories(${PROJECT_SOURCE_DIR}/third_party/glew/include)
endif()

include_directories(${PROJECT_SOURCE_DIR}/third_party/assimp/include)

if (BUILD_SHARED_LIBS)
    add_definitions(-DSG_COMPILE_AS_DLL)
    add_library(sg SHARED ${SRCS_sg} ${INCS_internal} ${INCS})
else()
    add_library(sg STATIC ${SRCS_sg} ${INCS_internal} ${INCS})
endif()

target_link_libraries(sg ${LIBS})
