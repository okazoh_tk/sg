/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/DebugView.h"
#include "sg/MemoryAllocator.h"
#include "sg/Context.h"
#include "sg/DebugFont.h"
#include "sg/Texture.h"
#include "sg/VertexBuffer.h"
#include "sg/model/MonoTextureModel.h"
#include "sg/EventDriver.h"
#include <string>
#include <vector>

namespace sg {

#define LINE            (8)
#define COL             (0x80/LINE)
#define ATLAS_WIDTH     (SG_DEBUG_FONT_WIDTH*COL)
#define ATLAS_HEIGHT    (SG_DEBUG_FONT_HEIGHT*LINE)
#define LINE_LIMIT      (100)

class SG_LOCAL_API DebugViewImpl {
public:
    DebugViewImpl()
        : mIsDirty(false)
        , mMeasurePerf(true)
        , mDrawPerf(true)
        , mDebugTextureModel(Buffer::DYNAMIC_DRAW, Buffer::DYNAMIC_DRAW)
        , mFps(0.f)
    {
        mDebugTextureModel.setPrimitiveType(SG_TRIANGLE_STRIP);
        Colorf color = {0,0,0,1};
        mDebugTextureModel.setColor(color);
        mDebugTextureModel.setTexture(&mFontTexture);


        auto allocator = getDefaultMemoryAllocator();
        auto pitch = ATLAS_WIDTH;
        uint32_t size = ATLAS_WIDTH * ATLAS_HEIGHT * sizeof(uint8_t);
        void* buffer = allocator->allocate(size);
        memset(buffer, 0, size);
        uint8_t* p;

        for (int32_t i = 0; i < 0x80; ++i) {
            p = (uint8_t*)buffer + ((i / COL) * pitch * SG_DEBUG_FONT_HEIGHT + (i % COL) * SG_DEBUG_FONT_WIDTH);
            copyDebugFont(i, p, pitch);
        }

        mFontTexture.set(
            ATLAS_WIDTH, ATLAS_HEIGHT, Texture::ALPHA,
            buffer,
            SG_A8,
            allocator);
    }

    void draw(EventDriver& caller, Context& gl)
    {
        gl.pushSetting();

        gl.wireframe.off();
        gl.culling.off();
        gl.depthTest.off();
        gl.blend.on();
        gl.blend.func(SG_SRC_ALPHA, SG_ONE_MINUS_SRC_ALPHA);

        auto size = caller.getWindowSize();
        mDebugMatrix.project(Ortho{ 0, (float)size.width, (float)size.height, 0, -1, 1 });
        gl.begin(&mDebugMatrix);
        if (mDrawPerf) {

            printDebugInfo(caller);

            if (mIsDirty) {
                makeVertexBuffer();
                mIsDirty = false;
            }

            if (mDebugTextIndices.size() > 0) {

                // draw quads by triangle_strip with degenerate triangle
                gl.draw(mDebugTextureModel);
            }
        }
        gl.end();

        gl.popSetting();
    }

    void printDebugInfo(EventDriver& caller)
    {
        printFps(caller.getFps());
        printRusage();
    }

    void printFps(float f)
    {
        auto fps = (uint32_t)(f * 10);
        // draw fps as string
        char str_fps[6]; // 100.0
        str_fps[0] = fps >= 1000 ? '0' + (fps / 1000) % 10 : ' ';
        str_fps[1] = fps >= 100 ? '0' + (fps / 100) % 10 : ' ';
        str_fps[2] = '0' + (fps / 10) % 10;
        str_fps[3] = '.';
        str_fps[4] = '0' + fps % 10;
        str_fps[5] = '\0';

        print(0, str_fps);
    }

    void printRusage()
    {
    }

    void print(int lineno, const char* str)
    {
        if (lineno < 0 || lineno > LINE_LIMIT) {
            return;
        }

        if ((uint32_t)lineno >= mTexts.size()) {
            mTexts.resize(lineno + 1, std::string(""));
        }
        if (mIsDirty || mTexts[lineno] != str) {
            mTexts[lineno] = str;
            mIsDirty = true;
        }
    }

    void clear(int lineno)
    {
        if (lineno > LINE_LIMIT) {
            return;
        }

        if (mTexts[lineno].length()) {
            mTexts[lineno] = "";
            mIsDirty = true;
        }
    }

    void clearAll()
    {
        if (mTexts.size() > 0) {
            mTexts.clear();
            mIsDirty = true;
        }
    }

    void getUV(int32_t ch, Vector2& top_left, Vector2& bottom_right) const
    {
        uint8_t index = (uint8_t)(ch % 0x7f);
        uint16_t x_in_pixel = (index % COL) * SG_DEBUG_FONT_WIDTH;
        uint16_t y_in_pixel = (index / COL) * SG_DEBUG_FONT_HEIGHT;
        top_left.x = x_in_pixel / (float)ATLAS_WIDTH;
        top_left.y = y_in_pixel / (float)ATLAS_HEIGHT;
        bottom_right.x = (x_in_pixel + SG_DEBUG_FONT_WIDTH) / (float)ATLAS_WIDTH;
        bottom_right.y = (y_in_pixel + SG_DEBUG_FONT_HEIGHT) / (float)ATLAS_HEIGHT;
    }

    void makeVertexBuffer()
    {
        mDebugTextVertices.clear();
        mDebugTextIndices.clear();
        Vector2 top_left, bottom_right;
        PosTexVertex vertex[4];
        uint16_t index = 0;

        int line = 0;
        for (auto& str : mTexts) {
            int col = 0;
            for (auto ch : str) {
                if (!(ch == ' ' || ch == '\0')) {
                    getUV(ch, top_left, bottom_right);
                    vertex[0].position.x = (float)col;
                    vertex[0].position.y = (float)line;
                    vertex[0].position.z = 0;
                    vertex[0].uv.x = top_left.x;
                    vertex[0].uv.y = top_left.y;
                    vertex[1].position.x = (float)col;
                    vertex[1].position.y = (float)line + SG_DEBUG_FONT_HEIGHT;
                    vertex[1].position.z = 0;
                    vertex[1].uv.x = top_left.x;
                    vertex[1].uv.y = bottom_right.y;
                    vertex[2].position.x = (float)col + SG_DEBUG_FONT_WIDTH;
                    vertex[2].position.y = (float)line;
                    vertex[2].position.z = 0;
                    vertex[2].uv.x = bottom_right.x;
                    vertex[2].uv.y = top_left.y;
                    vertex[3].position.x = (float)col + SG_DEBUG_FONT_WIDTH;
                    vertex[3].position.y = (float)line + SG_DEBUG_FONT_HEIGHT;
                    vertex[3].position.z = 0;
                    vertex[3].uv.x = bottom_right.x;
                    vertex[3].uv.y = bottom_right.y;

                    mDebugTextVertices.push_back(vertex[0]);
                    mDebugTextVertices.push_back(vertex[1]);
                    mDebugTextVertices.push_back(vertex[2]);
                    mDebugTextVertices.push_back(vertex[3]);

                    // make degenerate triangle
                    if (index != 0) {
                        mDebugTextIndices.push_back(index - 1);
                        mDebugTextIndices.push_back(index);
                    }
                    mDebugTextIndices.push_back(index++);
                    mDebugTextIndices.push_back(index++);
                    mDebugTextIndices.push_back(index++);
                    mDebugTextIndices.push_back(index++);
                }

                col += SG_DEBUG_FONT_WIDTH;
            }
            line += SG_DEBUG_FONT_HEIGHT;
        }

        auto vertices_num = mDebugTextVertices.size();
        if (vertices_num) {

            mDebugTextureModel.setVertices(
                mDebugTextVertices.data(),
                vertices_num);

            mDebugTextureModel.setIndices(
                mDebugTextIndices.data(),
                mDebugTextIndices.size());
        }
    }

private:
    bool mIsDirty;
    bool mMeasurePerf;
    bool mDrawPerf;
    float mFps;
    Texture mFontTexture;
    MonoTextureModel mDebugTextureModel;
    std::vector<std::string> mTexts;
    std::vector<MonoTextureModel::VertexType> mDebugTextVertices;
    std::vector<uint16_t> mDebugTextIndices;
    MatrixStack mDebugMatrix;
};

DebugView::DebugView()
    : mImpl(new DebugViewImpl())
{
}

DebugView::~DebugView()
{
    delete mImpl;
}

void DebugView::print(int lineno, const char* str)
{
    mImpl->print(lineno, str);
}

void DebugView::clear(int lineno)
{
    mImpl->clear(lineno);
}

void DebugView::clearAll()
{
    mImpl->clearAll();
}

void DebugView::onUpdate(EventDriver& /*caller*/)
{
    // do nothing.
}

void DebugView::onDraw(EventDriver& caller, Context& gl)
{
    mImpl->draw(caller, gl);
}

}
