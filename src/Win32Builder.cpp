/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/Config.h"
#include "OpenGL.h"
#include "./Win32Builder.h"
#include "SimpleDevice.h"

#if ENABLE_GLFW != 0
#include "./GLFWWindow.h"
#include "./GLEWContext.h"

#pragma comment(lib, "Opengl32.lib")
#define WindowClass     GLFWWindow
#define ContextClass    GLEWContext
#else

#include "./Win32Window.h"
#include "sg/Context.h"
#define ContextClass    Context

#if defined(SG_OPENGL_ES)
#define WindowClass     EGLWindow
#else
#define WindowClass    WGLWindow
#endif

#endif

namespace sg {

Win32Builder* Win32Builder::create()
{
    return new Win32Builder();
}

Win32Builder::Win32Builder()
    : mWindow(nullptr)
    , mContext(nullptr)
{
}

Win32Builder::~Win32Builder()
{
    if (mContext) {
        delete mContext;
        mContext = nullptr;
    }

    if (mWindow) {
        delete mWindow;
        mWindow = nullptr;
    }
}
    
bool Win32Builder::init(const Size& windowSize)
{
    auto window = new WindowClass(windowSize);

    if (!window->initWindow()) {
        delete window;
    } else {
        mWindow = window;
    }

    return mWindow != nullptr;
}

void Win32Builder::release()
{
    delete this;
}

Window* Win32Builder::getWindow()
{
    return mWindow;
}

Context* Win32Builder::createContext()
{
    if (nullptr == mContext) {
        mContext = new ContextClass(mWindow->getSize());
    }

    return mContext;
}

void Win32Builder::destroyContext(Context* context)
{
    delete context;
    mContext = nullptr;
}

IDevice* Win32Builder::getDeviceInterface()
{
    return static_cast<WindowClass*>(mWindow)->getDevice();
}

}
