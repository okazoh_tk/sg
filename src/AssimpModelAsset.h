/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SG_ASSIMP_MODEL_ASSET_H_INCLUDED_
#define SG_ASSIMP_MODEL_ASSET_H_INCLUDED_

#include "IModelAsset.h"
#include "sg/Mesh.h"
#include "sg/Material.h"
#include "sg/Node.h"
#include "sg/Program.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace sg {

class SG_LOCAL_API AssimpMesh : public Mesh {
public:
    AssimpMesh(aiMesh* mesh);
    virtual ~AssimpMesh();

    bool isComplete() const;

private:

    bool makeVertexBuffer(aiMesh* mesh);
    bool makeIndexBuffer(aiMesh* mesh);

private:
    std::vector<VertexAttrib> mLayout;
    VertexBuffer mVertexBuffer;
    IndexBuffer mIndexBuffer;
};

class SG_LOCAL_API AssimpMaterial : public Material {
public:
    AssimpMaterial(aiMaterial* material);
    virtual ~AssimpMaterial();

    virtual bool update(Context& gl);
    virtual void restore(Context& gl);

private:
    bool make(aiMaterial* material);

private:
    Program* mProgram;
    bool mCulling;
};

class SG_LOCAL_API AssimpModel {
public:
    AssimpModel(AssimpMesh* mesh, AssimpMaterial* material);
    ~AssimpModel();

    void update();
    void draw(Context& gl);

private:
    AssimpMesh* mMesh;
    AssimpMaterial* mMaterial;
};

class SG_LOCAL_API AssimpNode : public Node {
public:
    AssimpNode(uint32_t num_models);
    virtual ~AssimpNode();

    void set(uint32_t index, AssimpModel* model);
    void set(const aiMatrix4x4& mat);

protected:
    virtual bool onUpdate();
    //virtual void onUpdatePost();
    virtual bool onDraw(Context& gl);
    virtual void onDrawPost(Context& gl);

private:
    union {
        AssimpModel* model;
        AssimpModel** models;
    } mList;
    uint32_t mNum;
    bool mNeedTransform;
    Matrix4 mTransformation;
};

class SG_LOCAL_API AssimpModelAsset : public IModelAsset {
public:

    AssimpModelAsset(Model& owner);
    virtual ~AssimpModelAsset();

    virtual bool load(const char* filepath);
    virtual void update();
    virtual void draw(Context& gl);

private:
    bool mPrepared;
    std::vector<AssimpModel*> mModels;
    std::vector<AssimpMaterial*> mMaterials;
    std::vector<AssimpMesh*> mMeshes;
    AssimpNode* mRootNode;
};

}

#endif
