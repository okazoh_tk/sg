/*
 * Copyright (c) 2015, okazoh_tk All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "sg/EventDriver.h"
#include "sg/EventLooper.h"
#include "sg/Time.h"
#include "sg/MemoryAllocator.h"
#include "sg/EventDelegate.h"
#include "./DefaultBuilder.h"
#include "./Utility.h"

namespace sg {

class SG_LOCAL_API EventDriverImpl : EventDelegate {
public:

    EventDriverImpl(EventDriver* thiz, const Size& window_size)
        : mThis(thiz)
        , mMemoryAllocator(nullptr)
        , mBuilder(nullptr)
        , mWindow(nullptr)
        , mContext(nullptr)
        , mDevice(nullptr)
        , mMeasurePerf(true)
        , mDrawPerf(true)
        , mTime(Time::now())
        , mSwapCount(0)
        , mWindowSize(window_size)
    {
        if (nullptr == getDefaultMemoryAllocator()) {
            mMemoryAllocator = new Heap();
            setDefaultMemoryAllocator(mMemoryAllocator);
        }
    }

    ~EventDriverImpl()
    {
        releaseResources();

        if (mMemoryAllocator) {
            delete mMemoryAllocator;
        }
    }

    void setBuilder(Builder* builder)
    {
        mBuilder = builder;
    }

    int32_t buildResources()
    {
        if (!mBuilder) {
            mBuilder = CreateDefaultBuilder();
        }

        ASSERT_RET(mBuilder != nullptr, SG_ERROR_NO_BUILDER);

        if (!mBuilder->init(mWindowSize)) {
            mBuilder->release();
            return SG_ERROR_INIT_BUILDER;
        }

        mWindow = mBuilder->getWindow();
        mContext = mBuilder->createContext();
        mDevice = new Device(mBuilder->getDeviceInterface());

        // update new window size
        mWindowSize = mWindow->getSize();

        return SG_NO_ERROR;
    }

    void releaseResources()
    {
        if (mDevice) {
            delete mDevice;
            mDevice = nullptr;
        }

        if (mContext) {
            mBuilder->destroyContext(mContext);
            mContext = nullptr;
        }

        if (mBuilder) {
            mBuilder->release();
            mBuilder = nullptr;
        }
    }

    bool setup()
    {
        fire(mSystemEventListeners, listener->onLaunching(*mThis));

        int32_t ret = buildResources();

        if (ret != SG_NO_ERROR) {
            return false;
        }

        fire(mSystemEventListeners, listener->onLaunched(*mThis));

        return true;
    }

    bool handleEvents()
    {
        return mWindow->handleEvents(*this);
    }

    void terminate()
    {
        fire(mSystemEventListeners, listener->onDestroy(*mThis));
    }

    void exitLoop()
    {
        mWindow->exitLoop();
    }

    virtual void draw()
    {
        auto& gl = *mContext;

        fire(mRendererEventListeners, listener->onUpdate(*mThis));
        fire(mRendererEventListeners, listener->onDraw(*mThis, gl));

        measurePerf();

        mWindow->swap();
    }

    virtual void firePointerEvent(PointerEvent::Type type, int32_t id)
    {
        PointerEvent e(type, mDevice->pointer(id));
        fire(mPointerEventListeners, listener->onPointerEvent(*mThis, e));
    }

    virtual void fireButtonEvent(ButtonEvent::Type type, int32_t id)
    {
        ButtonEvent e(type, mDevice->button(id));
        fire(mButtonEventListeners, listener->onButtonEvent(*mThis, e));
    }

    virtual void fireKeyEvent(KeyEvent::Type type, int32_t ch)
    {
        KeyEvent e(type, mDevice->key(ch));
        fire(mKeyEventListeners, listener->onKeyEvent(*mThis, e));
    }

    virtual void fireScrollEvent(int32_t xoffset, int32_t yoffset)
    {
        ScrollEvent e(xoffset, yoffset);
        fire(mUIEventListeners, listener->onScroll(*mThis, e));
    }

    void addEventListener(KeyEventListener* listener, int32_t priority)
    {
        mKeyEventListeners.add(listener, priority);
    }

    void addEventListener(ButtonEventListener* listener, int32_t priority)
    {
        mButtonEventListeners.add(listener, priority);
    }

    void addEventListener(PointerEventListener* listener, int32_t priority)
    {
        mPointerEventListeners.add(listener, priority);
    }

    void addEventListener(UIEventListener* listener, int32_t priority)
    {
        mUIEventListeners.add(listener, priority);
    }

    void addEventListener(RendererEventListener* listener, int32_t priority)
    {
        mRendererEventListeners.add(listener, priority);
    }

    void addEventListener(SystemEventListener* listener, int32_t priority)
    {
        mSystemEventListeners.add(listener, priority);
    }

    void removeEventListener(KeyEventListener* listener)
    {
        mKeyEventListeners.remove(listener);
    }

    void removeEventListener(ButtonEventListener* listener)
    {
        mButtonEventListeners.remove(listener);
    }

    void removeEventListener(PointerEventListener* listener)
    {
        mPointerEventListeners.remove(listener);
    }

    void removeEventListener(UIEventListener* listener)
    {
        mUIEventListeners.remove(listener);
    }

    void removeEventListener(RendererEventListener* listener)
    {
        mRendererEventListeners.remove(listener);
    }

    void removeEventListener(SystemEventListener* listener)
    {
        mSystemEventListeners.remove(listener);
    }


    bool setWindowSize(const Size& size)
    {
        if (mWindow) {
            // already created
            return false;
        }

        mWindowSize = size;
        return true;
    }

    Size getWindowSize() const
    {
        return mWindow->getSize();
    }

    float getFps() const
    {
        return mFps;
    }

    const Device& getDevice() const
    {
        return *mDevice;
    }

    void measurePerf()
    {
        auto t = Time::now();
        auto diff = (Time::time_type)(t - mTime);
        ++mSwapCount;
        if (diff > 500 * 1000) {
            mFps = mSwapCount * 1000000.f / diff;
            mTime = t;
            mSwapCount = 0;
        }
    }

private:
    EventDriver* mThis;
    int mArgc;
    char** mArgv;
    MemoryAllocator* mMemoryAllocator;
    Builder* mBuilder;
    Window* mWindow;
    Context* mContext;
    Device* mDevice;
    bool mMeasurePerf;
    bool mDrawPerf;
    Time mTime;
    uint32_t mSwapCount;
    float mFps;
    Size mWindowSize;

    TEventListenerChain<SystemEventListener>::Head mSystemEventListeners;
    TEventListenerChain<RendererEventListener>::Head mRendererEventListeners;
    TEventListenerChain<UIEventListener>::Head mUIEventListeners;
    TEventListenerChain<PointerEventListener>::Head mPointerEventListeners;
    TEventListenerChain<ButtonEventListener>::Head mButtonEventListeners;
    TEventListenerChain<KeyEventListener>::Head mKeyEventListeners;
};


EventDriver::EventDriver(const Size& window_size)
    : mImpl(new EventDriverImpl(this, window_size))
{
}

EventDriver::~EventDriver()
{
    delete mImpl;
}

void EventDriver::exitLoop()
{
    mImpl->exitLoop();
}

void EventDriver::addEventListener(KeyEventListener* listener, int32_t priority)
{
    if (listener) {
        mImpl->addEventListener(listener, priority);
    }
}

void EventDriver::addEventListener(ButtonEventListener* listener, int32_t priority)
{
    if (listener) {
        mImpl->addEventListener(listener, priority);
    }
}

void EventDriver::addEventListener(PointerEventListener* listener, int32_t priority)
{
    if (listener) {
        mImpl->addEventListener(listener, priority);
    }
}

void EventDriver::addEventListener(UIEventListener* listener, int32_t priority)
{
    if (listener) {
        mImpl->addEventListener(listener, priority);
    }
}

void EventDriver::addEventListener(RendererEventListener* listener, int32_t priority)
{
    if (listener) {
        mImpl->addEventListener(listener, priority);
    }
}

void EventDriver::addEventListener(SystemEventListener* listener, int32_t priority)
{
    if (listener) {
        mImpl->addEventListener(listener, priority);
    }
}

void EventDriver::removeEventListener(KeyEventListener* listener)
{
    if (listener) {
        mImpl->removeEventListener(listener);
    }
}

void EventDriver::removeEventListener(ButtonEventListener* listener)
{
    if (listener) {
        mImpl->removeEventListener(listener);
    }
}

void EventDriver::removeEventListener(PointerEventListener* listener)
{
    if (listener) {
        mImpl->removeEventListener(listener);
    }
}

void EventDriver::removeEventListener(UIEventListener* listener)
{
    if (listener) {
        mImpl->removeEventListener(listener);
    }
}

void EventDriver::removeEventListener(RendererEventListener* listener)
{
    if (listener) {
        mImpl->removeEventListener(listener);
    }
}

void EventDriver::removeEventListener(SystemEventListener* listener)
{
    if (listener) {
        mImpl->removeEventListener(listener);
    }
}


Size EventDriver::getWindowSize() const
{
    return mImpl->getWindowSize();
}

float EventDriver::getFps() const
{
    return mImpl->getFps();
}

const Device& EventDriver::getDevice() const
{
    return mImpl->getDevice();
}



EventLooper::EventLooper(const Size& window_size)
    : EventDriver(window_size)
{
}

EventLooper::~EventLooper()
{
}

void EventLooper::setBuilder(Builder* custom_builder)
{
    if (custom_builder) {
        mImpl->setBuilder(custom_builder);
    }
}

bool EventLooper::setup()
{
    return mImpl->setup();
}

bool EventLooper::handleEvents()
{
    return mImpl->handleEvents();
}

void EventLooper::terminate()
{
    mImpl->terminate();
}



}
